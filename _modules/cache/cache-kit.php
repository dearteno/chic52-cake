<?php

if (!defined('IN_SITE')) die('Direct Access to this location is not allowed.');

$cache_active = true; 
$cache_folder = $BASEDIR.'/_files/cache/';

class acmeCache{ 

 function fetch($name, $refreshSeconds = 0){
  if(!$GLOBALS['cache_active']) return false; 
  if(!$refreshSeconds) $refreshSeconds = $CACHE_TIMEOUT; 
  $cacheFile = acmeCache::cachePath($name); 
  if(file_exists($cacheFile) and
   ((time()-filemtime($cacheFile))< $refreshSeconds)) 
   $cacheContent = file_get_contents($cacheFile);
  return $cacheContent;
 } 
 
 function save($name, $cacheContent){
  if(!$GLOBALS['cache_active']) return; 
  $cacheFile = acmeCache::cachePath($name);
  acmeCache::savetofile($cacheFile, $cacheContent);
 } 

 function cachePath($name){
  $cacheFolder = $GLOBALS['cache_folder'];
  if(!$cacheFolder) $cacheFolder = trim(BASEDIR,'/').'/_files/cache/';
  return $cacheFolder . md5(strtolower(trim($name))) . '.cache';
 }
 
 function savetofile($filename, $data){
  $dir = trim(dirname($filename),'/').'/'; 
  acmeCache::forceDirectory($dir);  
  $file = fopen($filename, 'w');
  fwrite($file, $data); fclose($file);
 } 
  
 function forceDirectory($dir){
  return is_dir($dir) or (acmeCache::forceDirectory(dirname($dir)));
 }

}
?>