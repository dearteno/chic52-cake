<?

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: gmmz.tv
#
###### CMS Version 1.0 ######

# PHP Version
if (version_compare(PHP_VERSION, '5', '<')) die('This website requires PHP 5 or newer, this version is '.PHP_VERSION.'.');
//ini_set('memory_limit', '128M');

# Sets the default timezone used by all date/time functions in a script
date_default_timezone_set('Asia/Bangkok');

#error_reporting(E_ERROR | E_PARSE | E_COMPILE_ERROR | E_USER_ERROR);

# PATH WEBSITE
define('BASEDIR', $_SERVER['DOCUMENT_ROOT'].'/2014');
define('BASEURL', '/2014');

# WEBSITE URL
define('HOSTNAMEURL', 'http://'.strtolower($_SERVER['HTTP_HOST']));
define('HOSTURL', HOSTNAMEURL.BASEURL);
define('ABSURL', HOSTURL); 

# BASE UPLOAD
define('BASEDIR_UPLOAD', BASEDIR.'/_files');
define('BASEURL_UPLOAD', BASEURL.'/_files');
define('ABSURL_UPLOAD', HOSTURL.'/_files');

# BACKEND URL
define('BACKEND_URL', HOSTNAMEURL.BASEURL.'/chicadmin');
define('BACKEND_DIR', BASEDIR.'/chicadmin');



header("Content-Type: text/html; charset=UTF-8");
header('P3P: CP="CAO PSA OUR"');

define('IN_SITE', true);


$WEBSITE_TITLE_HTML		="Chic Republic";

$BACKEND_URL	=BACKEND_URL;
$BASEURL		=BASEURL;
$BASEDIR		=BASEDIR;
$BASEDIR_UPLOAD =BASEDIR_UPLOAD;
$BASEURL_UPLOAD =BASEURL_UPLOAD;


$dialog_open	="false";




?>