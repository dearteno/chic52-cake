<?php
header('Content-Type: text/html; charset=utf-8');
require_once('kgPager.class.php');
$conn = mysql_pconnect('localhost', 'root', '1234');
$select = mysql_select_db('test');
mysql_query("SET NAMES 'utf8'");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>KG Pager Class v2.0 - Test</title>
<style type="text/css">
    body { font:0.8em/1.8em tahoma, helvetica, sans serif; color#333; }
    #pager_links a { text-decoration:none; color:#ff3300; background:#fff; border:1px solid #e0e0e0; padding:1px 4px 1px 4px; margin:2px; }
    #pager_links a:hover { text-decoration:none; color:#3399ff; background:#f2f2f2; border:1px solid #3399ff; padding:1px 4px 1px 4px; margin:2px; }
    #current_page { border:1px solid #333; padding:1px 4px 1px 4px; margin:2px; color:#333; }
</style>
</head>
<body>

<?php
$query = "SELECT * FROM test_table"; // sql
$sql = mysql_query($query);
$total_records = mysql_num_rows($sql); // จำนวน record ทั้งหมด
$scroll_page = 5; // จำนวน scroll page
$per_page = 10; // จำนวน record ต่อหนึ่ง page
$current_page = $_GET['page']; // หน้าปัจจุบัน
$pager_url = 'test.php?page='; // url page ที่ต้องการเรียก
$inactive_page_tag = 'id="current_page"'; // 
$previous_page_text = '&lt; ก่อนหน้า'; // หน้าก่อนหน้า
$next_page_text = 'หน้าถัดไป &gt;'; // หน้าถัดไป
$first_page_text = '&lt;&lt; หน้าแรก'; // ไปหน้าแรก
$last_page_text = 'หน้าสุดท้าย &gt;&gt;'; // ไปหน้าสุดท้าย

$kgPagerOBJ = & new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);

echo '<p><strong>จำนวนทั้งหมด : </strong>';
echo $kgPagerOBJ -> total_pages;
echo ' หน้า</p>';
$sql = mysql_query($query." ORDER BY id ASC LIMIT ".$kgPagerOBJ -> start.", ".$kgPagerOBJ -> per_page."");
/*while ($read = mysql_fetch_assoc($sql)) {
    echo '<ul>';
    echo '<li>'.$read['id'].' - '.$read['name'].' - '.$read['mail'].'</li>';
    echo '</ul>';
}*/

$i = 1;
while ($read = mysql_fetch_assoc($sql)) 
{
    echo $read['name'].' &nbsp&nbsp ';
    if($i% 5 == 0)
        echo '<br />';
   $i++;
}
echo '<p id="pager_links">';
echo $kgPagerOBJ -> first_page;
echo $kgPagerOBJ -> previous_page;
echo $kgPagerOBJ -> page_links;
echo $kgPagerOBJ -> next_page;
echo $kgPagerOBJ -> last_page;
echo '</p>';
?>
</body>
</html>