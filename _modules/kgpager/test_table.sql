-- 
-- โครงสร้างตาราง `test_table`
-- 

CREATE TABLE `test_table` (
  `id` int(11) NOT NULL auto_increment,
  `name` tinytext,
  `mail` tinytext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- 
-- dump ตาราง `test_table`
-- 

INSERT INTO `test_table` VALUES (1, 'name1', 'name1@mail');
INSERT INTO `test_table` VALUES (2, 'name2', 'name2@mail');
INSERT INTO `test_table` VALUES (3, 'name3', 'name3@mail');
INSERT INTO `test_table` VALUES (4, 'name4', 'name4@mail');
INSERT INTO `test_table` VALUES (5, 'name5', 'name5@mail');
INSERT INTO `test_table` VALUES (6, 'name6', 'name6@mail');
INSERT INTO `test_table` VALUES (7, 'name7', 'name7@mail');
INSERT INTO `test_table` VALUES (8, 'name8', 'name8@mail');
INSERT INTO `test_table` VALUES (9, 'name9', 'name9@mail');
INSERT INTO `test_table` VALUES (10, 'name10', 'name10@mail');
INSERT INTO `test_table` VALUES (11, 'name11', 'name11@mail');
INSERT INTO `test_table` VALUES (12, 'name12', 'name12@mail');
INSERT INTO `test_table` VALUES (13, 'name13', 'name13@mail');
INSERT INTO `test_table` VALUES (14, 'name14', 'name14@mail');
INSERT INTO `test_table` VALUES (15, 'name15', 'name15@mail');
INSERT INTO `test_table` VALUES (16, 'name16', 'name16@mail');
INSERT INTO `test_table` VALUES (17, 'name17', 'name17@mail');
INSERT INTO `test_table` VALUES (18, 'name18', 'name18@mail');
INSERT INTO `test_table` VALUES (19, 'name19', 'name19@mail');
INSERT INTO `test_table` VALUES (20, 'name20', 'name20@mail');
INSERT INTO `test_table` VALUES (21, 'name21', 'name21@mail');