<?
if (!defined('IN_SITE')) die('Direct Access to this location is not allowed.');


$ARRAY_MONTH[1] = "ม.ค.";
$ARRAY_MONTH[2] = "ก.พ.";
$ARRAY_MONTH[3] = "มี.ค.";
$ARRAY_MONTH[4] = "เม.ย.";
$ARRAY_MONTH[5] = "พ.ค.";
$ARRAY_MONTH[6] = "มิ.ย.";
$ARRAY_MONTH[7] = "ก.ค.";
$ARRAY_MONTH[8] = "ส.ค.";
$ARRAY_MONTH[9] = "ก.ย.";
$ARRAY_MONTH[10] = "ต.ค.";
$ARRAY_MONTH[11] = "พ.ย.";
$ARRAY_MONTH[12] = "ธ.ค.";

$ARRAY_MONTH_TH[1] = "มกราคม";
$ARRAY_MONTH_TH[2] = "กุมภาพันธ์";
$ARRAY_MONTH_TH[3] = "มีนาคม";
$ARRAY_MONTH_TH[4] = "เมษายน";
$ARRAY_MONTH_TH[5] = "พฤษภาคม";
$ARRAY_MONTH_TH[6] = "มิถุนายน";
$ARRAY_MONTH_TH[7] = "กรกฎาคม";
$ARRAY_MONTH_TH[8] = "สิงหาคม";
$ARRAY_MONTH_TH[9] = "กันยายน";
$ARRAY_MONTH_TH[10] = "ตุลาคม";
$ARRAY_MONTH_TH[11] = "พฤศจิกายน";
$ARRAY_MONTH_TH[12] = "ธันวาคม";

$ARRAY_MONTH_ENG[1] = "Jan";
$ARRAY_MONTH_ENG[2] = "Feb";
$ARRAY_MONTH_ENG[3] = "Mar";
$ARRAY_MONTH_ENG[4] = "Apr";
$ARRAY_MONTH_ENG[5] = "May";
$ARRAY_MONTH_ENG[6] = "Jun";
$ARRAY_MONTH_ENG[7] = "Jul";
$ARRAY_MONTH_ENG[8] = "Aug";
$ARRAY_MONTH_ENG[9] = "Sep";
$ARRAY_MONTH_ENG[10] = "Oct";
$ARRAY_MONTH_ENG[11] = "Nov";
$ARRAY_MONTH_ENG[12] = "Dec";


### GET IP #####
function getip () {

		$IP_ADDRESS_X	= $_SERVER['HTTP_X_FORWARDED_FOR'];
		if (!empty($IP_ADDRESS_X)) {
			$IP_ADDRESS		= $_SERVER['REMOTE_ADDR'].":".$IP_ADDRESS_X;
		}else{
			$IP_ADDRESS		= $_SERVER['REMOTE_ADDR'];		
		
		}
return $IP_ADDRESS;
}

### GET IP #####

##### JAVASCRIPT ALERT #####
function alert($text) {
	$text = str_replace("\n","\\n",$text);
echo "
<script language=\"Javascript\">
alert(\"$text\");
</script>
";

}
##### JAVASCRIPT ALERT #####

##### REDIRECT PAGE #####
function redirect ($gopage) {
echo "
<html>
<head>
<title></title>
<meta http-equiv=\"refresh\" content=\"0;URL=$gopage\">
</head>
<body bgcolor=\"#FFFFFF\">
</body>
</html>	
";
}
##### REDIRECT PAGE #####




##### RANDOM FUNCTION #####

function get_random() 
{ 
	srand((double)microtime()*100000000); 
    $max	= getrandmax(); 
	$max1	= rand(1,$max);
	$max	= getrandmax(); 
	$max2	= rand(1,$max);
    return "$max1$max2";
} 

function random_number($len=5, $str='') {

	for($i=1; $i<=$len; $i++) {
		$ord=rand(48, 57);
		$str.=chr($ord);
	}
	return $str;

}

//เก็บตก function สุ่มตัวอักษร
function random_string($len=5, $str='') {
for($i=1; $i<=$len; $i++) {
$ord=rand(48, 90);
if((($ord >= 48) && ($ord <= 57)) || (($ord >= 65) && ($ord<= 90)))
$str.=chr($ord);
else
$str.=random_string(1);
}
return $str;
}


##### RANDOM FUNCTION #####

##### DELETE FILE #####
function deletedata($file) {

#chmod($file,0777);

	if (is_dir($file)) {
	
	$handle = opendir($file); 
	
		while($filename = readdir($handle)) {
			if ($filename != "." && $filename != "..") {
			deletedata($file."/".$filename);
			}
		}

	closedir($handle);
	rmdir($file);

}else{
	unlink($file);
}

}
##### DELETE FILE #####


##########  UTF2TIS ###################

function UTF2TIS($utf8) {

$tis = "";
$utf8 = preg_split("//",$utf8);

$last_utf8 = count($utf8);
for ($i=0;$i<=$last_utf8; $i++) {

	$temp = ord ($utf8[$i] );
	if($temp > 127) { 
		if(($temp >= 0xc0) && ($temp <= 0xfd)) { # >= 192 && <=253    			
		
			$temp2 = ord ($utf8[$i+1]);
			$temp3 = ord ($utf8[$i+2]);
			if(($temp != 224) && (($temp2 < 184) || ($temp2 > 187))) { last; } # not Thai 
			
			$temp	-=	224;	$temp	*=	 16384;
			$temp2	-=	128;	$temp2	*=	 64;
			$temp3	-=	128;

			$total	= $temp + $temp2 + $temp3;
			$total = $total - 0xe00 + 0xa0; # convert UTF -> TIS

			$tis .= chr ($total); # output ascii equivalent 
		}
	}
	else { 
		if ($temp !=0) {
		$tis .= chr($temp); # standard 7 bit ascii
		}
	}
}

return $tis;

}


##########  UTF2TIS ###################


############ TIS2UTF ######################

function TIS2UTF($string) {

	$string = preg_replace("/&amp;/", "&", $string);
	$string = preg_replace("/&quot;/", "\"", $string);

	for( $i=0 ; $i< strlen($string) ; $i++ ){
	$s = substr($string, $i, 1);
	$val = ord($s);
	
	if($val < 0x80){
	    $utf8 .= $s;
	}
	else if (( 0xA1 <= $val && $val <= 0xDA ) || ( 0xDF <= $val && $val <= 0xFB )){

			$unicode = 0x0E00 + $val - 0xA0;
			$utf8 .= chr( 0xE0 | ($unicode >> 12) );
			$utf8 .= chr( 0x80 | (($unicode >> 6) & 0x3F) );
			$utf8 .= chr( 0x80 | ($unicode & 0x3F) );
		}
    }

	$utf8 = preg_replace("/&/", "&amp;", $utf8);
    return $utf8;
}

############ TIS2UTF ######################

	function getMicroTime(){ 
	   list($usec, $sec) = explode(" ",microtime()); 
	   return ((float)$usec + (float)$sec); 
    } 

	function startTime() {
		global $start_MicroTime;
		$start_MicroTime	=getMicroTime();

	}

	function stopTime() {
		global $start_MicroTime;
		global $stop_MicroTime;
		global $result_MicroTime;
		$stop_MicroTime		=getMicroTime();
		$result_MicroTime	=$stop_MicroTime-$start_MicroTime;
		$result_MicroTime	=sprintf("%3.4f", $result_MicroTime);
	}

	function DateAdd($datestr, $num, $unit) {
		   $units = array("Y","m","d","H","i","s");
		   $unix = strtotime($datestr);

		   while(list(,$u) = each($units)) $$u = date($u, $unix);
		   $$unit += $num;
		   return mktime($H, $i, $s, $m, $d, $Y);
	}


function LastDay($m, $y) {
  for ($i=29; $i<=32; $i++) {
    if (checkdate($m, $i, $y) == 0) {
      return $i - 1;
    }
  }
}




function datediff($date1,$date2)
{
  $s=strtotime($date2) - strtotime($date1);

  $m=intval($s/60);
  $s=$s%60;

  $h=intval($m/60);
  $m=$m%60;

  $d=intval($h/24);
  $h=$h%24;

  $diff=""; 
  if(!$d=="0") $diff.="$d";
  if($diff=="") $diff="-";
  trim($diff); 
  return $diff;
}



function is_utf8($str) {
    $c=0; $b=0;
    $bits=0;
    $len=strlen($str);
    for($i=0; $i<$len; $i++){
        $c=ord($str[$i]);
        if($c > 128){
            if(($c >= 254)) return false;
            elseif($c >= 252) $bits=6;
            elseif($c >= 248) $bits=5;
            elseif($c >= 240) $bits=4;
            elseif($c >= 224) $bits=3;
            elseif($c >= 192) $bits=2;
            else return false;
            if(($i+$bits) > $len) return false;
            while($bits > 1){
                $i++;
                $b=ord($str[$i]);
                if($b < 128 || $b > 191) return false;
                $bits--;
            }
        }
    }
    return true;
}


/*
** Function: text2links (PHP)
** Desc: Converts plain-links into HTML-Links
** Example: 
** Author: Jonas John
*/

function text2links($str='') {

    if($str=='' or !preg_match('/(http|www\.|@)/i', $str)) { return $str; }
    
    $lines = explode("\n", $str); $new_text = '';
    while (list($k,$l) = each($lines)) { 
        // replace links:
        $l = preg_replace("/([ \t]|^)www\./i", "\\1http://www.", $l);
        $l = preg_replace("/([ \t]|^)ftp\./i", "\\1ftp://ftp.", $l);
        
        $l = preg_replace("/(http:\/\/[^ )\r\n!]+)/i", 
            "<a href=\"\\1\" target='_blank' class='linkongray'>\\1</a>", $l);
        
        $l = preg_replace("/(https:\/\/[^ )\r\n!]+)/i", 
            "<a href=\"\\1\" target='_blank' class='linkongray'>\\1</a>", $l);
        
        $l = preg_replace("/(ftp:\/\/[^ )\r\n!]+)/i", 
            "<a href=\"\\1\">\\1</a>", $l);
        
        $l = preg_replace(
            "/([-a-z0-9_]+(\.[_a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)+))/i", 
            "<a href=\"mailto:\\1\">\\1</a>", $l);
            
        $new_text .= $l."\n";
    }
    
    return $new_text;
}


function checkmail($email) {
	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)) {
		$return = false;
	}
	else{
		$return = true;
	}

return $return;
}

/*
* parse_youtube_url() PHP function
* Author: takien
* URL: http://takien.com
*
* @param string $url URL to be parsed, eg:
* http://youtu.be/zc0s358b3Ys,
* http://www.youtube.com/embed/zc0s358b3Ys
* http://www.youtube.com/watch?v=zc0s358b3Ys
* @param string $return what to return
* - embed, return embed code
* - thumb, return URL to thumbnail image
* - hqthumb, return URL to high quality thumbnail image.
* @param string $width width of embeded video, default 560
* @param string $height height of embeded video, default 349
* @param string $rel whether embeded video to show related video after play or not.
*/
function parse_youtube_url($url, $return='', $width='', $height='', $rel=0){
	$urls = parse_url($url);

	//url is http://youtu.be/xxxx
	if($urls['host'] == 'youtu.be'){
		$id = ltrim($urls['path'], '/');
	}
	//url is http://www.youtube.com/embed/xxxx
	else if(strpos($urls['path'],'embed') == 1){
		$id = end(explode('/',$urls['path']));
	}
	 //url is xxxx only
	else if(strpos($url,'/')===false){
		$id = $url;
	}
	//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
	//url is http://www.youtube.com/watch?v=xxxx
	else{
		parse_str($urls['query']);

		if (empty($v) && !empty($feature)){
			$id = end(explode('v=',$urls['query']));
		} else {
			$id = $v;
		}
	}
	$id = substr($id, 0, 11);

	if (is_array($return) || $return == 'all') {
		$arr_all = array();
		$arr_all['code'] = $id;
		$arr_all['embed'] = '<iframe width="'.($width?$width:560).'" height="'.($height?$height:349).'" src="http://www.youtube.com/embed/'.$id.'?rel='.$rel.'" frameborder="0" allowfullscreen></iframe>';
		$arr_all['url'] = 'http://www.youtube.com/watch?v='.$id.'&rel='.$rel;
		$arr_all['url_embed'] = 'http://www.youtube.com/embed/'.$id.'?rel='.$rel;
		$arr_all['thumb'] = 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';
		$arr_all['hqthumb'] = 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';

		$arr_return = array();
		if (!empty($return) && is_array($return)) {
			foreach($return as $item) {
				if(isset($arr_all[$item])) $arr_return[$item] = $arr_all[$item];
			}
		}
		if (empty($arr_return)) {
			$arr_return = $arr_all;
		}

		return $arr_return;
	}
	//return embed iframe
	else if($return == 'embed'){
		return '<iframe width="'.($width?$width:560).'" height="'.($height?$height:349).'" src="http://www.youtube.com/embed/'.$id.'?rel='.$rel.'" frameborder="0" allowfullscreen></iframe>';
	}
	//return url embed iframe
	else if($return == 'url_embed'){
		return 'http://www.youtube.com/embed/'.$id.'?rel='.$rel;
	}
	//return normal thumb
	else if($return == 'url'){
		return 'http://www.youtube.com/watch?v='.$id.'&rel='.$rel;
	}
	//return normal thumb
	else if($return == 'thumb'){
		return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';
	}
	//return hqthumb
	else if($return == 'hqthumb'){
		return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';
	}
	// else return id
	else{
		return $id;
	}
}

function get_youtube_info($youtube_id) {
	$info = array();
	$entry = @simplexml_load_file('http://gdata.youtube.com/feeds/api/videos/' . $youtube_id);

	if ($entry) {
		$media = $entry->children('http://search.yahoo.com/mrss/');

		$info['title'] = (string)$media->group->title;
		$info['description'] = (string)$media->group->description;
	}
	return $info;
}

function web_sendmail($to_name,$to_email,$from_name,$from_email,$subject,$body_html) {


$from_name	=UTF2TIS($from_name);
$from_email	=UTF2TIS($from_email);
$to_name	=UTF2TIS($to_name);
$to_email	=UTF2TIS($to_email);
$subject	=UTF2TIS($subject);
$body_html	=UTF2TIS($body_html);

$headers  = "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=windows-874\n";
$headers .= "From: ". $from_name ." <".$from_email.">\n"; 
$headers .= "X-Priority: 1\r\n"; 
$headers .= "X-MSMail-Priority: High\r\n"; 
$headers .= "X-Mailer: Just My Server"; 
mail($to_email, $subject, $body_html, $headers);

}



function generate_seo_link($input, $replace = '-', $remove_words = true, $words_array = array()) {

	$input	=strtolower($input);
	$input	=trim($input);
	$input	=preg_replace('/ /', '-',$input);
	$input	=preg_replace('/[^a-zA-Z0-9ก-ฮฯ-ูเ๙\-\s]/', '',$input);
	$return	=trim($input);

	if($remove_words) { $return = remove_words($return, $replace, $words_array); }


	return str_replace(' ', $replace, $return);
}

/* takes an input, scrubs unnecessary words */
function remove_words($input,$replace,$words_array = array(),$unique_words = true)
{
	//separate all words based on spaces
	$input_array = explode(' ',$input);

	//create the return array
	$return = array();

	//loops through words, remove bad words, keep good ones
	foreach($input_array as $word)
	{
		//if it's a word we should add...
		if(!in_array($word,$words_array) && ($unique_words ? !in_array($word,$return) : true))
		{
			$return[] = $word;
		}
	}

	//return good words separated by dashes
	return implode($replace,$return);
}

function coverttime_y_m_d_text($date) {
	global $ARRAY_MONTH_ENG;
list($TOPIC_YEAR,$TOPIC_MONTH,$TOPIC_DAY)=explode(":", preg_replace("[' '|-|T]",":", $date));
$TOPIC_MONTH = $ARRAY_MONTH_ENG[$TOPIC_MONTH*1];
$TOPIC_YEAR =$TOPIC_YEAR-2000;
$sendtime	="$TOPIC_DAY $TOPIC_MONTH $TOPIC_YEAR";
return $sendtime;
}

function coverttime_y_m_d_text2($date) {
	global $ARRAY_MONTH_TH;
list($TOPIC_YEAR,$TOPIC_MONTH,$TOPIC_DAY,$TOPIC_TEMP)=explode(":", preg_replace("[' '|-|T|:| ]",":", $date));

$TOPIC_MONTH = $ARRAY_MONTH_TH[$TOPIC_MONTH*1];
$TOPIC_YEAR =$TOPIC_YEAR+543;
$sendtime	="$TOPIC_DAY $TOPIC_MONTH $TOPIC_YEAR";
return $sendtime;
}

function coverttime_y_m_d($date) {
list($TOPIC_DAY,$TOPIC_MONTH,$TOPIC_YEAR)=explode(":", preg_replace("[' '|-|T]",":", $date));
$sendtime	="$TOPIC_YEAR-$TOPIC_MONTH-$TOPIC_DAY";
return $sendtime;
}

function coverttime_y_m_d_h_m_s($date) {

$date1 = preg_replace("/(.*?) (.*)/i","$1",$date);
$time1 = preg_replace("/(.*?) (.*)/i","$2",$date);
list($TOPIC_DAY,$TOPIC_MONTH,$TOPIC_YEAR,$TOPIC_HOUR,$TOPIC_MIN,$TOPIC_SEC)=explode(":", preg_replace("[' '|-]",":", $date1));
$sendtime	="$TOPIC_YEAR-$TOPIC_MONTH-$TOPIC_DAY $time1";
return $sendtime;
}


function coverttime_d_m_y($date) {
list($TOPIC_DAY,$TOPIC_MONTH,$TOPIC_YEAR)=explode(":", preg_replace("[' '|-|T]",":", $date));
$sendtime	="$TOPIC_DAY-$TOPIC_MONTH-$TOPIC_YEAR";
return $sendtime;
}

?>