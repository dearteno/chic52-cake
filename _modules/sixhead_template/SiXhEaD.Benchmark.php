<?php

if (!defined('IN_SITE')) die('Direct Access to this location is not allowed.');

/*
SiXhEaD Benchmark - PHP Benchmark
Copyright (c) 2005, Chairath Soonthornwiphat (sixhead.com). All rights reserved.
Code licensed under the BSD License: (license.txt)
Version 0.1.1
*/
class BenchMark {

	function __construct () {
		$this->_set_start_time();
	}

	function _set_start_time () {
		$time = microtime();
		$time = explode(" ", $time);
		$time = $time[1] + $time[0];
		$this->start = $time;
	}
	
	function _get_total_time () {
		$time = microtime();
		$time = explode(" ", $time);
		$time = $time[1] + $time[0];
		$finish = $time;
		$totaltime = ($finish - $this->start);
		return number_format($totaltime, 4, '.', '');	
	}

	function _get_memory_usage () {
		return number_format($this->bytesTo(memory_get_usage(),"KB"),2);
	}

	function result () {
		return "This page took " . $this->_get_total_time() . " seconds to load. Memory Usage " . $this->_get_memory_usage() . " KB<br />";
	}

	function display () {
		echo $this->result();
	}

	function bytesTo($value, $unit) {
		/*
		b  = bits        B  = Bytes
		Kb = Kilobits    KB = Kilobytes
		Mb = Megabits    MB = Megabytes
		Gb = Gigabits    GB = Gigabytes
		*/

		$k = 1024;
		$m = 1048576;
		$g = 1073741824;

		switch (trim($unit)) {
		case "b": //bytes
			return ($value * 8);
		case "Kb": //Kilobits
			return (($value * 8) / $k);
		case "Mb": // Megabits
			return (($value * 8) / $m);
		case "Gb": // Gigabits
			return (($value * 8) / $g);
		case "B": // Bytes
			return $value;
		case "KB": // Kilobytes
			return ($value / $k);
		//return bcdiv($value, $k, 4);
		case "MB": // Megabytes
			return ($value / $m);
		case "GB": // Gigabytes
			return ($value / $g);
		default: return 0;
		}
	}
}

?>