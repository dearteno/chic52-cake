<?php
ini_set("display_errors", true);
#error_reporting(E_ALL);

require "SiXhEaD.Template.php";

$tp = new Template("_tp_demo.array.html");
$tp->block("table1");
for ($i=1;$i<=5;$i++) {

	$column[0]	=	"AAA " . $i;
	$column[1]	=	"BBB " . $i;
	$column[2]	=	"CCC " . $i;
	$column[3]	=	"DDD " . $i;

	$tp->apply();
}

$tp->block("data_sub1");
$tp->sub(3);
for ($i=11;$i<=18;$i++) {
	$columnSub1[0][0]	=	"A.$i";
	$columnSub1[0][1]	=	"B.$i";
	$columnSub1[0][2]	=	"C.$i";

	$tp->apply();
}

$tp->block("data_sub2");
$tp->sub(4);
for ($i=21;$i<=32;$i++) {
	$columnSub2["Data"]	=	$i;

	$tp->apply();
}


$config[A1] = "A1 A1 A1";
$config[A2] = "A2 A2 A2";

$config2["B1"][0] = "B1 B1 B1";
$config2["B2"][1] = "B2 B2 B2";

$summary[0] = 50;
$summary[1] = 200;

$days[sunday][morning]			=	"07:00";
$days["monday"]["afternoon"]	=	"13:00";
$days[tuesday][evening]			=	"17:00";

$data["aa"]		=	"AAA";
$data['bb']		=	"BBB";
$data[cc]		=	"CCC";
$data['dd'][1]	=	"DDD";
$data["ee"][0]	=	"EEE";
$data[5][2]		=	"5-2";
$data[5]["a"]	=	"5-a";

$tp->display();
exit;
?>