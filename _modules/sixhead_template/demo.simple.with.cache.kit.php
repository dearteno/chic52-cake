<?php

require "SiXhEaD.Template.php";
require "cache-kit.php";

$cache_active	=	true;
$cache_folder	=	"./";


$strCacheName	=	md5($_SERVER["REQUEST_URI"]);
$cacheResult	=	acmeCache::fetch($strCacheName, 60); # sec
if($cacheResult){
	echo $cacheResult;
	exit;
}



$tp = new Template("_tp_demo.simple.html");
$tp->block("table1");
for ($i=1;$i<=5;$i++) {

	$column1	=	"AAA " . $i;
	$column2	=	"BBB " . $i;
	$column3	=	"CCC " . $i;
	$column4	=	"DDD " . $i;

	$tp->apply();
}

$DateTime = date("Y-m-d H:i:s");

acmeCache::save($strCacheName, $tp->generate());

$tp->display();
exit;

?>