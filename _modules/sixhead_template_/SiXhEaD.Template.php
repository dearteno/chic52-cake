<?php

if (!defined('IN_SITE')) die('Direct Access to this location is not allowed.');

/*
SiXhEaD Template - PHP5 Template Engine for Programmer & Designer
Copyright (c) 2005, Chairath Soonthornwiphat (sixhead.com). All rights reserved.
Code licensed under the BSD License: (license.txt)
Version 5.7
*/
class Template
{
	var $template_file = "";
	var $template_key = "";

	function __construct ($template_file,$template_key="SiXhEaD")
	{
		$this->template_file=	$template_file;
		$this->template_key	=	$template_key;
		$this->_start();
	}

	function _start ()
	{
		$fp = fopen($this->template_file,"r");
		if (!$fp)
		{ 
			$this->_debug ("Template file \"$this->template_file\" not found");
		}
		$html = fread($fp,filesize($this->template_file)); fclose($fp);
		$html = $this->_encode_special($html);

		$this->set_global(true);
		$this->all_html = $html;
		$this->_set_key($this->template_key);
		$this->_auto_hide();
	}

	function assign ($key,$value)
	{
		$this->tp_vars["$key"] = $value;
	}

	function set_global ($is_global)
	{
		$this->is_global = $is_global;
	}
	
	function _set_key ($template_key)
	{
		$this->template_key = $template_key;
	}

	function _auto_hide ()
	{
		if (preg_match_all("/<!--\/$this->template_key:(.*?)-->/",$this->all_html, $all_block_names))
		{
			foreach ($all_block_names[1] as $block_name) 
			{
				$this->block($block_name);
			}
		}
	}
	
	function block ($block_name)
	{
		$this->block[$block_name]["hide"]	=	0;
		$this->block[$block_name]["apply"]	=	"";

		$this->block_name = $block_name; # set current block Name
		if (preg_match("/<!--$this->template_key:$block_name-->(.*?)<!--\/$this->template_key:$block_name-->/",$this->all_html, $matches))
		{
			$this->block[$block_name]["html"]	=	$matches[1];
			$this->hide();
			$this->sub(0);
		}
		else
		{
			$this->_debug ("&lt;!--$this->template_key:$block_name--&gt;............&lt;!--/$this->template_key:$block_name--&gt; not found");
		}
	}

	function hide ()
	{
		$this->block[$this->block_name]["hide"]	=	1;
	}

	function apply ()
	{
		$this->block[$this->block_name]["hide"]		=	0;
		$this->block[$this->block_name]["apply"]	.=	$this->_encode_dollar($this->apply_block($this->block_name));
	}
		
	function apply_block ($block_name)
	{ # apply $VAR to block
		return $this->_set_value($this->block_html($block_name));
	}

	function block_html ($block_name)
	{ # Get HTML from block
		if ($this->all_sub_in_block > 0)
		{ # sub block Template
			$this->_sub_count();
			return $this->_decode_special($this->block[$block_name]["sub_html"][$this->current_sub]);
		}
		else
		{
			return $this->_decode_special($this->block[$block_name]["html"]);
		}
	}

	function sub ($all_sub_in_block)
	{
		$this->all_sub_in_block = $all_sub_in_block;
		$this->_sub_clear();
		if ($this->all_sub_in_block > 0)
		{
			for ($current_sub=1;$current_sub<=$all_sub_in_block;$current_sub++)
			{
				if (preg_match("/<!--SUB:$current_sub-->(.*?)<!--\/SUB:$current_sub-->/i",$this->block[$this->block_name]["html"], $matches))
				{
					$this->block[$this->block_name]["sub_html"][$current_sub]	=	$matches[1];
				}
				else
				{
					$this->_debug ("&lt;!--SUB:$current_sub--&gt;............&lt;!--/SUB:$current_sub--&gt; not found in block : " . $this->block_name);
				}
			}
		}
	}

	function _sub_clear ()
	{
		$this->current_sub = 0;
	}

	function _sub_count ()
	{
		if ($this->current_sub == $this->all_sub_in_block)
		{ 
			$this->_sub_clear();
		}
		$this->current_sub += 1;
	}

	function apply_sub ($block_name,$sub_no)
	{
		return $this->_set_value($this->_sub_html($block_name,$sub_no));
	}
	
	function _sub_html ($block_name,$sub_no)
	{
		return $this->_decode_special($this->block[$block_name]["sub_html"][$sub_no]);
	}

	function get_current_sub()
	{
		$current_sub = $this->current_sub + 1;
		if ($current_sub > $this->all_sub_in_block)
		{ 
			$current_sub = 1;
		}
		return $current_sub;
	}

	function get_current_sub_total ()
	{
		return $this->all_sub_in_block;
	}

	function generate ()
	{
		$html	=	$this->all_html;
		if (is_array($this->block))
		{
			foreach (array_keys($this->block) as $block_name)
			{
				$gen_block_name[$block_name] = "/<!--$this->template_key:$block_name-->(.*?)<!--\/$this->template_key:$block_name-->/";
				if ($this->block[$block_name]["hide"] == 1) 
				{ 
					$gen_block_apply[$block_name] = "";
				}
				else
				{ 
					$gen_block_apply[$block_name] = $this->block[$block_name]["apply"];
				}
			}
			$html = preg_replace($gen_block_name, $gen_block_apply, $html);		
		}

		$html	=	$this->_set_value($html);
		$html	=	$this->_decode_special($html);
		$html	=	$this->_decode_dollar($html);	
		return $html;
	}

	function display ()
	{
		echo $this->generate();
	}


	function _set_value ($html)
	{
		if ($this->is_global == TRUE)
		{
			$html	=	preg_replace("/\\$(\w+)\[(\"|\'|)(\w+)(\"|\'|)\]\[(\"|\'|)(\w+)(\"|\'|)\]/e","\$GLOBALS['$1']['$3']['$6']",$html);
			$html	=	preg_replace("/\\$(\w+)\[(\"|\'|)(\w+)(\"|\'|)\]/e","\$GLOBALS['$1']['$3']",$html);
			$html	=	preg_replace("/\\$(\w+)/e","\$GLOBALS['$1']",$html);
		}
		else
		{
			$html	=	preg_replace("/\\$(\w+)/e","\$this->tp_vars['$1']",$html);
		}
		return $html;
	}

	function __destruct() 
	{
    }

	function _encode_special($string)
	{
		$string	=	str_replace("\r", "%0D", $string);
		$string	=	str_replace("\n", "%0A", $string);
		$string	=	str_replace("\t", "%09", $string);
		return $string;
	}
	
	function _decode_special($string)
	{
		$string	=	str_replace("%0D", "\r", $string);
		$string	=	str_replace("%0A", "\n", $string);
		$string	=	str_replace("%09", "\t", $string);
		return $string;
	}

	function _encode_dollar($string)
	{
		return str_replace("\$", "%24", $string);
	}

	function _decode_dollar($string)
	{
		return str_replace("%24", "$", $string);
	}

	function _debug ($string)
	{
		echo "
<br/><strong>$this->template_key Template debug message</strong><br/><br/>
$string
";
		exit;
	}
}
?>