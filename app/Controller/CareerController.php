<?php

class CareerController extends AppController {

    var $uses       = array('Career');
    var $helpers    = array('Html','Form');

	public function index() {

        $this->set('showLayoutContent', true);
        $this->set('careers',$this->Career->careerAll()) ;
        $this->layout = 'career';
        $this->render('career');

	}
}
