<?php

class CatalogController extends AppController {

    var $uses       = array('Catalog','CatalogImage');
    var $helpers    = array('Html','Form');

	public function index() {

        $this->set('showLayoutContent', true);
        $this->set('catalogs',$this->Catalog->Catalogs()) ;

        $this->layout = 'catalog';
        $this->render('catalog');

	}

    public function open($id=null) {

        $this->set('showLayoutContent', true);
        $this->set('images',$this->CatalogImage->open($id)) ;
        $this->set('image',$this->CatalogImage->first($id)) ;

        $this->layout = 'blank';
        $this->render('open');

    }
}
