<?php

class ContactController extends AppController {

    var $uses       = array('Contact');
    var $helpers    = array('Html','Form');

	public function index() {


        $this->set('showLayoutContent', true);
      // $this->set('contact',) ;

      //  $this->layout = 'about';
        $this->render('contact');

	}

  public function post() {

    if ($this->request->is('post')){


  // Initialize filename-variable
    $filename = null;
    $FILES =  $this->request->params['form'] ;

    if (
        !empty($FILES['filename']['tmp_name']) && is_uploaded_file($FILES['filename']['tmp_name'])
    ) {
        // Strip path information
       $filename = basename($FILES['filename']['name']) ;
       move_uploaded_file($FILES['filename']['tmp_name'], ROOT. DS .'_files'.DS. 'contact'.DS. $filename);
    }


      // Set the file-name only to save in the database
      $this->request->data['filename'] = $filename;
      $this->Contact->saveContact($this->request->data);

    }

    $this->set('showLayoutContent', true);
  // $this->set('contact',) ;

  //  $this->layout = 'about';
    $this->render('contact');

	}

}
