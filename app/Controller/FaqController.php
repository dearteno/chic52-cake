<?php

class FaqController extends AppController {

    var $uses       = array('Faq');
    var $helpers    = array('Html','Form');

	public function index() {


        $this->set('showLayoutContent', true);
        $this->set('faqs',$this->Faq->faqs()) ;

      //  $this->layout = 'about';
        $this->render('faq');

	}
}
