<?php

class HomeController extends AppController {

    var $uses       = array('Banner');
    var $helpers    = array('Html','Form');

	public function home() {

        $this->set('showLayoutContent', true);
        $this->set('banners',$this->Banner->findAll()) ;
        $this->set('bannerTops',$this->Banner->bannerTop()) ;
        $this->set('bannerBottoms',$this->Banner->bannerBottom()) ;
        $this->layout = 'home';
        $this->render('home');

	}
}
