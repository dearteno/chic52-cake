<?php

class LocationController extends AppController {

    var $uses       = array('Location');
    var $helpers    = array('Html','Form');

	public function index() {



        $this->set('showLayoutContent', true);
        $this->set('bangkoks',$this->Location->bangkoks()) ;
        $this->set('provinces',$this->Location->provinces()) ;
        $this->set('maps',$this->Location->maps()) ;

        $this->layout = 'location';
        $this->render('location');

	}
}
