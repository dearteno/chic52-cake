<?php

class ServiceController extends AppController {

    var $uses       = array('Service');
    var $helpers    = array('Html','Form');

	public function index() {

        $this->set('showLayoutContent', true);

        $this->set('services',$this->Service->services()) ;
        $this->set('count',$this->Service->count()) ;

        //$this->layout = 'service';
        $this->render('service');

	}
}
