<?php

class StoreController extends AppController {

    var $uses       = array('AboutImage');
    var $helpers    = array('Html','Form');

	public function index() {

        $this->set('showLayoutContent', true);

        $this->set('images',$this->AboutImage->image()) ;
        $this->set('count',$this->AboutImage->count()) ;

        $this->layout = 'store';
        $this->render('store');

	}
}
