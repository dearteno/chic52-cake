<?php

App::uses('AppModel', 'Model');

class About extends AppModel  {

    public $useTable = 'chic_about';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function findOne(){

        $conditions = array(
            "ID" => "1",
        );

        $order = array('ABOUT_SORT DESC');
        return  $this->find('first', array('conditions' => $conditions, 'order'=> $order));

    }

    public  function findStore(){

        $conditions = array(
            "ID" => "2",
        );

        $order = array('ABOUT_SORT DESC');
        return  $this->find('first', array('conditions' => $conditions, 'order'=> $order));

    }

}
