<?php

App::uses('AppModel', 'Model');

class AboutImage extends AppModel  {

    public $useTable = 'chic_about_image';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }


    public  function image(){

        $conditions = array(
            "RESOURCE_ID" => "2",
        );

        $order = array('RESOURCE_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

    public  function count(){

        $conditions = array(
            "RESOURCE_ID" => "2",
        );

        $order = array('RESOURCE_SORT DESC');
        return  $this->find('count', array('conditions' => $conditions, 'order'=> $order));

    }

}
