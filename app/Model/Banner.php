<?php

App::uses('AppModel', 'Model');

class Banner extends AppModel  {

    public $useTable = 'chic_banner';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function bannerTop(){

        $conditions = array(
            "BANNER_STATUS" => "S",
            "BANNER_TYPE" => 1,
        );

        $order = array('BANNER_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }


    public  function bannerBottom(){

        $conditions = array(
            "BANNER_STATUS" => "S",
            "BANNER_TYPE" => 3,
        );

        $order = array('BANNER_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

}
