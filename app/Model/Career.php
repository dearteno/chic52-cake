<?php

App::uses('AppModel', 'Model');

class Career extends AppModel  {

    public $useTable = 'chic_career';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function careerAll(){

        $conditions = array(
            "CONTENT_STATUS" => "S",

        );

        $order = array('CONTENT_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }



}
