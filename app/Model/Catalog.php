<?php

App::uses('AppModel', 'Model');

class Catalog extends AppModel  {

    public $useTable = 'chic_catalog';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function catalogs(){

        $conditions = array(
            "CONTENT_STATUS" => "S",
        );

        $order = array('CONTENT_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

    public  function open($id){

        $conditions = array(
            "CONTENT_STATUS" => "S",
            "ID" => $id,
        );

        $order = array('CONTENT_SORT DESC');
        return  $this->find('first', array('conditions' => $conditions, 'order'=> $order));

    }

}
