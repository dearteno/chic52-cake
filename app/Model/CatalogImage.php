<?php

App::uses('AppModel', 'Model');

class CatalogImage extends AppModel  {

    public $useTable = 'chic_catalog_image';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function open($id){

        $conditions = array(
        //    "RESOURCE_STATUS" => "S",
            "RESOURCE_ID" => $id,
        );

        $order = array('RESOURCE_SORT ASC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

    public  function first($id){

        $conditions = array(
            //    "RESOURCE_STATUS" => "S",
            "RESOURCE_ID" => $id,
        );

        $order = array('RESOURCE_SORT ASC');
        return  $this->find('first', array('conditions' => $conditions, 'order'=> $order));

    }

}
