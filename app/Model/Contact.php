<?php

App::uses('AppModel', 'Model');

class Contact extends AppModel  {

    public $useTable = 'chic_contact';
    public $primaryKey = 'ID';


    public $validate = array(

        'NAME' => array(
            'rule' => 'notEmpty'
        ),

        'EMAIL' => array(
            'rule' => 'email'
        ),
        'MESSAGE' => array(
            'rule' => 'notEmpty'
        )
    );

    public function findAll(){

       return  $this->find('all');

    }


  public function saveContact($postData = null){

    $data = array(
      'NAME' => $postData['name'] ,
      'TEL' =>  $postData['tel'],
      'EMAIL' => $postData['email'],
      'FILENAME' => isset($postData['filename']) ? $postData['filename'] : '',
      'MESSAGE' => $postData['message'],
      'ADDDATE' => date('Y-m-d H:i:s'),
      'IP_ADDRESS' => '127.0.0.1',
       );
 
    $this->create();
    $this->save($data);

    if ($this->validationErrors) {
      return $this->validationErrors ;
    }

    }
}
