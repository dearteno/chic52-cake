<?php

App::uses('AppModel', 'Model');

class Faq extends AppModel  {

    public $useTable = 'chic_faq';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function faqs(){

        $conditions = array(
            "CONTENT_STATUS" => "S",
        );

        $order = array('CONTENT_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

}
