<?php

App::uses('AppModel', 'Model');

class Location extends AppModel  {

    public $useTable = 'chic_map';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }

    public  function maps(){

        $conditions = array(
            "MAP_STATUS" => "S",
        );

        $order = array('MAP_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

    public  function bangkoks(){

        $conditions = array(
            "MAP_STATUS" => "S",
            "MAP_TYPE" => "1",
        );

        $order = array('MAP_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }
    public  function provinces(){

        $conditions = array(
            "MAP_STATUS" => "S",
            "MAP_TYPE" => "2",
        );

        $order = array('MAP_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

}
