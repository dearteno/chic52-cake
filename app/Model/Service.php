<?php

App::uses('AppModel', 'Model');

class Service extends AppModel  {

    public $useTable = 'chic_service';
    public $primaryKey = 'ID';

    public function findAll(){

       return  $this->find('all');

    }


    public  function services(){

        $conditions = array(
            "SERVICE_STATUS" => "S",
        );

        $order = array('SERVICE_SORT DESC');
        return  $this->find('all', array('conditions' => $conditions, 'order'=> $order));

    }

    public  function count(){

        $conditions = array(
            "SERVICE_STATUS" => "S",
        );

        $order = array('SERVICE_SORT DESC');
        return  $this->find('count', array('conditions' => $conditions, 'order'=> $order));

    }

}
