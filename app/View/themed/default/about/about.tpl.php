<article>
    <header class="header-content">
        <h1>
                    <span>
                        ABOUT US
                    </span>
        </h1>
        <div class="header-content__left  header-content__about-us">
            <h2>
                CHIC REPUBLIC
            </h2>

            <p>
                The 1st Home Fashion Store in Thailand<br>
                A shopping republic of unique furniture and home decorations
            </p>
        </div>
    </header>
    <div class="site-detail-content  content-about-us">
        <div class="content-about-us__change  font-lang-en">

            <a href="aboutus_eng">English</a><a  class="act" href="aboutus">Thai</a>
        </div>
        <figure>
            <img src="_files/images/full/<?php  echo $about['About']['ABOUT_PIC']?>" alt="">
        </figure>
        <p>
            <?php  echo $about['About']['ABOUT_INTRO_TH']?>
        </p>
        <p>
            <?php  echo $about['About']['ABOUT_CONTENT_TH']?>
        </p>
    </div>
</article>