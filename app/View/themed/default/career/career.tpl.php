<!-- #intro-pnl -->
<div role="main" id="main" class="wrapper  content-career" itemscope itemtype="http://schema.org/Product">
    <div class="font-lang-th-bold">
        <img src="img/banner/career-thumb.jpg" alt="">
        <p>สนใจร่วมงานกับเรา ส่ง<span class="font-lang-en"> RESUME </span>หรือ <span class="font-lang-en">CV</span> ของคุณได้ที่ </p>
        <address><span class="font-lang-en">E-mail : hr02@chicrepublicthai.com</span><br>
            สอบถามเพิ่มเติม โทร. 0 2514 7111 ต่อ 7128
        </address>
    </div>
    <ul class="font-lang-th-bold">
        <?php foreach( $careers as $career ) : ?>
            <li><span>
					 <?php echo $career['Career']['CONTENT_NAME'] ?>
                    (<?php echo $career['Career']['CONTENT_LOCATION'] ?>)   <br>
                    จำนวน <?php echo $career['Career']['CONTENT_NUM'] ?>    อัตรา
					<a class="application-form" href="#<?php echo  $career['Career']['ID'] ?>">
                        [คุณสมบัติ]
                    </a>
				</span></li>
        <?php endforeach;?>
    </ul>
</div>
<!-- role = main -->
<div style="display: none;">
    <?php foreach( $careers as $career ) : ?>
        <div id="<?php echo $career['Career']['ID'] ?>" class="light-box-career">
            <h1 class="font-lang-en">CAREER OPPORTUNITIES WITH US</h1>
            <article>
                <div class="light-box-career__detail">
                    <h2><?php echo $career['Career']['CONTENT_NAME'] ?>(<?php echo $career['Career']['CONTENT_LOCATION']?>)  <small>จำนวน <?php echo $career['Career']['CONTENT_NUM'] ?>    อัตรา</small></h2>
                    <p><?php echo $career['Career']['CONTENT_DETAIL'] ?></p>

                </div>
                <div class="light-box-career__form">
                    <address>สอบถามเพิ่มเติม โทร. 0 2514 7123</address>
                    <p>สนใจร่วมงานกับเรา ส่ง <span class="font-lang-en">RESUME</span> หรือ <span class="font-lang-en">CV</span> ของคุณได้จาก<br> แบบฟอร์ม ด้านล่างนี้</p>

                    <form action="">
                        <input type="hidden" name="career_id">
                        <h3 class="font-lang-en">SEND YOU RESUME / CV</h3>
                        <label for="">ชื่อ</label>
                        <input type="text">
                        <label for="">นามสกุล</label>
                        <input type="text">
                        <label for="">เบอร์โทร</label>
                        <input type="text">
                        <label for="">อีเมล์</label>
                        <input type="text">
                        <label  class="font-lang-en" for="">Resume /CV</label>
                        <input type="file">
                        <button  class="font-lang-en" type="submit">SEND</button>
                    </form>
                </div>
            </article>
        </div>
    <?php endforeach;?>
</div>