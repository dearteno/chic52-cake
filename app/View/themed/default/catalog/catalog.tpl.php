<article>
    <header class="header-content">
        <h1><span>CATALOGUE</span></h1>
        <div class="header-content__left  header-content__faq">
            <h2>DOWNLOAD CATALOGUE & BROCHURE</h2>
        </div>
    </header>
    <div class="site-detail-content  content-catalog">
        <ul>
            <?php foreach ($catalogs as $catalog): ?>
                <li>
                    <figure>
                        <img src="_files/catalog/thumb/<?php echo $catalog['Catalog']['CONTENT_FILE1'] ?>" alt="">
                    </figure>
                    <h1><?php echo $catalog['Catalog']['CONTENT_NAME'] ?></h1>
                    <p><?php echo $catalog['Catalog']['CONTENT_INTRO'] ?></p>
                    <p></p>
                    <ul>
                        <li>
                            <a class="open-cat" href="catalog/open/<?php echo $catalog['Catalog']['ID'] ?>">[OPEN]</a>
                        </li>
                        <li>
                            <a href="_files/catalog/download/<?php echo $catalog['Catalog']['CONTENT_FILE1'] ?>" target="_blank">[DOWNLOAD]</a>
                        </li>
                    </ul>
                </li>
            <?php endforeach ; ?>


        </ul>
    </div>
</article>