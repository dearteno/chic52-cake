

<!--<div class="zoom">
    <a href="#" class="close-btn">X Close</a>
    <img src="images/gallery-full/gallery-01.jpg" width="100%" >

</div>-->
<div class="ebook-wrapper">




    <div id="content-wrapper" class="gallery-wrapper">
        <div id="gallery-content">
            <div class="zoom-wrapper">
                <img id="zoom_07" src="" alt="" data-zoom-image="_files/catalog/full/<?php echo $image['CatalogImage']['RESOURCE_PIC'] ?>" />
            </div>

            <div class="back-page-shadow"></div>
            <div class="next-page-shadow"></div>

            <div class="main clearfix">
                <div class="bb-custom-wrapper">
                    <div id="bb-bookblock" class="bb-bookblock">
                        <?php foreach ($images as $image ): ?>
                        <div class="bb-item">
                            <a href="#">
                                <img src="_files/catalog/full/<?php echo $image['CatalogImage']['RESOURCE_PIC'] ?>" alt="<?php echo $image['CatalogImage']['RESOURCE_TOPIC'] ?>" />
                            </a>
                        </div>
                        <?php endforeach ; ?>
                    </div>
                </div>

            </div>
        </div>
        <nav class="bb-nav-wrapper">
            <a id="bb-nav-next" href="#" class="bb-custom-icon bb-custom-icon-arrow-right">Next &rarr;</a>
            <a id="bb-nav-prev" href="#" class="bb-custom-icon bb-custom-icon-arrow-left">&larr; Previous</a>
        </nav>
    </div>

</div>



<!--<img id="zoom_07" src="images/gallery/gallery-00.jpg" data-zoom-image="images/gallery-full/gallery-01.jpg"/>-->

<script>
    //alert('ddd');
    var currentPage = 0;

    var Page = (function() {

        var config = {
                $bookBlock: $('#bb-bookblock'),
                $navNext: $('.bb-custom-icon-arrow-right'),
                $navPrev: $('.bb-custom-icon-arrow-left'),
                $navFirst: $('#bb-nav-first'),
                $navLast: $('#bb-nav-last')
            },
            init = function() {
                config.$bookBlock.bookblock({
                    speed: 600,
                    shadowSides: 0.8,
                    shadowFlip: 0.7,
                    onEndFlip: function(old, page, isLimit) {
                       // alert(page);
                        currentPage = page + 1;
                       // alert(currentPage);
                        currentPageStr = ("0" + currentPage).slice(-2);
                       // alert(currentPageStr);
                        $('.zoom img').attr('src', './ebook/images/gallery-full/gallery-' + currentPageStr + '.jpg');

                        $('.zoomLens').css('background-image', 'url(./ebook/images/gallery-full/gallery-' + currentPageStr + '.jpg)');

                        //alert($('.zoomLens').css('background-image'));
                        return false;
                    },
                });
                initEvents();
            },
            initEvents = function() {

                var $slides = config.$bookBlock.children();

                // add navigation events
                config.$navNext.on('click touchstart', function() {
                    config.$bookBlock.bookblock('next');
                    return false;
                });

                config.$navPrev.on('click touchstart', function() {
                    config.$bookBlock.bookblock('prev');
                    return false;
                });

                config.$navFirst.on('click touchstart', function() {
                    config.$bookBlock.bookblock('first');
                    return false;
                });

                config.$navLast.on('click touchstart', function() {
                    config.$bookBlock.bookblock('last');
                    return false;
                });

                // add swipe events
                $slides.on({
                    'swipeleft': function(event) {
                        config.$bookBlock.bookblock('next');
                        return false;
                    },
                    'swiperight': function(event) {
                        config.$bookBlock.bookblock('prev');
                        return false;
                    }
                });

                $('.back-page-btn, .next-page-btn, .zoom-btn').on({
                    'swipeleft': function(event) {
                        config.$bookBlock.bookblock('next');
                        return false;
                    },
                    'swiperight': function(event) {
                        config.$bookBlock.bookblock('prev');
                        return false;
                    }
                });





                // add keyboard events
                $(document).keydown(function(e) {
                    var keyCode = e.keyCode || e.which,
                        arrow = {
                            left: 37,
                            up: 38,
                            right: 39,
                            down: 40
                        };

                    switch (keyCode) {
                        case arrow.left:
                            config.$bookBlock.bookblock('prev');
                            break;
                        case arrow.right:
                            config.$bookBlock.bookblock('next');
                            break;
                    }
                });
            };

        return {
            init: init
        };

    })();

    Page.init();

    $(document).ready(function() {

        var winW = $(window).width();
        if (winW > 468) {
            $("#zoom_07").elevateZoom({
                zoomType: "lens",
                lensShape: "round",
                lensSize: 200
            });
        }

    });
</script>