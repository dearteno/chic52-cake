<article>
    <header class="header-content">
        <h1><span>CONTACT US</span></h1>
        <div class="header-content__left  header-content__faq">
            <!--<h2>Customer Delight </h2>
            <p class="font-lang-th">ให้คำปรึกษาและบริการคุณอย่างเป็นส่วนตัวทั้งก่อนและหลังการเลือกซื้อ</p>-->
            <h2>We'll help you resolve your issues, please contact us right here</h2>
            <p class="font-lang-en">Operating Hours : Daily 10.00 AM - 21.30 PM</p>
            <p><strong>E-mail: customerdelight@chicrepublicthai.com </strong>
            </p>
        </div>
    </header>
    <div class="site-detail-content  content-contact-us">
        <div>
            <h1>Questions or comments?<br>Please contact us via the form below</h1>
            <p class="font-lang-th">ท่านสามารถสอบถามข้อส่งสัยของท่าน ได้จากแบบฟอร์มด้านล่างนี้</p>
            <form action="contact/post" method="post" enctype="multipart/form-data">
                <label for="">Name<span class="font-lang-th"> / ชื่อ :</span> </label>
                <input name="name" type="text">
                <label for="">Surname<span class="font-lang-th"> / นามสกุล :</span></label>
                <input name="surname" type="text">
                <label for="">Tel<span class="font-lang-th">/ โทรศัพท์ :</span> </label>
                <input name="tel" type="text">
                <label for="">E-mail<span class="font-lang-th">/ อีเมล์ :</span></label>
                <input name="email" type="text">
                <label for="">Attach file<span class="font-lang-th">/ แนบไฟล์</span></label>
                <input name="filename" type="file">
                <label for="">Message<span class="font-lang-th"> / ข้อความ :</span></label>
                <textarea name="message" id="" cols="30" rows="10"></textarea>
                <button type="submit">SEND</button>
            </form>
        </div>
    </div>
</article>
