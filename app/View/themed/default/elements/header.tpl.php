<div>
    <p id="logo"><a href="/"><span>CHIC REPUBLIC</span></a>
    </p>
    <div>
        <nav id="main-nav" role="navigation">
            <ul>
                <li>
                    <a href="/"><span>HOME</span></a>
                </li>
                <li>
                    <a href="#"><span>New Product</span></a>
                </li>
                <li>
                    <a href="product/category"><span>All Product</span></a>
                    <div class="sub-nav">
                        <ul>
                            <li>by Category
                                <ul>
                                    <li><a href="#">Living room</a>
                                    </li>
                                    <li><a href="#">Dinning Room</a>
                                    </li>
                                    <li><a href="#">Bedroom</a>
                                    </li>
                                    <li><a href="#">Home Office</a>
                                    </li>
                                    <li><a href="#">Beach &amp; Garden</a>
                                    </li>
                                    <li><a href="#">Home Decor</a>
                                    </li>
                                    <li><a href="#">Lighting</a>
                                    </li>
                                    <li><a href="#">Sleep Gallery</a>
                                    </li>
                                </ul>
                            </li>
                            <li>by Style
                                <ul>
                                    <li><a href="product/style">Romantic Vintage</a>
                                    </li>
                                    <li><a href="product/style">Modern Chic</a>
                                    </li>
                                    <li><a href="product/style">Elegant American</a>
                                    </li>
                                    <li><a href="product/style">Luxury Classic</a>
                                    </li>
                                    <li><a href="product/style">Beach &amp; Garden</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="promotion"><span>Promotion</span></a>
                </li>
                <li>
                    <a href="member"><span>Member</span></a>
                </li>
            </ul>
        </nav>
        <form id="site-search" action="*" method="post" enctype="multipart/form-data" role="search">
            <input type="text" name="search" placeholder="SEARCH">
            <input type="submit" name="go-search">
        </form>
    </div>
    <nav class="sns-link" role="navigation">
        <a class="sleep-link  hd-t" href="sleep">sleep gallery</a>
        <a href="#"><i class="icon-facebook-1"></i><span>Facebook</span></a>
        <a href="#"><i class="icon-twitter"></i><span>Twitter</span></a>
        <a href="#"><i class="icon-instagramm"></i><span>Instagram</span></a>
        <a href="#"><i class="icon-youtube"></i><span>YouTube</span></a>
    </nav>
</div>