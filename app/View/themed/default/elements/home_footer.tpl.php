<p class="home-promotion"></p>
<footer>
    <p>Copyright &copy; 2013 CHIC REPUBLIC Co.,Ltd. All right reserved.</p>
</footer>
<!-- Development -->
<?php echo $this->Html->script('vendor/jquery.min.js');?>

<script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
</script>

<?php echo $this->Html->script('vendor/jquery.easing.min.js'); ?>
<?php echo $this->Html->script('vendor/jquery.royalslider.js'); ?>
<?php echo $this->Html->script('vendor/jquery.bxslider.js'); ?>
<?php echo $this->Html->script('plugins.js'); ?>
<?php echo $this->Html->script('vendor/jquery.colorbox.js'); ?>

<script>
    $(document).ready(function () {
        $('.button-subscribe').colorbox({
            inline: true,
            closeButton: true
        });
    });
</script>




