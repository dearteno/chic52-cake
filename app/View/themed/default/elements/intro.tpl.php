 <div id="intro-pnl" itemscope itemtype="http://schema.org/Article">
        <!--
          พี่อั้ม: ตรงนี้ผมมองว่าเป็น 1 เรื่องนะครับสำหรับ intro ในหน้า page ของ category ของ
          product
        -->
          <header>
            <meta itemprop="articleSection" content="LUXURY CLASSIC">
            <!--
              พี่อั้มพ่น บรรทัดข้างบนหัว comment นี้เสมอนะครับส่วนที่ต้อง dynamic เปลี่ยนไปตาม
              หัวข้อคือ คำที่อยู่ใน content="" ในที่นี้คือ LUXURY CLASSIC (ตรงกับ h1 บรรทัด 62)
            -->
              <h1 style="background: url(img/products/luxury/head.png);" itemprop="name"><span>LUXURY<br>CLASSIC</span></h1>
          </header>
          <article itemprop="articleBody">
            <!-- ตรงนี้ที่ผมไม่มี PSD file นะครับผมขอใส่ lorem ipsum ไปก่อน -->
            <p>Make your living room the most comfortable room in your home! <br>
With stylish and elegant living room furniture from American Furniture Warehouse you're sure to find the perfect piece to match your style. Come discover why at American Furniture Warehouse, we make beautiful living rooms happen for less every day.</p>
          </article>
        </div> <!-- #intro-pnl -->

        <div role="main" id="main" class="wrapper" itemscope itemtype="http://schema.org/Product">
          <aside itemprop="url">
            <div id="product_gallery" class="royalSlider rsDefault">
              <a itemprop="image" class="rsImg" href="img/products/luxury/img-pd-01-01.jpg">
                <img src="img/products/luxury/thumb-pd-01-01.jpg" class="rsTmb" />
              </a>
              <a itemprop="image" class="rsImg" href="img/products/luxury/img-pd-01-01.jpg">
                <img src="img/products/luxury/thumb-pd-01-01.jpg" class="rsTmb" />
              </a>
            </div>
          </aside>
          <article>
            <section>
              <h2>สแตรนด์มูน <br>
                <em>เก้าอี้นวมวิงแชร์, Skiftebo สีส้ม</em>
              </h2>

              <h3>THB 8,990</h3>

              <p>
                <em>รายละเอียดสินค้าที่ปรากฏขึ้นกับขนาด สี และวัสดุที่คุณเลือก</em><br>
                รหัสสินค้า : 202.742.54
              </p>

              <p>ให้คุณเองพิงและพักผ่อนได้สบาย เพราะพนักพิงสูงที่ช่วยรองรับต้นคอได้ดี</p>
            </section>

            <section id="relate">
              <h4>Relate Products</h4>
              <ul>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-01.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-02.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-03.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-04.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-05.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-06.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-07.jpg" alt="สแตรนด์มูน"></a></li>
                <li><a href="luxury_classic_detail.php"><img src="img/products/luxury/thumb-cat-08.jpg" alt="สแตรนด์มูน"></a></li>
              </ul>
            </section>
          </article>
        </div> <!-- role = main -->