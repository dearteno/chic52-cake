<article>
    <header class="header-content">
        <h1><span>FAQ</span></h1>
        <div class="header-content__left  header-content__faq">
            <h2>Looking for the answers to our most popular questions</h2>
        </div>
    </header>

    <div class="site-detail-content  content-faq">
        <dl>
            <?php foreach ($faqs as $faq): ?>
                <dt><span class="font-lang-en"> <?php echo  $faq['Faq']['CONTENT_QUESTION'] ?> </span></dt>
                <dd>
                    <p><?php echo   $faq['Faq']['CONTENT_ANSWER'] ?></p>
                </dd>

            <?php endforeach;?>
        </dl>
    </div>
</article>