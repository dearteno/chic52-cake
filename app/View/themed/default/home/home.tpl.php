<div id="billboard" role="banner00" class="royalSlider rsDefault">

    <?php foreach ($bannerTops as $banner): ?>
       <figure>
            <a href="#"><img src="_files/images/full/<?php  echo $banner['Banner']['BANNER_PIC']?>" alt=""></a>
       </figure>
    <?php endforeach ;?>
</div>
<!-- #billboard -->
<div role="banner">
    <div id="highlight-product">
        <p class="button-arrival">
            <a href="#">new arrival</a>
        </p>
        <ul class="bxslider">
            <li>
                <a href="#">
                    <img src="img/products/pd_hl1.png" alt="CSF210" title="CSF210">
                    <ul class="fractional">
                        <li>MARTHA/2</li>
                        <li>2 seater sofa</li>
                        <li>(CSF210)</li>
                        <li>THB 24,000</li>
                        <li><strong>Special  THB 19,200</strong></li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl2.png" alt="CSF219" title="CSF219">
                    <ul class="fractional">
                        <li>VERANDA/2</li>
                        <li>2 seater sofa</li>
                        <li>(CSF219)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl3.png" alt="CSF332" title="CSF332">
                    <ul class="fractional">
                        <li>NORMAN/2</li>
                        <li>2 seater sofa</li>
                        <li>(CSF332)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl4.png" alt="CSF317" title="CSF317">
                    <ul class="fractional">
                        <li>COLLIN/1</li>
                        <li>2 seater sofa</li>
                        <li>(CSF317)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl5.png" alt="CSF210" title="CSF210">
                    <ul class="fractional">
                        <li>COLLIN/1</li>
                        <li>2 seater sofa</li>
                        <li>(CSF317)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl6.png" alt="CSF219" title="CSF219">
                    <ul class="fractional">
                        <li>COLLIN/1</li>
                        <li>2 seater sofa</li>
                        <li>(CSF317)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl7.png" alt="CSF332" title="CSF332">
                    <ul class="fractional">
                        <li>COLLIN/1</li>
                        <li>2 seater sofa</li>
                        <li>(CSF317)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="img/products/pd_hl8.png" alt="CSF317" title="CSF317">
                    <ul class="fractional">
                        <li>COLLIN/1</li>
                        <li>2 seater sofa</li>
                        <li>(CSF317)</li>
                        <li>THB 24,000</li>
                    </ul>
                </a>
            </li>
        </ul>
    </div>
    <div class="royalSlider rsDefault adslot-s">
        <?php foreach ($bannerBottoms as $banner): ?>
            <figure>
                <a href="<?php echo $banner['Banner']['BANNER_LINK'] ?>">
                    <a href="#"><img src="_files/images/thumb/<?php echo  $banner['Banner']['BANNER_PIC'] ?>" alt=""></a>
                </a>
            </figure>
        <?php endforeach ;?>
    </div>
</div>
