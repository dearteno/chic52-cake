<!DOCTYPE html>
<html class="no-js">

<?php echo $this->element('head', array('cache' => true)); ?>

<body class="category elegant">
<?php echo $this->element('menu_header', array('cache' => true)); ?>
<?php echo $this->element('header', array('cache' => true)); ?>

<div role="banner" >
    <img src="img/banner/career.jpg">
</div>
<!-- #billboard -->

<div id="intro-pnl-new" class="intro-pnl-new2" itemscope itemtype="http://schema.org/Article">
    <!--
      พี่อั้ม: ตรงนี้ผมมองว่าเป็น 1 เรื่องนะครับสำหรับ intro ในหน้า page ของ category ของ
      product
    -->
    <header>
        <meta itemprop="articleSection" content="ROMANTIC VINTAGE">
        <!--
          พี่อั้มพ่น บรรทัดข้างบนหัว comment นี้เสมอนะครับส่วนที่ต้อง dynamic เปลี่ยนไปตาม
          หัวข้อคือ คำที่อยู่ใน content="" ในที่นี้คือ LUXURY CLASSIC (ตรงกับ h1 บรรทัด 62)
        -->
        <h1 itemprop="name"><span>CAREERS</span></h1>
    </header>
    <article class="header-content__left  header-career" itemprop="articleBody">

        <h2 class="">Looking for a new challenging job, please apply via the form below</h2>
    </article>
</div>
<!-- #intro-pnl -->

<!-- Main content -->

    <?php
    if (isset($showLayoutContent)) {
        echo $content_for_layout;
    }
    ?>

<!-- End main content -->

<?php echo $this->element('footer', array('cache' => true)); ?>
<script>
    $(document).ready(function(){
        $('.application-form').colorbox({inline: true,closeButton: true});
    });
</script>
</body>

</html>
