<!DOCTYPE html>
<html class="no-js">

<?php echo $this->element('head', array('cache' => true)); ?>

<body class="category elegant">
<?php echo $this->element('menu_header', array('cache' => true)); ?>
<?php echo $this->element('header', array('cache' => true)); ?>

<!-- Main content -->
<div class="main-content-960">
    <?php
    //  if (isset($showLayoutContent)) {
    echo $content_for_layout;
    //  }
    ?>
</div>
<!-- End main content -->

<?php echo $this->element('footer', array('cache' => true)); ?>
<script>
    $(document).ready(function(){
        $('.open-cat').colorbox({closeButton: false,onClosed: function(){$('.zoomContainer').remove();}});
    });
</script>
<!--
<?php echo $this->Html->script('./ebook/js/bookblock/jquerypp.custom.js'); ?>
<?php echo $this->Html->script('./ebook/js/bookblock/jquery.bookblock.js'); ?>
<?php echo $this->Html->script('./ebook/js/elevatezoom/jquery.elevatezoom.js'); ?>
-->
<script src="./ebook/js/bookblock/jquerypp.custom.js"></script>
<script src="./ebook/js/bookblock/jquery.bookblock.js"></script>
<script src="./ebook/js/elevatezoom/jquery.elevatezoom.js"></script>
</body>

</html>
