<!DOCTYPE html>
<html class="no-js">

<?php echo $this->element('head', array('cache' => true)); ?>

<body class="category elegant">
<?php echo $this->element('menu_header', array('cache' => true)); ?>
<?php echo $this->element('header', array('cache' => true)); ?>

<!-- Main content -->
<div class="main-content-960">
    <?php
  //  if (isset($showLayoutContent)) {
        echo $content_for_layout;
  //  }
    ?>
</div>
<!-- End main content -->

<?php echo $this->element('footer', array('cache' => true)); ?>

</body>

</html>