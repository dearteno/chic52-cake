<article>
    <header class="header-content">
        <h1 class="text-long"><span>STORE<br>
                LOCATION </span></h1>

        <div class="header-content__left  header-content__location">
            <h2>Visit our stores to discover for something stunning, luxurious, classic and personal</h2>
        </div>
    </header>
    <div class="site-detail-content  content-location">
        <h1 class="hr">Bangkok</h1>
        <?php foreach ($bangkoks as $store): ?>
            <article>
                <figure>
                    <img src="_files/images/full/<?php echo $store['Location']['MAP_PIC1']?>" alt="" />

                </figure>
                <div>
                    <h1><?php echo $store['Location']['MAP_NAME']?></h1>
                    <address>
                        <p><?php echo $store['Location']['MAP_INTRO']?><br>
                            <?php echo $store['Location']['MAP_TIME']?><br>
                            <?php echo $store['Location']['MAP_TEL'] ?><br>
                            <?php echo $store['Location']['MAP_ADDRESS']?>
                        </p>
                        <a class="view-map" href="#map<?php echo $store['Location']['ID'] ?>">MAP</a>
                    </address>
                </div>
            </article>
        <?php endforeach ; ?>
        <h1 class="hr">Province</h1>
        <?php foreach ($provinces as $store): ?>
            <article>
                <figure>
                    <img src="_files/images/full/<?php echo $store['Location']['MAP_PIC1']?>" alt="" />

                </figure>
                <div>
                    <h1><?php echo $store['Location']['MAP_NAME']?></h1>
                    <address>
                        <p><?php echo $store['Location']['MAP_INTRO']?><br>
                            <?php echo $store['Location']['MAP_TIME']?><br>
                            <?php echo $store['Location']['MAP_TEL'] ?><br>
                            <?php echo $store['Location']['MAP_ADDRESS']?>
                        </p>
                        <a class="view-map" href="#map<?php echo $store['Location']['ID'] ?>">MAP</a>
                    </address>
                </div>
            </article>
        <?php endforeach ; ?>

    </div>
</article>
<div style="display: none">
    <?php foreach ($maps as $store): ?>
        <div id="map<?php echo['ID']?>" class="light-box-map">
            <h1>LOCATION MAP</h1>
            <figure>
                <img src="_files/images/full/<?php echo $store['Location']['MAP_PIC2']?>" alt="" />
            </figure>
            <h2><?php echo $store['Location']['MAP_NAME']?></h2>
            <p><?php echo  $store['Location']['MAP_INRO']?></p>
        </div>
    <?php endforeach; ?>
</div>