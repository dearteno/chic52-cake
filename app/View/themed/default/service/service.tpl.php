<article>
    <header class="header-content">
        <h1><span>SERVICE</span></h1>
        <div class="header-content__left  header-content__service">
            <h2>What services do you expect from us?</h2>
        </div>
    </header>
    <div class="site-detail-content  content-service">
        <?php foreach($services as $service) : ?>
            <article>
                <figure>
                    <img src="_files/images/thumb/<?php echo $service['Service']['SERVICE_PIC'] ?>" alt=""/>
                </figure>
                <div>
                    <h1 class="font-lang-en"><?php echo $service['Service']['SERVICE_TOPIC'] ?></h1>
                    <p class="font-lang-th"><?php echo $service['Service']['SERVICE_INTRO_TH'] ?>
                    </p>
                    <p class="font-lang-en"><?php echo $service['Service']['SERVICE_INTRO_EN'] ?></p>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
</article>