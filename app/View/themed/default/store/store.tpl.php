<article>
    <header class="header-content">
        <h1>
                    <span>
                        VISIT STORE
                    </span>
        </h1>

        <div class="header-content__left">


            <h2>Everything you need to surround yourself in good taste is right here <br>at <strong>CHIC REPUBLIC</strong></h2>
        </div>
    </header>
    <div class="site-detail-content content-visit-store">

        <div class="content-visit-store__list">
            <!-- 12 items per/li -->
            <ul class="store-slider">
                <?php $i = 1 ;?>
                <li>
                    <?php foreach($images as $image): ?>


                    <a href="_files/images/full/<?php echo $image['AboutImage']['RESOURCE_PIC']?>"><img src="_files/images/thumb/<?php echo $image['AboutImage']['RESOURCE_PIC']?>" alt=""/></a>

                    <?php if ((0 == $i%12) && ( $i !== $count)): ?>
                </li><li>
            <?php endif ;?>

            <?php if ( $i == $count): ?>
                </li>
            <?php  endif ;?>
            <?php $i++ ;?>
            <?php endforeach; ?>
            </ul>
        </div>
        <ul class="content-visit-store__nav">
            <li id="store-prev" class="prev"></li>
            <li id="store-next" class="next"></li>
        </ul>
    </div>
</article>