<p class="detail-promotion"></p>
<footer>
    <div class="wrapper">
        <form id="subscribe" method="post" action="*" enctype="multipart/form-data">
            <h1>JOIN US</h1>
            <label for="enewsletter">Subscribe E-Newsletter</label>
            <input type="email" placeholder="Email" name="email" id="enewsletter">
            <button class="footer-submit" type="submit">SUBMIT</button>
        </form>
        <section role="navigation">
            <h1>MENU</h1>
            <nav>
                <a href="#"><span>HOME</span></a>
                <a href="#"><span>About Us</span></a>
                <a href="#"><span>Careers</span></a>
                <a href="#"><span>Contact Us</span></a>
                <a href="#"><span>Catalogue</span></a>
            </nav>
        </section>
        <section role="navigation">
            <h1>by Catagory</h1>
            <nav>
                <a href="#"><span>Living Room</span></a>
                <a href="#"><span>Dining Room</span></a>
                <a href="#"><span>Bedroom</span></a>
                <a href="#"><span>Home Office</span></a>
                <a href="#"><span>Beach & Garden</span></a>
                <a href="#"><span>Home Decor</span></a>
                <a href="#"><span>Lighting</span></a>
            </nav>
        </section>
        <section role="navigation">
            <h1>by Style</h1>
            <nav>
                <a href="#"><span>Romantic Vintage</span></a>
                <a href="#"><span>Modern Chic</span></a>
                <a href="#"><span>Elegant American</span></a>
                <a href="#"><span>Luxury Classic</span></a>
                <a href="#"><span>Beach and Garden</span></a>
            </nav>
        </section>

        <nav class="sns-link" role="navigation">
            <a href="#"><i class="icon-facebook-1"></i><span>Facebook</span></a>
            <a href="#"><i class="icon-twitter"></i><span>Twitter</span></a>
            <a href="#"><i class="icon-instagramm"></i><span>Instagram</span></a>
            <a href="#"><i class="icon-youtube"></i><span>YouTube</span></a>
        </nav>
    </div>

    <p>Copyright &copy; 2013 CHIC REPUBLIC Co.,Ltd. All right reserved.</p>
</footer>
<div style="display: none;">

    <div id="lbSubscribe" class="subscribe">
        <div>
            <h1>JOIN US</h1>
            <p>Subscribe E-Newsletter</p>
        </div>
        <div>
            <form action="#">
                <input type="email" placeholder="Email" name="email" id="enewsletter">
                <button class="footer-submit" type="submit">SUBMIT</button>
            </form>
        </div>
    </div>
</div>
<!-- Development -->
<script src="js/vendor/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
</script>
<script src="js/vendor/jquery.easing.min.js"></script>
<script src="js/vendor/jquery.royalslider.js"></script>
<script src="js/vendor/jquery.bxslider.js"></script>
<script src="js/plugins.js"></script>
<script src="js/vendor/jquery.colorbox.js"></script>

<!-- Production -->
<!-- <script src="js/plugins.min.js"></script> -->


<script>
    $(document).ready(function () {
        $('.button-subscribe').colorbox({
            inline: true,
            closeButton: true
        });
    });
</script>




