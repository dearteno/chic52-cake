    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Chic Republic</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=1040, minimum-scale=0.5">
        <meta name="keywords" content="">
        <!-- FB OG -->
        <meta property="og:title" content="">
        <meta property="og:site_name" content="">
        <meta property="og:url" content="">
        <meta property="og:description" content="">
        <meta property="og:locale" content="th-TH">
        <meta property="og:locale:alternate" content="en-EN">
        <!-- Google -->
        <meta name="google-site-verification" content="">
        <link href="" rel="author"><!-- ทำ G+ profile แล้วเอา code มาใส่ให้เขาครับ เปิดเป็น profile แบบบริษัทนะครับ -->
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/colorbox.css">
        <script src="js/vendor/modernizr-2.7.1.min.js"></script>
    </head>    