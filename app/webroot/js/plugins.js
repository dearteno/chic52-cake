// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
jQuery(document).ready(function($) {
    // Banner Big
    $("#billboard").royalSlider({
        keyboardNavEnabled: false,
        transitionType: "fade",
        controlNavigation: "none",
        arrowsNav: false,
        transitionSpeed: 1400,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 3000
        }
    });

    $("#billboard-inner").royalSlider({
        keyboardNavEnabled: false,
        transitionType: "fade",
        controlNavigation: "none",
        arrowsNav: false,
        transitionSpeed: 1400,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 2500
        },
        imageScaleMode: 'fill'
    });

    // Highlihgt Product
    $('.bxslider').bxSlider({
        mode: 'horizontal',
        captions: false,
        pager: false,
        nextText: '<span>NEXT</span>',
        prevText: '<span>PREVIOUS</span>',
        minSlides: 1,
        maxSlides: 4,
        slideWidth: 160
    });

    // Banner Size S
    $(".adslot-s").royalSlider({
        keyboardNavEnabled: false,
        transitionType: "move",
        slidesOrientation: "vertical",
        controlNavigation: "none",
        arrowsNav: false,
        transitionSpeed: 400,
        loop: true,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 5000
        }
    });

    // Product Detail
    $('#product_gallery').royalSlider({
        keyboardNavEnabled: true,
        transitionType: "fade",
        imageScaleMode: "fit",
        arrowsNav: false,
        controlNavigation: "thumbnails",
        thumbs: {
            // thumbnails options go gere
            spacing: 5,
            arrowsAutoHide: true,
            autoCenter: false
        }

    });
    
    /* product slide */
    $('.product-list-slide').bxSlider({
        pager: false
    });


	$('.store-slider').bxSlider({
		mode: 'horizontal',
        captions: false,
        pager: false,
        nextSelector: '#store-next',
		prevSelector: '#store-prev',
		nextText: '<span class="hd-t">next</span>',
		prevText: '<span class="hd-t">prev</span>'
		
	});

		$("#emailsubmit").validate({
			rules: {
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				email: ""
			},
			submitHandler: function() {
				var email	=document.subscribe.email.value;
			
				url_load	="email_register.php";
				$.post(url_load, "email="+email, function(data){
						if (data =="OK")
						{
							$("#lbSubscribe_form").hide();
							$("#lbSubscribe_result").show();
							document.subscribe.email.value='';
							setTimeout( "jQuery('#lbSubscribe_form').show();jQuery('#lbSubscribe_result').hide();",2000 );
						}
				});

				return false;
			}
		});


		$("#subscribe").validate({
			rules: {
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				email: ""
			},
			submitHandler: function() {
				var email	=document.subscribe.email.value;
			
				url_load	="email_register.php";
				$.post(url_load, "email="+email, function(data){
						if (data =="OK")
						{
							$("#subscribe_email").hide();
							$("#subscribe_result").show();
							document.subscribe.email.value='';
							setTimeout( "jQuery('#subscribe_email').show();jQuery('#subscribe_result').hide();",2000 );
						}
				});

				return false;
			}
		});


});