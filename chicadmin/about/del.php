<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="about";


$TITLE_TOPIC	="ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_ABOUT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$about_cover		=	$row["NEWS_COVER"];
				$seo_image		=	$row["SEO_IMAGE"];
				$about_image		=	$row["NEWS_IMAGE"];
				$about_image1		=	$row["NEWS_IMAGE1"];
				$about_image2		=	$row["NEWS_IMAGE2"];
				$about_image3		=	$row["NEWS_IMAGE3"];
				$about_image4		=	$row["NEWS_IMAGE4"];
			}


			$array_image	=	preg_split("/,/",$about_image);

			for ($i=0;$i<=count($array_image)-1;$i++) {
			    $temp_image = trim($array_image[$i]);
				list($temp_image,$temp_text) = preg_split('/\|/', $temp_image);

					if($temp_image !="" AND $temp_image !="blank_user.jpg") {
						deletedata("../../_files/images/full/$temp_image");
						deletedata("../../_files/images/source/$temp_image");
						deletedata("../../_files/images/thumb/$temp_image");
					}
			}
					if($about_image1 !="" AND $about_image1 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$about_image1");
						deletedata("../../_files/images/source/$about_image1");
						deletedata("../../_files/images/thumb/$about_image1");
					}
					if($about_image2 !="" AND $about_image2 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$about_image2");
						deletedata("../../_files/images/source/$about_image2");
						deletedata("../../_files/images/thumb/$about_image2");
					}
					if($about_image3 !="" AND $about_image3 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$about_image3");
						deletedata("../../_files/images/source/$about_image3");
						deletedata("../../_files/images/thumb/$about_image3");
					}
					if($about_image4 !="" AND $about_image4 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$about_image4");
						deletedata("../../_files/images/source/$about_image4");
						deletedata("../../_files/images/thumb/$about_image4");
					}
					if($about_cover !="" AND $about_cover !="blank_user.jpg") {
						deletedata("../../_files/images/full/$about_cover");
						deletedata("../../_files/images/source/$about_cover");
						deletedata("../../_files/images/thumb/$about_cover");
					}

					if($seo_image !="" AND $seo_image !="blank_user.jpg") {
						deletedata("../../_files/images/full/$seo_image");
						deletedata("../../_files/images/source/$seo_image");
						deletedata("../../_files/images/thumb/$seo_image");
					}

$SQL			=	"DELETE FROM $DB_ABOUT WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>