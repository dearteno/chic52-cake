<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="about";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");



$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="image_add.php?id=$id";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="image_edit.php?id=<ID>";$UP_LINK="image_sort.php?action=up&id=<ID>";$DOWN_LINK="image_sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:image_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###


if($id==1){
	$about_name	='Chic Concept';
	$tp_edit	=	new Template("_tp_edit.html");
}
if($id==2){
	$about_name	='Visit Store';
	$tp_edit	=	new Template("_tp_edit_store.html");


		$SQL			=	"SELECT * FROM $DB_ABOUT_IMAGE WHERE RESOURCE_ID='$id' ORDER BY RESOURCE_SORT ;";	
		$result			=	mysql_query($SQL);

			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$resource_name		=	$row["RESOURCE_TOPIC"];
				$resource_cover		=	$row["RESOURCE_PIC"];

				$EDIT_LINK_HTML	=$EDIT_LINK;
				$DEL_LINK_HTML	=$DEL_LINK;
				$UP_LINK_HTML	=$UP_LINK;
				$DOWN_LINK_HTML	=$DOWN_LINK;

				$EDIT_LINK_HTML =preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);
				$DEL_LINK_HTML =preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);
				$UP_LINK_HTML =preg_replace("/<ID>/i",$ID,$UP_LINK_HTML);
				$DOWN_LINK_HTML =preg_replace("/<ID>/i",$ID,$DOWN_LINK_HTML);


$IMAGE_HTML .="<div id=\"\" class=\"resource_block\">
<img src=\"../../_files/images/thumb/$resource_cover\" width=\"160\" /><br/>
<span class=\"bold\">$resource_name</span><br/>
<a href=\"$UP_LINK_HTML\" class=\"up-record\"><img src='../images/icons/arrow_left.png'></a>&nbsp;
<a href=\"$DEL_LINK_HTML\" class=\"delete-record\"><img src='../images/icons/delete.png'></a>&nbsp;
<a href=\"$DOWN_LINK_HTML\" class=\"down-record\"><img src='../images/icons/arrow_right.png'></a>
</div>";

			}
 	 	 



}

$TITLE_TOPIC	="<a href='index.php'>About Us</a>&nbsp;/&nbsp;$about_name&nbsp;/&nbsp;แก้ไข";



if ($action =="edit") {


		$about_topic			=	$_POST["about_topic"];
		$about_pic_temp			=	$_POST["about_pic_temp"];
		$about_intro_th			=	$_POST["about_intro_th"];
		$about_intro_en			=	$_POST["about_intro_en"];
		$about_content_th		=	$_POST["about_content_th"];
		$about_content_en		=	$_POST["about_content_en"];

		$file				=	$_FILES['about_pic']['name'];
		$typefile			=	$_FILES['about_pic']['type'];	
		$sizefile			=	$_FILES['about_pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($about_pic_temp !=""){
						deletedata("../../_files/images/full/$about_pic_temp");
						deletedata("../../_files/images/source/$about_pic_temp");
						deletedata("../../_files/images/thumb/$about_pic_temp");						
					}


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['about_pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['about_pic']['tmp_name'],$pathfull);
					chmod("$pathfull", 0777); 

					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['about_pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 


					$thumb = new Thumbnail($pathfull);
					$thumb->resize(944,495);
					$thumb->save($pathfull);

					list($w1, $h1) = getimagesize($paththumb);
					$w2 = 180 ;
					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$ABOUT_PIC	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_ABOUT SET ABOUT_PIC='$ABOUT_PIC',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
					$result			=	mysql_query($SQL);

		}



		if($id ==2){

			for($i=1;$i<=10;$i++){

					$file				=	$_FILES["resource_imagecontent$i"]['name'];
					$typefile			=	$_FILES["resource_imagecontent$i"]['type'];	
					$sizefile			=	$_FILES["resource_imagecontent$i"]['size'];
					$typecheck1		=	strtolower($file);
					$typecheck1		=	strrchr($typecheck1,'.');
					if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

					if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {

						$ran		=random_string(8);
						$paththumb	="../../_files/images/source/$ran$typecheck1";
						copy($_FILES["resource_imagecontent$i"]['tmp_name'],$paththumb);
						chmod("$paththumb", 0777); 
						$pathfull	="../../_files/images/full/$ran$typecheck1";
						copy($_FILES["resource_imagecontent$i"]['tmp_name'],$pathfull);
						chmod("$pathfull", 0777); 

						$paththumb	="../../_files/images/thumb/$ran$typecheck1";
						copy($_FILES["resource_imagecontent$i"]['tmp_name'],$paththumb);
						chmod("$paththumb", 0777); 

						$thumb = new Thumbnail($pathfull);
						$thumb->resize(940,630);
						$thumb->save($pathfull);

						$thumb = new Thumbnail($paththumb);
						$thumb->resize(359,240);
						$thumb->cropFromCenter(240,240);
						$thumb->save($paththumb);

						$SQL			=	"SELECT * FROM $DB_ABOUT_IMAGE WHERE RESOURCE_ID='$id' ORDER BY RESOURCE_SORT DESC LIMIT 0,1;";	

						$result			=	mysql_query($SQL);
							while ($row			=	mysql_fetch_array($result)){	
								$resource_sort	=	$row["RESOURCE_SORT"];
							}
						$resource_sort		= $resource_sort+1;


						$SQL			=	"INSERT INTO $DB_ABOUT_IMAGE VALUES('', '$id', '$ran$typecheck1', '', '$resource_sort', NOW());";	
						$result			=	mysql_query($SQL);


					}

				
			}
			
		}




	$SQL			=	"UPDATE $DB_ABOUT SET ABOUT_TOPIC='$about_topic',
	ABOUT_INTRO_TH='$about_intro_th',ABOUT_INTRO_EN='$about_intro_en',
	ABOUT_CONTENT_TH='$about_content_th',ABOUT_CONTENT_EN='$about_content_en',
	UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);


		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_ABOUT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$about_topic		=	$row["ABOUT_TOPIC"];
				$about_pic_temp			=	$row["ABOUT_PIC"];
				$about_intro_th			=	$row["ABOUT_INTRO_TH"];
				$about_intro_en			=	$row["ABOUT_INTRO_EN"];
				$about_content_th		=	$row["ABOUT_CONTENT_TH"];
				$about_content_en		=	$row["ABOUT_CONTENT_EN"];
			}



		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>