<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: phoinikasdigital.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="news";
$page_sub_nav	="lineup_list";


include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("/zadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("/zadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("/zadmin/logout.php");exit;}



$p			= $_GET["p"];
if ($p =="" or !is_numeric($p)) {	$p		=	1;}
$PERPAGE	=	20;

### แสดงรายชื่อพนักงาน ###

$t				=	$_GET["t"];
if($t =="") {	$t ="all";}

$id				=	$_GET["id"];
if ($id =="" or !is_numeric($id)) {redirect("index.php");exit;}


if ($U_STATUS =="") {redirect("$BASEURL/zadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/zadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/zadmin/logout.php");exit;}


### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="image_add.php?id=$id";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="image_edit.php?id=<ID>";$UP_LINK="image_sort.php?action=up&id=<ID>";$DOWN_LINK="image_sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:image_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###



$tp				=	new Template("../_tp_main.html");

$tp_page		=	new Template("_tp_image_list.html");


		$SQL			=	"SELECT * FROM $DB_RESOURCE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_name		=	$row["CONTENT_NAME"];
				$content_status			=	$row["CONTENT_STATUS"];
				$file1temp				=	$row["CONTENT_FILE1"];
				$file2temp				=	$row["CONTENT_FILE2"];
				$file3temp				=	$row["CONTENT_FILE3"];
				$file4temp				=	$row["CONTENT_FILE4"];

			}







$TITLE_TOPIC	="<a href='index.php'>ข่าว</a>&nbsp;/&nbsp;<a href='leaflet.php'>ใบปลิว</a>&nbsp;/&nbsp;<a href='image_list.php?id=$id'>$content_name</a>";









		$SQL			=	"SELECT * FROM $DB_RESOURCE_IMAGE WHERE RESOURCE_ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);







$total_records = $count;
$scroll_page = 10; // จำนวน scroll page
$per_page = $PERPAGE; // จำนวน record ต่อหนึ่ง page
$current_page = $p; // หน้าปัจจุบัน
$pager_url = "image_list.php?id=$id&p="; // url page ที่ต้องการเรียก
$inactive_page_tag = ''; // 
$previous_page_text = '<img src="/zadmin/images/arrows/arrow_previous.png">'; // หน้าก่อนหน้า
$next_page_text = '<img src="/zadmin/images/arrows/arrow_next.png">'; // หน้าถัดไป
$first_page_text = '<img src="/zadmin/images/arrows/arrow_first.png">'; // ไปหน้าแรก
$last_page_text = '<img src="/zadmin/images/arrows/arrow_last.png">'; // ไปหน้าสุดท้าย

$kgPagerOBJ = new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);


$pagelink .=$kgPagerOBJ -> first_page;
$pagelink .=$kgPagerOBJ -> previous_page;
$pagelink .=$kgPagerOBJ -> page_links;
$pagelink .=$kgPagerOBJ -> next_page;
$pagelink .=$kgPagerOBJ -> last_page;

if($pagelink=="<span>1</span>") {
	$pagelink="";
}


			if ($p	== 1) {
				$start	=0;
				$limit	=$PERPAGE;
				$i		=1;
			}else{
				$start	=(($PERPAGE*$p)-$PERPAGE);
				$limit	=$PERPAGE;
				$i		=(($PERPAGE*$p)-$PERPAGE)+1;
			}



$tp_page->Block("MEMBER_LIST");
$tp_page->Sub(1);


		$SQL			=	"SELECT * FROM $DB_RESOURCE_IMAGE WHERE RESOURCE_ID='$id' ORDER BY RESOURCE_SORT LIMIT $start,$limit;";	

		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$resource_name		=	$row["RESOURCE_TOPIC"];
				$resource_cover		=	$row["RESOURCE_PIC"];

				$EDIT_LINK_HTML	=$EDIT_LINK;
				$DEL_LINK_HTML	=$DEL_LINK;
				$UP_LINK_HTML	=$UP_LINK;
				$DOWN_LINK_HTML	=$DOWN_LINK;

				$EDIT_LINK_HTML =preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);
				$DEL_LINK_HTML =preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);
				$UP_LINK_HTML =preg_replace("/<ID>/i",$ID,$UP_LINK_HTML);
				$DOWN_LINK_HTML =preg_replace("/<ID>/i",$ID,$DOWN_LINK_HTML);

				$tp_page->Apply();
				$i++;

			}
 	 	 




$CONTENT_HTML	=	$tp_page->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>