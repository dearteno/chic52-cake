<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");


$page_nav		="banner";
$page_sub_nav	="list";


include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### Addรายชื่อพนักงาน ###

$tp				=	new Template("../_tp_main.html");


$tp_add	=	new Template("_tp_add.html");

$t		=	$_POST["t"];
if($t==""){$t		=	$_GET["t"];}
if($t==""){redirect("index.php");exit;}


if($t==1){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=1'>Banner Slider Top</a> / Add";
	$TEXT_PIC ="size 1020 x 440 jpg, png (Auto resize)";
	$PIC_W=1020;
	$PIC_H=440;

}
if($t==2){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=2'>Intro Page</a> / Add";
	$TEXT_PIC ="size 1020 x 440 jpg, png (Auto resize)";
	$PIC_W=1020;
	$PIC_H=440;
	$BLOCK_PIC21="<!-- ";
	$BLOCK_PIC22=" -->";
}
if($t==3){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=3'>Banner Slider Bottom</a> / Add";
	$TEXT_PIC ="size 300 x 200 jpg, png (Auto resize)";
	$PIC_W=300;
	$PIC_H=200;
	$BLOCK_PIC21="<!-- ";
	$BLOCK_PIC22=" -->";
}


$action		=	$_POST["action"];

if ($action =="add") {

		$banner_type		=	$_POST["banner_type"];
		$youtube_id			=	$_POST["youtube_id"];
		$banner_head_text	=	$_POST["banner_head_text"];
		$banner_small_text	=	$_POST["banner_small_text"];
		$banner_intro		=	$_POST["banner_intro"];
		$banner_status		=	$_POST["banner_status"];


		$link	=	$_POST["link"];

		$file			=	$_FILES['pic']['name'];
		$typefile		=	$_FILES['pic']['type'];	
		$sizefile		=	$_FILES['pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}




					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 180 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 180 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}


					$picfile	=	"$ran$typecheck1";


		}


		$file			=	$_FILES['pic2']['name'];
		$typefile		=	$_FILES['pic2']['type'];	
		$sizefile		=	$_FILES['pic2']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$pictemp");
						deletedata("../../_files/images/source/$pictemp");
						deletedata("../../_files/images/thumb/$pictemp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}

					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 180 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 180 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}


					$picfile2	=	"$ran$typecheck1";



		}




		
		if ($link =="http://") {
		    $link="";
		}


		$SQL			=	"SELECT * FROM $DB_BANNER WHERE BANNER_TYPE='$t' ORDER BY BANNER_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$BANNER_SORT			=	$row["BANNER_SORT"];
			}

		$BANNER_SORT = $BANNER_SORT+1;



	$SQL		=	"INSERT INTO $DB_BANNER VALUES('', '$t', '$banner_type','$youtube_id','$banner_intro','$picfile', '$picfile2','$link', '$BANNER_SORT','$banner_status','$U_USERNAME',NOW(),'$U_USERNAME',NOW());";	
	$result		=	mysql_query($SQL);    


	$tp_add->Block("STAFF_SUCCESS");
	$tp_add->Apply();

	$CONTENT_HTML	=	$tp_add->Generate();
	$tp->Display();


/*
	$BANNER_HTML		=acmeCache::fetch('BANNER_HTML', 1);
	if(!$BANNER_HTML){

			$tp_sponsor		=	new Template(BASEDIR."/_tp_sponsor.html");
			$tp_sponsor->Block("BANNER_LIST");
			$tp_sponsor->Sub(1);

			$SQL			=	"SELECT * FROM $DB_BANNER  ORDER BY BANNER_SORT DESC;";
			$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$name		=	$row["BANNER_NAME"];
					$pic 		=	BASEURL_UPLOAD."/images/full/".$row["BANNER_PIC"];
					$link 		=	$row["BANNER_LINK"];
					$tp_sponsor->Apply();
				}

		$BANNER_HTML	=	$tp_sponsor->Generate();

		acmeCache::save('BANNER_HTML', $BANNER_HTML);

	}
*/


	ob_end_flush();
	mysql_close();
	exit;



}else{

$tp_add->Block("STAFF_INFO");
$tp_add->Apply();

}








$link ='http://';






$tp_add->Block("STAFF_FORM");
$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>