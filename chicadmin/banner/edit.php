<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");


$page_nav		="banner";
$page_sub_nav	="list";
$TITLE_TOPIC	="<a href='index.php'>Banner</a> / Edit";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### Editรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_member_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("$BASEURL/chicadmin/logout.php");exit;}

$t		=	$_POST["t"];
if($t==""){$t		=	$_GET["t"];}



if($t==1){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=1'>Banner Slider Top</a> / Edit";
	$TEXT_PIC ="size 1020 x 440 jpg, png (Auto resize)";
	$PIC_W=1020;
	$PIC_H=440;

}
if($t==2){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=2'>Intro Page</a> / Edit";
	$TEXT_PIC ="size 1020 x 440 jpg, png (Auto resize)";
	$PIC_W=1020;
	$PIC_H=440;
	$BLOCK_PIC11="<!-- ";
	$BLOCK_PIC22=" -->";
}
if($t==3){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=3'>Banner Slider Bottom</a> / Edit";
	$TEXT_PIC ="size 300 x 200 jpg, png (Auto resize)";
	$PIC_W=300;
	$PIC_H=200;
	$BLOCK_PIC21="<!-- ";
	$BLOCK_PIC22=" -->";
}

if ($action =="edit") {

		$banner_type		=	$_POST["banner_type"];
		$youtube_id			=	$_POST["youtube_id"];
		$banner_head_text	=	$_POST["banner_head_text"];
		$banner_small_text	=	$_POST["banner_small_text"];
		$banner_intro		=	$_POST["banner_intro"];
		$banner_status		=	$_POST["banner_status"];
		$link	=	$_POST["link"];
		$pictemp		=	$_POST["pictemp"];
		$pictemp2		=	$_POST["pictemp2"];

		$file			=	$_FILES['pic']['name'];
		$typefile		=	$_FILES['pic']['type'];	
		$sizefile		=	$_FILES['pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$pictemp");
						deletedata("../../_files/images/source/$pictemp");
						deletedata("../../_files/images/thumb/$pictemp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}



					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 180 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 180 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					
					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}

					$picfile	=	"$ran$typecheck1";

					$SQL		=	"UPDATE $DB_BANNER SET BANNER_PIC='$picfile' WHERE ID='$id'";	
					$result		=	mysql_query($SQL);    


		}


		$file			=	$_FILES['pic2']['name'];
		$typefile		=	$_FILES['pic2']['type'];	
		$sizefile		=	$_FILES['pic2']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$pictemp2");
						deletedata("../../_files/images/source/$pictemp2");
						deletedata("../../_files/images/thumb/$pictemp2");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}

					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 180 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 180 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}

					$picfile	=	"$ran$typecheck1";

					$SQL		=	"UPDATE $DB_BANNER SET BANNER_PIC2='$picfile' WHERE ID='$id'";	
					$result		=	mysql_query($SQL);    


		}






	$SQL		=	"UPDATE $DB_BANNER SET BANNER_SOURCE='$banner_type', 	BANNER_YOUTUBE_ID='$youtube_id',BANNER_INTRO='$banner_intro',BANNER_LINK='$link',BANNER_STATUS='$banner_status',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id'";	
	$result		=	mysql_query($SQL);    



	$tp_member_edit->Block("STAFF_SUCCESS");
	$tp_member_edit->Apply();

	$CONTENT_HTML	=	$tp_member_edit->Generate();
	$tp->Display();




	ob_end_flush();
	mysql_close();
	exit;



}else{


		$tp_member_edit->Block("STAFF_INFO");
		$tp_member_edit->Apply();


}





		$SQL			=	"SELECT * FROM $DB_BANNER WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("$BASEURL/chicadmin/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$t					=	$row["BANNER_TYPE"];
				$banner_head_text	=	$row["BANNER_HEAD_TEXT"];
				$banner_small_text	=	$row["BANNER_SMALL_TEXT"];
				$banner_intro		=	$row["BANNER_INTRO"];
				$pic 				=	$row["BANNER_PIC"];
				$pic2 				=	$row["BANNER_PIC2"];
				$link 				=	$row["BANNER_LINK"];
				$banner_status 		=	$row["BANNER_STATUS"];
				$BANNER_SOURCE 		=	$row["BANNER_SOURCE"];
				$youtube_id			=	$row["BANNER_YOUTUBE_ID"];
			}

if($banner_status  =="S"){
	$banner_status_s="CHECKED";
}else{
	$banner_status_h="CHECKED";
}

if($BANNER_SOURCE=="youtube"){
	$banner_type='youtube';
}else{
	$banner_type='pic';
}

if($t==1){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=1'>Banner Slider Top</a> / Edit";
	$TEXT_PIC ="size 1020 x 440 jpg, png (Auto resize)";
	$PIC_W=1020;
	$PIC_H=440;
	$BLOCK_PIC21="";
	$BLOCK_PIC22="";


}
if($t==2){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=2'>Intro Page</a> / Edit";
	$TEXT_PIC ="size 1020 x 440 jpg, png (Auto resize)";
	$PIC_W=1020;
	$PIC_H=440;
	$BLOCK_PIC11="";
	$BLOCK_PIC22="";
}
if($t==3){ 
	$TITLE_TOPIC="Banner / <a href='index.php?t=3'>Banner Slider Bottom</a> / Edit";
	$TEXT_PIC ="size 300 x 200 jpg, png (Auto resize)";
	$PIC_W=300;
	$PIC_H=200;
	$BLOCK_PIC21="";
	$BLOCK_PIC22="";
}

if($BANNER_SOURCE  =="youtube"){
	$BLOCK_PIC11 ="<!-- ";
	$BLOCK_PIC22 =" -->";
}else{
	$BLOCK_FLASH11 ="<!-- ";
	$BLOCK_FLASH22 =" -->";
}

		$tp_member_edit->Block("STAFF_FORM");
		$tp_member_edit->Apply();


$CONTENT_HTML	=	$tp_member_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>