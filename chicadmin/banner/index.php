<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");

$p			= $_GET["p"];
if ($p =="" or !is_numeric($p)) {	$p		=	1;}
$t			= $_GET["t"];
if ($t =="" or !is_numeric($t)) {	$t		=	1;}	

$page_nav		="banner";
$page_sub_nav	="list";
$TITLE_TOPIC	="Banner";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="add.php?t=$t";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {
	$UP_LINK="sort.php?id=<ID>&action=up";
	$DOWN_LINK="sort.php?id=<ID>&action=down";
	$EDIT_LINK ="edit.php?id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";
	$UP_LINK="javascript:no_access_popup();";
	$DOWN_LINK="javascript:no_access_popup();";

}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:deletebanner(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-X/i",$U_ACCESS)) {$EXPORT_LINK_HTML ="export.php";}
else{$EXPORT_LINK_HTML ="javascript:no_access_popup();";}


### SET MENU ###



$PERPAGE	=	100;

if($t==1){ $TITLE_TOPIC.=" / Banner Slider Top";}
if($t==2){ $TITLE_TOPIC.=" / Intro Page";}
if($t==3){ $TITLE_TOPIC.=" / Banner Slider Bottom";}

### แสดงรายชื่อพนักงาน ###


$tp				=	new Template("../_tp_main.html");
$tp_sponsor		=	new Template("_tp_list.html");


		$SQL			=	"SELECT * FROM $DB_BANNER WHERE BANNER_TYPE='$t' ORDER BY ID DESC;";	


		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);







$total_records = $count;
$scroll_page = 10; // จำนวน scroll page
$per_page = $PERPAGE; // จำนวน record ต่อหนึ่ง page
$current_page = $p; // หน้าปัจจุบัน
$pager_url = "index.php?t=$t&p="; // url page ที่ต้องการเรียก
$inactive_page_tag = ''; // 
$previous_page_text = '<img src="$BASEURL/chicadmin/images/arrows/arrow_previous.png">'; // หน้าก่อนหน้า
$next_page_text = '<img src="$BASEURL/chicadmin/images/arrows/arrow_next.png">'; // หน้าถัดไป
$first_page_text = '<img src="$BASEURL/chicadmin/images/arrows/arrow_first.png">'; // ไปหน้าแรก
$last_page_text = '<img src="$BASEURL/chicadmin/images/arrows/arrow_last.png">'; // ไปหน้าสุดท้าย

$kgPagerOBJ = new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);


$pagelink .=$kgPagerOBJ -> first_page;
$pagelink .=$kgPagerOBJ -> previous_page;
$pagelink .=$kgPagerOBJ -> page_links;
$pagelink .=$kgPagerOBJ -> next_page;
$pagelink .=$kgPagerOBJ -> last_page;

if($pagelink=="<span>1</span>") {
	$pagelink="";
}


			if ($p	== 1) {
				$start	=0;
				$limit	=$PERPAGE;
				$i		=1;
			}else{
				$start	=(($PERPAGE*$p)-$PERPAGE);
				$limit	=$PERPAGE;
				$i		=(($PERPAGE*$p)-$PERPAGE)+1;
			}



$tp_sponsor->Block("MEMBER_LIST");
$tp_sponsor->Sub(2);



		$SQL			=	"SELECT * FROM $DB_BANNER WHERE BANNER_TYPE='$t' ORDER BY BANNER_SORT DESC LIMIT $start,$limit;";
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$BANNER_HEAD_TEXT		=	$row["BANNER_HEAD_TEXT"];
				$BANNER_SMALL_TEXT		=	$row["BANNER_SMALL_TEXT"];
				$BANNER_INTRO			=	$row["BANNER_INTRO"];
				$BANNER_PIC 			=	$row["BANNER_PIC"];
				$BANNER_LINK 		=	$row["BANNER_LINK"];
				$BANNER_STATUS 		=	$row["BANNER_STATUS"];

				$BANNER_SOURCE 		=	$row["BANNER_SOURCE"];
				$BANNER_YOUTUBE_ID 		=	$row["BANNER_YOUTUBE_ID"];
 	

				$C_USERNAME 		=	$row["CREATE_USERNAME"];
				$C_TIME		 		=	$row["CREATE_TIME"];
				$L_USERNAME 		=	$row["UPDATE_USERNAME"];
				$L_TIME		 		=	$row["UPDATE_TIME"];				
				

				if($C_TIME=="0000-00-00 00:00:00"){$C_TIME ="";}
				if($L_TIME=="0000-00-00 00:00:00"){$L_TIME ="";}

 	 	 
				if ($BANNER_STATUS=="S") {
					$STATUS ='<img src="../images/icons/accept32.png">';
				} 	 	 	 
				if ($BANNER_STATUS=="H") {
					$STATUS ='<img src="../images/icons/cancel32.png">';
				} 

				$EDIT_LINK_HTML	=	$EDIT_LINK;
				$DEL_LINK_HTML	=	$DEL_LINK;
				$DOWN_LINK_HTML	=	$DOWN_LINK;
				$UP_LINK_HTML	=	$UP_LINK;


				$EDIT_LINK_HTML =	preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);
				$DEL_LINK_HTML  =	preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);
				$UP_LINK_HTML  =	preg_replace("/<ID>/i",$ID,$UP_LINK_HTML);
				$DOWN_LINK_HTML  =	preg_replace("/<ID>/i",$ID,$DOWN_LINK_HTML);

				if($BANNER_SOURCE=="youtube"){
					$BANNER_HTML ='<iframe width="200" height="113" src="//www.youtube.com/embed/'.$BANNER_YOUTUBE_ID.'" frameborder="0" allowfullscreen></iframe>';					
				}else{
					$BANNER_HTML ="<img src=\"../../_files/images/thumb/$BANNER_PIC\" />";					
				}

				$tp_sponsor->Apply();
				$i++;
			}




$CONTENT_HTML	=	$tp_sponsor->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>