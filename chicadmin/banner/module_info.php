<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="banner";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="Banner";
$MODULE_DETAIL		="";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,";


$MODULE_SUB[]		="Banner Slider Top|member_list|index.php?t=1";
$MODULE_SUB[]		="Banner Slider Bottom|member_list|index.php?t=3";
$MODULE_SUB[]		="Intro Page|member_list|index.php?t=2";
#$MODULE_SUB[]		="Banner Promotion|member_list|index.php?t=2";

?>