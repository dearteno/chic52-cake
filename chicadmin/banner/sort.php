<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="banner";
$page_sub_nav	="list";
$TITLE_TOPIC	="<a href='index.php'>Banner</a> / Edit";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}



### เรียงลำดับใหม่ ###

$act	=	$_GET["action"];
$id		=	$_GET["id"];


	$SQL		="SELECT * FROM $DB_BANNER WHERE ID='$id'";
	$result		=mysql_query($SQL);
		while ($row		=	mysql_fetch_array($result)){	
			$BANNER_SORT1	=	$row["BANNER_SORT"];
			$BANNER_TYPE	=	$row["BANNER_TYPE"];
		}


if ($act =="down") {
	
	$SQL		="SELECT * FROM $DB_BANNER WHERE BANNER_TYPE='$BANNER_TYPE' AND BANNER_SORT < '$BANNER_SORT1' ORDER BY BANNER_SORT DESC LIMIT 0,1";

	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);
	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$BANNER_SORT2		=	$row["BANNER_SORT"];
		}

		$SQL		="UPDATE $DB_BANNER SET BANNER_SORT='$BANNER_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_BANNER SET BANNER_SORT='$BANNER_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}
}

if ($act =="up") {

	$SQL		="SELECT * FROM $DB_BANNER WHERE BANNER_TYPE='$BANNER_TYPE' AND BANNER_SORT > '$BANNER_SORT1' ORDER BY BANNER_SORT LIMIT 0,1";
	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);

	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$BANNER_SORT2		=	$row["BANNER_SORT"];
		}

		$SQL		="UPDATE $DB_BANNER SET BANNER_SORT='$BANNER_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_BANNER SET BANNER_SORT='$BANNER_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}

}

/*
	$BANNER_HTML		=acmeCache::fetch('BANNER_HTML', 1);
	if(!$BANNER_HTML){

			$tp_sponsor		=	new Template(BASEDIR."/_tp_sponsor.html");
			$tp_sponsor->Block("BANNER_LIST");
			$tp_sponsor->Sub(1);

			$SQL			=	"SELECT * FROM $DB_BANNER  ORDER BY BANNER_SORT DESC;";
			$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$name		=	$row["BANNER_NAME"];
					$pic 		=	BASEURL_UPLOAD."/images/full/".$row["BANNER_PIC"];
					$link 		=	$row["BANNER_LINK"];
					$tp_sponsor->Apply();
				}

		$BANNER_HTML	=	$tp_sponsor->Generate();

		acmeCache::save('BANNER_HTML', $BANNER_HTML);

	}
*/

header("Location: index.php?t=$BANNER_TYPE");
ob_end_flush();
mysql_close();
exit;
?>