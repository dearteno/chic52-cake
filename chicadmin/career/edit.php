<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

// Turn off all error reporting
error_reporting(0);

$page_nav		="career";


$TITLE_TOPIC	="<a href='index.php'>ตำแหน่งงาน</a>&nbsp;/&nbsp;แก้ไข";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {

		$content_name		=	$_POST["content_name"];
		$content_location	=	$_POST["content_location"];
		$content_num		=	$_POST["content_num"];
		$content_detail		=	$_POST["content_detail"];




		$content_status			=	$_POST["content_status"];

		if ($content_status =="Y") {$content_status ="S";}else{$content_status ="H";}




	$SQL			=	"UPDATE $DB_CAREER SET CONTENT_NAME='$content_name', CONTENT_LOCATION='$content_location', CONTENT_NUM='$content_num', CONTENT_DETAIL='$content_detail',CONTENT_STATUS='$content_status',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_CAREER WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$content_name		=	htmlspecialchars($row["CONTENT_NAME"]);
				$content_location	=	$row["CONTENT_LOCATION"];
				$content_num		=	$row["CONTENT_NUM"];
				$content_detail		=	$row["CONTENT_DETAIL"];
				$content_status		=	$row["CONTENT_STATUS"];

			}





if($content_status =="S"){
	$content_status_y="CHECKED";
}


		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>