<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="career";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="Career";
$MODULE_DETAIL		="Career";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,";

$MODULE_SUB[]		="ตำแหน่งงาน|career|index.php";
$MODULE_SUB[]		="รายชื่อผู้สมัคร|career|register.php";
?>