<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="career";


$TITLE_TOPIC	="Delete";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_CAREER_REGISTER WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$contact_cover		=	$row["NEWS_COVER"];
				$seo_image		=	$row["SEO_IMAGE"];
				$contact_image		=	$row["NEWS_IMAGE"];
				$contact_image1		=	$row["NEWS_IMAGE1"];
				$contact_image2		=	$row["NEWS_IMAGE2"];
				$contact_image3		=	$row["NEWS_IMAGE3"];
				$contact_image4		=	$row["NEWS_IMAGE4"];
			}


$SQL			=	"DELETE FROM $DB_CAREER_REGISTER WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_register_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>