<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: gmmz.tv
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

// Turn off all error reporting
error_reporting(0);

$page_nav		="catalog";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add		=	new Template("_tp_add.html");

$t			=	"leaflet";



$TITLE_TOPIC	="<a href='index.php'>Catalog</a>&nbsp;/&nbsp;เพิ่ม";
$action		=	$_POST["action"];




if ($action =="add") {




		$content_name			=	$_POST["content_name"];
		$content_intro			=	$_POST["content_intro"];
		$content_status			=	$_POST["content_status"];
		$file1temp				=	$_POST["file1temp"];
		$file2temp				=	$_POST["file2temp"];
		$file3temp				=	$_POST["file3temp"];

		if ($content_status =="Y") {$content_status ="S";}else{$content_status ="H";}



		$SQL			=	"SELECT * FROM $DB_CATALOG ORDER BY CONTENT_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row			=	mysql_fetch_array($result)){	
				$content_sort	=	$row["CONTENT_SORT"];
			}
		$content_sort		= $content_sort+1;


			$file				=	$_FILES["file1"]['name'];
			$typefile			=	$_FILES["file1"]['type'];	
			$sizefile			=	$_FILES["file1"]['size'];
			$typecheck1		=	strtolower($file);
			$typecheck1		=	strrchr($typecheck1,'.');
			if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

			$filelow		=	strtolower($file);

			if ($typecheck1 == ".jpg" OR $typecheck1 == ".png") {
						
					$paththumb	="../../_files/catalog/thumb/$filelow";
					copy($_FILES["file1"]['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 296;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 296;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);				

					$CONTENT_FILE1	=$filelow;

			}


			$file				=	$_FILES["file3"]['name'];
			$typefile			=	$_FILES["file3"]['type'];	
			$sizefile			=	$_FILES["file3"]['size'];
			$typecheck1		=	strtolower($file);
			$typecheck1		=	strrchr($typecheck1,'.');
			if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

			$filelow		=	strtolower($file);

			if ($typecheck1 == ".jpg" OR $typecheck1 == ".png" OR $typecheck1 == ".zip" OR $typecheck1 == ".pdf") {
						
					$paththumb	="../../_files/catalog/download/$filelow";
					copy($_FILES["file3"]['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					//$thumb = new Thumbnail($paththumb);
					//$thumb->resize(291,349);
					//$thumb->save($paththumb);				

					$CONTENT_FILE3	=$filelow;

			}


		$SQL			=	"INSERT INTO $DB_CATALOG VALUES('', '$content_name', '$content_intro', '$CONTENT_FILE1', '$CONTENT_FILE2', '$CONTENT_FILE3', '', '$content_sort', '$content_status', '$U_USERNAME', NOW(), '$U_USERNAME', NOW());";	
		$result			=	mysql_query($SQL);



		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>