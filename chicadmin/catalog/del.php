<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: gmmz.tv
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");


// Turn off all error reporting
error_reporting(0);

$page_nav		="catalog";


$TITLE_TOPIC	="<a href='index.php'>Catalog</a>&nbsp;/&nbsp;ลบ";


include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		$p				=	$_GET['p'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_CATALOG WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}


			while ($row		=	mysql_fetch_array($result)){	
				$CONTENT_FILE1			=	$row["CONTENT_FILE1"];
				$CONTENT_FILE2			=	$row["CONTENT_FILE2"];
				$CONTENT_FILE3			=	$row["CONTENT_FILE3"];
				$CONTENT_FILE4			=	$row["CONTENT_FILE4"];
			}


			if($CONTENT_FILE1 !=""){	deletedata("../../_files/catalog/thumb/$CONTENT_FILE1");}
			if($CONTENT_FILE2 !=""){	deletedata("../../_files/catalog/full/$CONTENT_FILE2");}
			if($CONTENT_FILE3 !=""){	deletedata("../../_files/catalog/download/$CONTENT_FILE3");}




$SQL			=	"DELETE FROM $DB_CATALOG WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>