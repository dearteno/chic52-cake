<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: gmmz.tv
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

// Turn off all error reporting
error_reporting(0);

$page_nav		="catalog";



$TITLE_TOPIC	="<a href='index.php'>Catalog</a>&nbsp;/&nbsp;แก้ไข";


include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {

		$content_name			=	$_POST["content_name"];
		$content_intro			=	$_POST["content_intro"];
		$content_answer			=	$_POST["content_answer"];
		$content_status			=	$_POST["content_status"];

		if ($content_status =="Y") {$content_status ="S";}else{$content_status ="H";}




			$file				=	$_FILES["file1"]['name'];
			$typefile			=	$_FILES["file1"]['type'];	
			$sizefile			=	$_FILES["file1"]['size'];
			$typecheck1		=	strtolower($file);
			$typecheck1		=	strrchr($typecheck1,'.');
			if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

			$filelow		=	strtolower($file);

			if ($typecheck1 == ".jpg" OR $typecheck1 == ".png") {
						
						$file1temp	=	$_POST["file1temp"];
						if($file1temp !=""){
							deletedata("../../_files/catalog/thumb/$file1temp");
						}		

					$paththumb	="../../_files/catalog/thumb/$filelow";
					copy($_FILES["file1"]['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 349 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 349 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);				

					$CONTENT_FILE1	=$filelow;

						$SQL			=	"UPDATE $DB_CATALOG SET CONTENT_FILE1='$CONTENT_FILE1' WHERE ID='$id';";	
						$result			=	mysql_query($SQL);

			}




			$file				=	$_FILES["file3"]['name'];
			$typefile			=	$_FILES["file3"]['type'];	
			$sizefile			=	$_FILES["file3"]['size'];
			$typecheck1		=	strtolower($file);
			$typecheck1		=	strrchr($typecheck1,'.');
			if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

			$filelow		=	strtolower($file);

			if ($typecheck1 == ".jpg" OR $typecheck1 == ".png" OR $typecheck1 == ".zip" OR $typecheck1 == ".pdf") {
						
						
						$file3temp	=	$_POST["file3temp"];
						if($file3temp !=""){
							deletedata("../../_files/catalog/download/$file3temp");
						}		

					$paththumb	="../../_files/catalog/download/$filelow";
					copy($_FILES["file3"]['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					//$thumb = new Thumbnail($paththumb);
					//$thumb->resize(291,349);
					//$thumb->save($paththumb);				

					$CONTENT_FILE3	=$filelow;

						$SQL			=	"UPDATE $DB_CATALOG SET CONTENT_FILE3='$CONTENT_FILE3' WHERE ID='$id';";	
						$result			=	mysql_query($SQL);

			}

	$SQL			=	"UPDATE $DB_CATALOG SET CONTENT_NAME='$content_name', CONTENT_INTRO='$content_intro', CONTENT_STATUS='$content_status',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_CATALOG WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_name		=	$row["CONTENT_NAME"];
				$content_intro		=	$row["CONTENT_INTRO"];
				$content_status			=	$row["CONTENT_STATUS"];
				$file1temp				=	$row["CONTENT_FILE1"];
				$file2temp				=	$row["CONTENT_FILE2"];
				$file3temp				=	$row["CONTENT_FILE3"];
				$file4temp				=	$row["CONTENT_FILE4"];

			}

if($file3temp !=""){	$file3download ="<a href='../../_files/catalog/download/$file3temp' target='_blank'>Download</a>";}



if($content_status =="S"){
	$content_status_y="CHECKED";
}


		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>