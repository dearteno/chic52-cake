<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: phoinikasdigital.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="catalog";


$TITLE_TOPIC	="<a href='index.php'>U-</a>&nbsp;/&nbsp;แก้ไข";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_image_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {exit;}


if ($action =="edit") {




		$catalog_name			=	$_POST["catalog_name"];
		$pictemp				=	$_POST["pictemp"];

		$file				=	$_FILES['resource_imagecontent']['name'];
		$typefile			=	$_FILES['resource_imagecontent']['type'];	
		$sizefile			=	$_FILES['resource_imagecontent']['size'];


		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !=""){
						deletedata("../../_files/catalog/full/$pictemp");
						deletedata("../../_files/catalog/source/$pictemp");
						deletedata("../../_files/catalog/thumb/$pictemp");					
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/catalog/source/$ran$typecheck1";
					copy($_FILES['resource_imagecontent']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/catalog/full/$ran$typecheck1";
					copy($_FILES['resource_imagecontent']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/catalog/thumb/$ran$typecheck1";
					copy($_FILES['resource_imagecontent']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

						list($w1, $h1) = getimagesize($pathfull);

						list($w1, $h1) = getimagesize($paththumb);
						if ($w1 > $h1) {
							$w2 = 1000 ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;
						}else{
							$h2 = 1000 ;
							$percent = $h2/$h1;
							$w2 = $w1 * $percent;				
						}

						$thumb = new Thumbnail($pathfull);
						$thumb->resize($w2,$h2);
						$thumb->save($pathfull);

						list($w1, $h1) = getimagesize($paththumb);
						if ($w1 > $h1) {
							$w2 = 210 ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;
						}else{
							$h2 = 210 ;
							$percent = $h2/$h1;
							$w2 = $w1 * $percent;				
						}

						$thumb = new Thumbnail($paththumb);
						$thumb->resize($w2,$h2);
						$thumb->save($paththumb);

						$catalog_pic	=	"$ran$typecheck1";

					$SQL		=	"UPDATE $DB_CATALOG_IMAGE SET RESOURCE_PIC='$catalog_pic' WHERE ID='$id';";	
					$result		=	mysql_query($SQL);
		}



	$SQL			=	"UPDATE $DB_CATALOG_IMAGE SET  	RESOURCE_TOPIC='$catalog_name' WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



		$SQL			=	"SELECT * FROM $DB_CATALOG_IMAGE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$catalog_name		=	$row["RESOURCE_TOPIC"];
				$catalog_cover		=	$row["RESOURCE_PIC"];
				$catalog_id			=	$row["RESOURCE_ID"];
			}
	


		$pictemp = $catalog_cover;



		$SQL			=	"SELECT * FROM $DB_CATALOG WHERE ID='$catalog_id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_name		=	$row["CONTENT_NAME"];
				$content_status			=	$row["CONTENT_STATUS"];
				$file1temp				=	$row["CONTENT_FILE1"];
				$file2temp				=	$row["CONTENT_FILE2"];
				$file3temp				=	$row["CONTENT_FILE3"];
				$file4temp				=	$row["CONTENT_FILE4"];

			}


$TITLE_TOPIC	="<a href='index.php'>ข่าว</a>&nbsp;/&nbsp;<a href='index.php'></a>&nbsp;/&nbsp;<a href='image_list.php?id=$catalog_id'>$content_name</a>&nbsp;/&nbsp;แก้ไข";






		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}







		$SQL			=	"SELECT * FROM $DB_CATALOG_IMAGE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$catalog_topic		=	$row["RESOURCE_TOPIC"];
				$catalog_cover		=	$row["RESOURCE_PIC"];
				$catalog_id			=	$row["RESOURCE_ID"];
			}
	


		$pictemp = $catalog_cover;


		$SQL			=	"SELECT * FROM $DB_CATALOG WHERE ID='$catalog_id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_name		=	$row["CONTENT_NAME"];
				$content_status			=	$row["CONTENT_STATUS"];
				$file1temp				=	$row["CONTENT_FILE1"];
				$file2temp				=	$row["CONTENT_FILE2"];
				$file3temp				=	$row["CONTENT_FILE3"];
				$file4temp				=	$row["CONTENT_FILE4"];

			}


$TITLE_TOPIC	="<a href='index.php'>ข่าว</a>&nbsp;/&nbsp;<a href='index.php'></a>&nbsp;/&nbsp;<a href='image_list.php?id=$catalog_id'>$content_name</a>&nbsp;/&nbsp;แก้ไข";





		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>