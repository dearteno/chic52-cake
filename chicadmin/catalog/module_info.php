<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="catalog";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="Catalog";
$MODULE_DETAIL		="Catalog";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,";

#$MODULE_SUB[]		="Chic Concept|service|detail.php?id=1";
#$MODULE_SUB[]		="Visit Store|service|detail.php?id=2";


?>