<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="contact";


$TITLE_TOPIC	="<a href='index.php'>Contact</a>&nbsp;/&nbsp;Detail";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_detail	=	new Template("_tp_detail.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}




$WRITE_LINK_HTML ="edit.php?id=$id";

		$SQL			=	"SELECT * FROM $DB_CONTACT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$subject			=	$row["SUBJECT"];
				$contact_name		=	$row["NAME"]." ".$row["SURNAME"];
				$tel				=	$row["TEL"];
				$email				=	$row["EMAIL"];
				$message			=	nl2br(htmlspecialchars($row["MESSAGE"]));
				$adddate			=	$row["ADDDATE"];
 	 	 		$status				=	$row["STATUS"]; 
 	 	 		$filename			=	$row["FILENAME"]; 
 	 	 		$remark				=	nl2br(htmlspecialchars($row["REMARK"])); 
			}

				if($subject =="1"){	$subject	="เรื่องทั้วไป";}
				if($subject =="2"){	$subject	="การเคลมสินค้า";}
				if($subject =="other"){	$subject	="อื่นๆ";}

		if($filename !=""){
			$filename	="<span class=\"bold\">Attach file</span><br/><a href='../../_files/contact/$filename' target='_blank'>$filename</a>";
		}


				if ($status=="N") {
					$status ='<span class="button submit medium green-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">รายการใหม่</span>';				    
				}
				if ($status=="R") {
					$status ='<span class="button submit medium blue-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">อ่านแล้ว</span>';				    
				}
				if ($status=="P") {
					$status ='<span class="button submit medium orange-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">อยู่ระหว่างดำเนินการ</span>';				    
				}
				if ($status=="E") {
					$status ='<span class="button submit medium grey-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">จบงาน</span>';				    
				}

		$SQL			=	"UPDATE $DB_CONTACT SET STATUS='R',STAFF='$U_USERNAME' WHERE ID='$id' AND STAFF='' AND STATUS='N';";	
		$result			=	mysql_query($SQL);



$CONTENT_HTML	=	$tp_detail->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>