<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="contact";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}



$WRITE_LINK_HTML ="edit.php?id=$id";


$action		=	$_POST["action"];

$TITLE_TOPIC	="<a href='index.php'>Contact</a>&nbsp;/&nbsp;<a href='detail.php?id=$id'>Detail</a>&nbsp;/&nbsp;แก้ไข";

if ($action =="edit") {

	$status		=	$_POST["status"];
	$remark		=	$_POST["remark"];



	$SQL			=	"UPDATE $DB_CONTACT SET STATUS='$status',REMARK='$remark',STAFF='$U_USERNAME' WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;


}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_CONTACT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$subject			=	$row["SUBJECT"];
				$contact_name		=	$row["NAME"]." ".$row["SURNAME"];
				$tel				=	$row["TEL"];
				$email				=	$row["EMAIL"];
				$message			=	nl2br(htmlspecialchars($row["MESSAGE"]));
				$adddate			=	$row["ADDDATE"];
				$remark				=	$row["REMARK"];
 	 	 		$status				=	$row["STATUS"]; 
 	 	 		$filename			=	$row["FILENAME"]; 
			}

				if($subject =="1"){	$subject	="เรื่องทั้วไป";}
				if($subject =="2"){	$subject	="การเคลมสินค้า";}
				if($subject =="other"){	$subject	="อื่นๆ";}

		if($filename !=""){
			$filename	="<span class=\"bold\">Attach file</span><br/><a href='../../_files/contact/$filename' target='_blank'>$filename</a>";
		}

				if($status =="N"){	$status_n='CHECKED';}
				if($status =="R"){	$status_r='CHECKED';}
				if($status =="P"){	$status_p='CHECKED';}
				if($status =="E"){	$status_e='CHECKED';}

		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>