<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="contact";
$page_sub_nav	="lineup_list";
$TITLE_TOPIC	="Contact";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="add.php";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="edit.php?id=<ID>";$UP_LINK="sort.php?action=up&id=<ID>";$DOWN_LINK="sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:page_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###

$p			= $_GET["p"];
if ($p =="" or !is_numeric($p)) {	$p		=	1;}
$PERPAGE	=	20;

### แสดงรายชื่อพนักงาน ###

$t				=	$_GET["t"];
if($t =="") {	$t ="all";}

$tp				=	new Template("../_tp_main.html");

$tp_page		=	new Template("_tp_list.html");


if($t =="all"){
	$SQL_SEARCH ="ID!=''";
}else{
	$SQL_SEARCH ="SUBJECT='$t'";
}

if($t =="all"){	$t_all	="selected";}
if($t =="1"){	$t_1	="selected";}
if($t =="2"){	$t_2	="selected";}
if($t =="other"){	$t_other	="selected";}




		$SQL			=	"SELECT * FROM $DB_CONTACT WHERE $SQL_SEARCH;";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);







$total_records = $count;
$scroll_page = 10; // จำนวน scroll page
$per_page = $PERPAGE; // จำนวน record ต่อหนึ่ง page
$current_page = $p; // หน้าปัจจุบัน
$pager_url = "index.php?t=$t&p="; // url page ที่ต้องการเรียก
$inactive_page_tag = ''; // 
$previous_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_previous.png">'; // หน้าก่อนหน้า
$next_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_next.png">'; // หน้าถัดไป
$first_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_first.png">'; // ไปหน้าแรก
$last_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_last.png">'; // ไปหน้าสุดท้าย

$kgPagerOBJ = new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);


$pagelink .=$kgPagerOBJ -> first_page;
$pagelink .=$kgPagerOBJ -> previous_page;
$pagelink .=$kgPagerOBJ -> page_links;
$pagelink .=$kgPagerOBJ -> next_page;
$pagelink .=$kgPagerOBJ -> last_page;

if($pagelink=="<span>1</span>") {
	$pagelink="";
}


			if ($p	== 1) {
				$start	=0;
				$limit	=$PERPAGE;
				$i		=1;
			}else{
				$start	=(($PERPAGE*$p)-$PERPAGE);
				$limit	=$PERPAGE;
				$i		=(($PERPAGE*$p)-$PERPAGE)+1;
			}



$tp_page->Block("MEMBER_LIST");
$tp_page->Sub(2);

		$SQL			=	"SELECT * FROM $DB_CONTACT WHERE $SQL_SEARCH ORDER BY ID DESC LIMIT $start,$limit;";	

		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$subject			=	$row["SUBJECT"];
				$contact_name		=	$row["NAME"]." ".$row["SURNAME"];
				$tel				=	$row["TEL"];
				$email				=	$row["EMAIL"];
				$message			=	$row["MESSAGE"];
				$adddate			=	$row["ADDDATE"];
 	 	 		$status				=	$row["STATUS"]; 	
				

				if($subject =="1"){	$subject	="เรื่องทั้วไป";}
				if($subject =="2"){	$subject	="การเคลมสินค้า";}
				if($subject =="other"){	$subject	="อื่นๆ";}


				if ($status=="N") {
					$status ='<span class="button submit medium green-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">รายการใหม่</span>';				    
				}
				if ($status=="R") {
					$status ='<span class="button submit medium blue-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">อ่านแล้ว</span>';				    
				}
				if ($status=="P") {
					$status ='<span class="button submit medium orange-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">อยู่ระหว่างดำเนินการ</span>';				    
				}
				if ($status=="E") {
					$status ='<span class="button submit medium grey-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">จบงาน</span>';				    
				}



				$EDIT_LINK_HTML	=$EDIT_LINK;
				$EDIT_LINK_HTML =preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);


				$DEL_LINK_HTML	=$DEL_LINK;
				$DEL_LINK_HTML =preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);


				$tp_page->Apply();
				$i++;

			}
 	 	 




$CONTENT_HTML	=	$tp_page->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>