<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="contact";



$TITLE_TOPIC	="<a href='map.php'>Map</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_map_add.html");


$action		=	$_POST["action"];



$map_date	=	date("d-m-Y");

if ($action =="add") {







		$map_name			=	$_POST["map_name"];
		$map_intro			=	$_POST["map_intro"];
		$map_time			=	$_POST["map_time"];
		$map_tel			=	$_POST["map_tel"];
		$map_address		=	$_POST["map_address"];



		$file				=	$_FILES['map_pic1']['name'];
		$typefile			=	$_FILES['map_pic1']['type'];	
		$sizefile			=	$_FILES['map_pic1']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['map_pic1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['map_pic1']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['map_pic1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$thumb = PhpThumbFactory::create($pathfull);
					$thumb->resize(272,180);
					$thumb->save($pathfull);

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize(272,180);
					$thumb->save($paththumb);

					$coverfile1	=	"$ran$typecheck1";

		}


		$file				=	$_FILES['map_pic2']['name'];
		$typefile			=	$_FILES['map_pic2']['type'];	
		$sizefile			=	$_FILES['map_pic2']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['map_pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['map_pic2']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['map_pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$thumb = PhpThumbFactory::create($pathfull);
					$thumb->resize(924,534);
					$thumb->save($pathfull);

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize(924,534);
					$thumb->save($paththumb);

					$coverfile2	=	"$ran$typecheck1";

		}



		$SQL			=	"SELECT * FROM $DB_MAP ORDER BY MAP_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$map_sort	=	$row["MAP_SORT"];
			}
		$map_sort		= $map_sort+1;



	$SQL			=	"INSERT INTO $DB_MAP VALUES('', '$map_name', '$coverfile1','$coverfile2', '$map_intro', '$map_time', '$map_tel', '$map_address', 'S', '$map_sort', '$U_USERNAME',NOW(),'$U_USERNAME',NOW());";
	$result			=	mysql_query($SQL);


		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>