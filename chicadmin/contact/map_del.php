<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="contact";


$TITLE_TOPIC	="ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_MAP WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$map_pic1		=	$row["MAP_PIC1"];
				$map_pic2		=	$row["MAP_PIC2"];
			}


					if($map_pic1 !="") {
						deletedata("../../_files/images/full/$map_pic1");
						deletedata("../../_files/images/source/$map_pic1");
						deletedata("../../_files/images/thumb/$map_pic1");
					}

					if($map_pic2 !="") {
						deletedata("../../_files/images/full/$map_pic2");
						deletedata("../../_files/images/source/$map_pic2");
						deletedata("../../_files/images/thumb/$map_pic2");
					}


$SQL			=	"DELETE FROM $DB_MAP WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_map_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>