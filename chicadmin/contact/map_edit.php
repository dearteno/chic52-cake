<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");






$page_nav		="contact";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");



$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="image_map_add.php?id=$id";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="image_map_edit.php?id=<ID>";$UP_LINK="image_map_sort.php?action=up&id=<ID>";$DOWN_LINK="image_map_sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:image_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###



	$map_name	='Chic Concept';
	$tp_edit	=	new Template("_tp_map_edit.html");



$TITLE_TOPIC	="<a href='map.php'>Map</a>&nbsp;/&nbsp;แก้ไข";



if ($action =="edit") {






		$map_type			=	$_POST["map_type"];
		$map_name			=	$_POST["map_name"];
		$map_intro			=	$_POST["map_intro"];
		$map_time			=	$_POST["map_time"];
		$map_tel			=	$_POST["map_tel"];
		$map_address		=	$_POST["map_address"];
		$map_pic_temp1			=	$_POST["map_pic_temp1"];
		$map_pic_temp2			=	$_POST["map_pic_temp2"];


		$file				=	$_FILES['map_pic1']['name'];
		$typefile			=	$_FILES['map_pic1']['type'];	
		$sizefile			=	$_FILES['map_pic1']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($map_pic_temp1 !=""){
						deletedata("../../_files/images/full/$map_pic_temp1");
						deletedata("../../_files/images/source/$map_pic_temp1");
						deletedata("../../_files/images/thumb/$map_pic_temp1");						
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['map_pic1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['map_pic1']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['map_pic1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$thumb = PhpThumbFactory::create($pathfull);
					$thumb->resize(272,180);
					$thumb->save($pathfull);

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize(272,180);
					$thumb->save($paththumb);

					$coverfile1	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_MAP SET MAP_PIC1='$coverfile1',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
					$result			=	mysql_query($SQL);

		}


		$file				=	$_FILES['map_pic2']['name'];
		$typefile			=	$_FILES['map_pic2']['type'];	
		$sizefile			=	$_FILES['map_pic2']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($map_pic_temp2 !=""){
						deletedata("../../_files/images/full/$map_pic_temp2");
						deletedata("../../_files/images/source/$map_pic_temp2");
						deletedata("../../_files/images/thumb/$map_pic_temp2");						
					}


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['map_pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['map_pic2']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['map_pic2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$thumb = PhpThumbFactory::create($pathfull);
					$thumb->resize(924,534);
					$thumb->save($pathfull);

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize(924,534);
					$thumb->save($paththumb);

					$coverfile2	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_MAP SET MAP_PIC2='$coverfile2',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
					$result			=	mysql_query($SQL);
		
		}







	$SQL			=	"UPDATE $DB_MAP SET MAP_TYPE='$map_type', MAP_NAME='$map_name', MAP_INTRO='$map_intro', MAP_TIME='$map_time', MAP_TEL='$map_tel', MAP_ADDRESS ='$map_address',
	UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);


		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_MAP WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$map_type			=	$row["MAP_TYPE"];
				$map_name			=	$row["MAP_NAME"];
				$map_pic_temp1		=	$row["MAP_PIC1"];
				$map_pic_temp2		=	$row["MAP_PIC2"];
				$map_intro			=	$row["MAP_INTRO"];
				$map_time			=	$row["MAP_TIME"];
				$map_tel			=	$row["MAP_TEL"];
				$map_address		=	$row["MAP_ADDRESS"];

			}


if($map_type==1){
	$map_1 ="SELECTED";
}
if($map_type==2){
	$map_2 ="SELECTED";
}

		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>