<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="contact";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="Contact";
$MODULE_DETAIL		="";
$MODULE_VERSION		="1.0";




$MODULE_PERMISSION	=",R,W,D,E,";


$MODULE_SUB[]		="FAQ|contact|faq.php";
$MODULE_SUB[]		="Your Question|contact|index.php";
$MODULE_SUB[]		="Map|contact|map.php";

?>