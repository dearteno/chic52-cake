<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="contact";
$page_sub_nav	="lineup_list";
$TITLE_TOPIC	="Contact";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="add.php";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="edit.php?id=<ID>";$UP_LINK="sort.php?action=up&id=<ID>";$DOWN_LINK="sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:page_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###

$p			= $_GET["p"];
if ($p =="" or !is_numeric($p)) {	$p		=	1;}
$PERPAGE	=	10;

### แสดงรายชื่อพนักงาน ###

$t				=	$_GET["t"];
if($t =="") {	$t ="all";}




		$SQL			=	"SELECT * FROM $DB_CONTACT ORDER BY ID DESC;";	

		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$subject			=	$row["SUBJECT"];
				$contact_name		=	$row["NAME"]." ".$row["SURNAME"];
				$tel				=	$row["TEL"];
				$email				=	$row["EMAIL"];
				$message			=	nl2br(htmlspecialchars($row["MESSAGE"]));
				$adddate			=	$row["ADDDATE"];
 	 	 		$status				=	$row["STATUS"]; 
 	 	 		$box_sn				=	$row["BOX_SN"]; 
 	 	 		$box_model			=	$row["BOX_MODEL"]; 
 	 	 		$box_package		=	$row["BOX_PACKAGE"]; 
 	 	 		$box_dish			=	$row["BOX_DISH"]; 
				

		$to_email	='gmmzonline@gmail.com';
		#$to_email	='cgimaster@gmail.com';
		$from_name	=$contact_name;
		$from_email =$email;
		$subject	="[GMM Z] รายการติดต่อจากคุณ $from_name";
		$date		= $adddate;
		#$date	= date("d-m-Y H:i:s", dateadd(Now, 12, "H"));
		$body_html ="
		วันที่ : $date<br/>
		ชื่อ : $from_name<br/>
		เบอร์ติดต่อ : $tel<br/>
		Email : $email<br/>
		<br/>
		หัวข้อ : $subject<br/>
		<br/>
		รายละเอียด<br/>
		$message<br/><br/>
		";

		if($box_sn !="" or $box_model !="" or $box_package !="" or $box_dish !=""){
		$body_html .="
		ข้อมูลกล่อง<br/>
		Serial Number : $box_sn<br/>
		รุ่นกล่องทีใช้ปัจจุบัน : $box_model<br/>
		แพคเกจปัจจุบัน : $box_package<br/>
		จานรับสัญญาณ : $box_dish<br/>
		";			
		}

		web_sendmail($to_email,$to_email,$from_name,$from_email,$subject,$body_html);
				
sleep(1);

echo "$from_email<Br/>";
			}
 	 	 



ob_end_flush();
mysql_close();
?>