<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

// Turn off all error reporting
error_reporting(0);

$page_nav		="content";

$t		=	$_GET["t"];
if($t ==""){
$t		=	$_POST["t"];	
}




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_add.html");


		$SQL			=	"SELECT * FROM $DB_CONTENT_TOPIC WHERE ID='$t';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if($count==0){
			exit;
		}
			while ($row		=	mysql_fetch_array($result)){	
				$tname		=	$row["TOPIC_NAME"];
			}


$TITLE_TOPIC	="เนื้อหา / <a href='index.php?t=$t'>$tname</a> / เพิ่ม";




$action		=	$_POST["action"];



$content_date	=	date("d-m-Y");
$display_date	=	date("d-m-Y");

if ($action =="add") {

		$now_date		=	date("Y-m-d");
		$content_folder	=	date("Y-m-d");
		$hour			=	$_POST["hour"];
		$min			=	$_POST["min"];

		if (!is_dir("../../_files/images/source/$content_folder")){mkdir("../../_files/images/source/$content_folder");chmod("../../_files/images/source/$content_folder", 0777); }
		if (!is_dir("../../_files/images/full/$content_folder")){mkdir("../../_files/images/full/$content_folder");chmod("../../_files/images/full/$content_folder", 0777); }
		if (!is_dir("../../_files/images/thumb/$content_folder")){mkdir("../../_files/images/thumb/$content_folder");chmod("../../_files/images/thumb/$content_folder", 0777); }

		### SEO ###
		$seo_url			=	generate_seo_link($_POST["seo_url"], '-', false,'');
		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$file				=	$_FILES['seo_image']['name'];
		$typefile			=	$_FILES['seo_image']['type'];	
		$sizefile			=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$seofile	=	"$ran$typecheck1";

		}
		### SEO ###



		$content_name			=	$_POST["content_name"];
		$content_intro			=	$_POST["content_intro"];
		$content_tag			=	$_POST["content_tag"];
		$content_content		=	$_POST["content_content"];
		$content_stick			=	$_POST["content_stick"];
		$content_status			=	$_POST["content_status"];
		$facebook_comment		=	$_POST["facebook_comment"];
		$content_date			=	coverttime_y_m_d($_POST["content_date"]);
		$display_date			=	coverttime_y_m_d($_POST["display_date"]);

		if($hour !='#' AND $min !='#'){
			$display_date ="$display_date $hour:$min:00";
		}else{
			$display_date ="$display_date 00:00:00";		
		}

		if ($content_status =="Y") {$content_status ="S";}else{$content_status ="H";}
		if ($content_stick =="Y") {$content_stick ="S";}else{$content_stick ="H";}
		if ($facebook_comment =="Y") {$facebook_comment ="Y";}else{$facebook_comment ="N";}




		$check_date		=	date("Y-m-d h:m:s");

		if($display_date > $check_date){
			if($content_stick =="S"){
				$content_stick="W";
			}
			if($content_status =="S"){
				$content_status="W";
			}
		}
/*
		if($content_status > $now_date){		$content_status ="W";		}
		if($content_stick > $now_date){			$content_stick	="W";		}
*/

		$file				=	$_FILES['content_cover']['name'];
		$typefile			=	$_FILES['content_cover']['type'];	
		$sizefile			=	$_FILES['content_cover']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$content_folder/$ran$typecheck1";
					copy($_FILES['content_cover']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$content_folder/$ran$typecheck1";
					copy($_FILES['content_cover']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$content_folder/$ran$typecheck1";
					copy($_FILES['content_cover']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$w2=400;

					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = new Thumbnail($pathfull);
					$thumb->resize($w2,$h2);
					//$thumb->crop(0,0,270,125);
					$thumb->save($pathfull);

					if($t==5){	$w2=420; $h2=280;}
					else{$w2=295; $h2=180;}


					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {

						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$coverfile	=	"$ran$typecheck1";

		}





		$SQL_IMAGE_INSERT ="'<IMG1>', '<TXT1>', '<IMG2>', '<TXT2>', '<IMG3>', '<TXT3>', '<IMG4>', '<TXT4>', '<IMG5>', '<TXT5>', '<IMG6>', '<TXT6>',";

		for ($i=1;$i<=6;$i++ ) {

				$file				=	$_FILES["content_imagecontent$i"]['name'];
				$typefile			=	$_FILES["content_imagecontent$i"]['type'];	
				$sizefile			=	$_FILES["content_imagecontent$i"]['size'];
				$image_text			=	$_POST["content_imagetext$i"];
				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
							
							$ran		=random_string(8);
							$paththumb	="../../_files/images/source/$content_folder/$ran$typecheck1";
							copy($_FILES["content_imagecontent$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/images/full/$content_folder/$ran$typecheck1";
							copy($_FILES["content_imagecontent$i"]['tmp_name'],$pathfull);
							chmod("$paththumb", 0777); 
							$paththumb	="../../_files/images/thumb/$content_folder/$ran$typecheck1";
							copy($_FILES["content_imagecontent$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 

							list($w1, $h1) = getimagesize($pathfull);

							$w2 = 580 ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;

							$thumb = new Thumbnail($pathfull);
							$thumb->resize($w2,$h2);
							$thumb->save($pathfull);

							list($w1, $h1) = getimagesize($paththumb);
							if ($w1 > $h1) {
								$w2 = 120 ;
								$percent = $w2/$w1;
								$h2 = $h1 * $percent;
							}else{
								$h2 = 120 ;
								$percent = $h2/$h1;
								$w2 = $w1 * $percent;				
							}

							$thumb = new Thumbnail($paththumb);
							$thumb->resize($w2,$h2);
							$thumb->save($paththumb);
						
							$IMG_ADD ="$ran$typecheck1";
							$TXT_ADD =$image_text;

				}else{
							$IMG_ADD ='';
							$TXT_ADD ='';				
				
				}

				$SQL_IMAGE_INSERT = preg_replace("/<IMG$i>/i", "$IMG_ADD", $SQL_IMAGE_INSERT);
				$SQL_IMAGE_INSERT = preg_replace("/<TXT$i>/i", "$TXT_ADD", $SQL_IMAGE_INSERT);

		}



		$SQL			=	"SELECT * FROM $DB_CONTENT WHERE TOPIC_ID='$t' ORDER BY CONTENT_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row			=	mysql_fetch_array($result)){	
				$content_sort	=	$row["CONTENT_SORT"];
			}
		$content_sort		= $content_sort+1;

		$array_tag_new			=	preg_split("/,/i",$content_tag);

		for ($i=0;$i<=count($array_tag_new)-1;$i++) {
			$array_tag_new[$i]	=	trim($array_tag_new[$i]);

			if (!empty($array_tag_new[$i])) {
					$tag_new		=	$array_tag_new[$i];
					$tag_show		=	generate_seo_link($tag_new, '-', false,'');
					$SQL			=	"SELECT * FROM $DB_TAG WHERE TAG_NAME='$tag_new';";	
					$result			=	mysql_query($SQL);
					$count			=	mysql_num_rows($result);
					if ($count ==0) {

						$SQL2			=	"INSERT INTO $DB_TAG VALUES ('', '$tag_new','$tag_show');";	
						$result2			=	mysql_query($SQL2);

						$SQL2			=	"SELECT * FROM $DB_TAG WHERE TAG_NAME='$tag_new' AND TAG_SHOW='$tag_show';";	
						$result2			=	mysql_query($SQL2);

						while ($row2		=	mysql_fetch_array($result2)){	
							$ID				=	$row2["ID"];
							$tag_content_add	.=	"$ID,";
						}				

					}else{

						while ($row		=	mysql_fetch_array($result)){	
							$ID				=	$row["ID"];
							$tag_content_add	.=	"$ID,";
						}				
					
					}
					
					$tag_name_add	.=$tag_new.",";
			}

		}


		if (!empty($tag_content_add)) {
			$tag_content_add=	",$tag_content_add";
		}
		if (!empty($tag_name_add)) {
			$tag_name_add=	",$tag_name_add";
		}


		$SQL			=	"INSERT INTO $DB_CONTENT VALUES ('', '$t', '$content_name', '$content_intro', '$coverfile', '$content_content', '$content_stick', '$content_sort', '$content_status', '$content_date', '$display_date', $SQL_IMAGE_INSERT '$tag_content_add', '$tag_name_add', '$content_folder', 0, '$seo_url', '$seo_title', '$seo_description', '$seo_keyword', '$seofile', '$facebook_comment', '$U_USERNAME', NOW(), '$U_USERNAME', NOW());";	
		$result			=	mysql_query($SQL);



		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>