<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");


// Turn off all error reporting
error_reporting(0);

$page_nav		="content";


$TITLE_TOPIC	="ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		$p				=	$_GET['p'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_CONTENT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_cover		=	$row["CONTENT_COVER"];
				$seo_image		=	$row["SEO_IMAGE"];
				$content_image1		=	$row["CONTENT_IMAGE1"];
				$content_image2		=	$row["CONTENT_IMAGE2"];
				$content_image3		=	$row["CONTENT_IMAGE3"];
				$content_image4		=	$row["CONTENT_IMAGE4"];
				$content_image5		=	$row["CONTENT_IMAGE5"];
				$content_image6		=	$row["CONTENT_IMAGE6"];
				$content_folder		=	$row["CONTENT_FOLDER"];
				$topic_id			=	$row["TOPIC_ID"];
			}


					if($content_image1 !="" AND $content_image1 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_image1");
						deletedata("../../_files/images/source/$content_folder/$content_image1");
						deletedata("../../_files/images/thumb/$content_folder/$content_image1");
					}
					if($content_image2 !="" AND $content_image2 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_image2");
						deletedata("../../_files/images/source/$content_folder/$content_image2");
						deletedata("../../_files/images/thumb/$content_folder/$content_image2");
					}
					if($content_image3 !="" AND $content_image3 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_image3");
						deletedata("../../_files/images/source/$content_folder/$content_image3");
						deletedata("../../_files/images/thumb/$content_folder/$content_image3");
					}
					if($content_image4 !="" AND $content_image4 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_image4");
						deletedata("../../_files/images/source/$content_folder/$content_image4");
						deletedata("../../_files/images/thumb/$content_folder/$content_image4");
					}
					if($content_image5 !="" AND $content_image5 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_image5");
						deletedata("../../_files/images/source/$content_folder/$content_image5");
						deletedata("../../_files/images/thumb/$content_folder/$content_image5");
					}
					if($content_image6 !="" AND $content_image6 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_image6");
						deletedata("../../_files/images/source/$content_folder/$content_image6");
						deletedata("../../_files/images/thumb/$content_folder/$content_image6");
					}
					if($content_cover !="" AND $content_cover !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$content_cover");
						deletedata("../../_files/images/source/$content_folder/$content_cover");
						deletedata("../../_files/images/thumb/$content_folder/$content_cover");
					}

					if($seo_image !="" AND $seo_image !="blank_user.jpg") {
						deletedata("../../_files/images/full/$content_folder/$seo_image");
						deletedata("../../_files/images/source/$content_folder/$seo_image");
						deletedata("../../_files/images/thumb/$content_folder/$seo_image");
					}

$SQL			=	"DELETE FROM $DB_CONTENT WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>