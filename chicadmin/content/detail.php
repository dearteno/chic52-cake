<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="content";


$TITLE_TOPIC	="<a href='index.php'>เนื้อหา</a>&nbsp;/&nbsp;รายละเอียด";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_detail	=	new Template("_tp_detail.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}




$WRITE_LINK_HTML ="edit.php?id=$id";

		$SQL			=	"SELECT * FROM $DB_CONTENT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_name		=	$row["CONTENT_NAME"];
				$topic_id			=	$row["TOPIC_ID"];
				$content_intro		=	$row["CONTENT_INTRO"];				
				$content_cover		=	$row["CONTENT_COVER"];
				$content_content		=	$row["CONTENT_CONTENT"];

				$content_homepage		=	$row["CONTENT_HOMEPAGE"];
				$content_status			=	$row["CONTENT_STATUS"];
				$content_date			=	coverttime_y_m_d($row["CONTENT_DATE"]);
				$content_date_show		=	coverttime_y_m_d($row["CONTENT_DATE_SHOW"]);

				$content_image1		=	$row["CONTENT_IMAGE1"];
				$content_image_text1	=	$row["CONTENT_IMAGE_TEXT1"];
				$content_image2		=	$row["CONTENT_IMAGE2"];
				$content_image_text2	=	$row["CONTENT_IMAGE_TEXT2"];
				$content_image3		=	$row["CONTENT_IMAGE3"];
				$content_image_text3	=	$row["CONTENT_IMAGE_TEXT3"];
				$content_image4		=	$row["CONTENT_IMAGE4"];
				$content_image_text4	=	$row["CONTENT_IMAGE_TEXT4"];
				$content_image5		=	$row["CONTENT_IMAGE5"];
				$content_image_text5	=	$row["CONTENT_IMAGE_TEXT5"];
				$content_image6		=	$row["CONTENT_IMAGE6"];
				$content_image_text6	=	$row["CONTENT_IMAGE_TEXT6"];				

				$content_tags_id	=	$row["CONTENT_TAGS_ID"];
				$content_tags_name	=	$row["CONTENT_TAGS_NAME"];
				$content_folder		=	$row["CONTENT_FOLDER"];

				$seo_url			=	$row["SEO_URL"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];


				$facebook_comment		=	$row["FACEBOOK_COMMENT"];

			}

		if($facebook_comment =="Y"){
			$facebook_comment ='<span class="button submit medium green-back ui-corner-all" style="width:200px;text-align:center;font-size:12px;">เปิดใช้งาน Facebook Comment</span>';
		}
		if($facebook_comment =="N"){
			$facebook_comment ='<span class="button submit medium grey-back ui-corner-all" style="width:200px;text-align:center;font-size:12px;">ไม่ใช้งาน Facebook Comment</span>';
		}

		if($content_image1 !=""){
			$content_imagecontent1_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image1\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent1_temp\" id=\"content_imagecontent1_temp\" value=\"$content_image1\" />";
		}
		if($content_image2 !=""){
			$content_imagecontent2_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image2\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent2_temp\" id=\"content_imagecontent2_temp\" value=\"$content_image2\" />";
		}
		if($content_image3 !=""){
			$content_imagecontent3_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image3\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent3_temp\" id=\"content_imagecontent3_temp\" value=\"$content_image3\" />";
		}
		if($content_image4 !=""){
			$content_imagecontent4_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image4\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent4_temp\" id=\"content_imagecontent4_temp\" value=\"$content_image4\" />";
		}	
		if($content_image5 !=""){
			$content_imagecontent5_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image5\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent4_temp\" id=\"content_imagecontent5_temp\" value=\"$content_image5\" />";
		}	
		if($content_image6 !=""){
			$content_imagecontent6_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image6\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent4_temp\" id=\"content_imagecontent6_temp\" value=\"$content_image6\" />";
		}	

if ($content_status=="S") {
    $content_status ='<span class="button submit medium green-back ui-corner-all" style="width:160px;text-align:center;font-size:12px;">แสดงผลหน้า Homepage</span>';
} 	 	 	 
if ($content_status=="H") {
    $content_status ='<span class="button submit medium grey-back ui-corner-all" style="width:160px;text-align:center;font-size:12px;">ซ่อนหน้า Homepage</span>';
} 	 
if ($content_homepage=="S") {
    $content_homepage ='<span class="button submit medium green-back ui-corner-all" style="width:120px;text-align:center;font-size:12px;">แสดงผลหน้า List</span>';
} 	 	 	 
if ($content_homepage=="H") {
    $content_homepage ='<span class="button submit medium grey-back ui-corner-all" style="width:120px;text-align:center;font-size:12px;">ซ่อนหน้า List</span>';
} 	 






$CONTENT_HTML	=	$tp_detail->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>