<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

// Turn off all error reporting
error_reporting(0);

$page_nav		="content";


$TITLE_TOPIC	="<a href='index.php'>เนื้อหา</a>&nbsp;/&nbsp;แก้ไข";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {


		$now_date		=	date("Y-m-d");
		$hour			=	$_POST["hour"];
		$min			=	$_POST["min"];

		$SQL			=	"SELECT * FROM $DB_CONTENT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$topic_id			=	$row["TOPIC_ID"];
				$content_folder		=	$row["CONTENT_FOLDER"];
			}



		$SQL			=	"SELECT * FROM $DB_CONTENT_TOPIC WHERE ID='$topic_id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if($count==0){
			exit;
		}
			while ($row		=	mysql_fetch_array($result)){	
				$tname		=	$row["TOPIC_NAME"];
			}


		### SEO ###
		$seo_url			=	generate_seo_link($_POST["seo_url"], '-', false,'');

		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$file				=	$_FILES['seo_image']['name'];
		$typefile			=	$_FILES['seo_image']['type'];	
		$sizefile			=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$seofile	=	"$ran$typecheck1";
					$SQL		=	"UPDATE $DB_CONTENT SET SEO_IMAGE='$seofile' WHERE ID='$id';";	
					$result		=	mysql_query($SQL);
		}
		### SEO ###




		$content_name			=	$_POST["content_name"];
		$content_intro			=	$_POST["content_intro"];
		$content_tag			=	$_POST["content_tag"];
		$content_content		=	$_POST["content_content"];
		$content_stick			=	$_POST["content_stick"];
		$content_status			=	$_POST["content_status"];
		$facebook_comment		=	$_POST["facebook_comment"];
		$content_date	=	coverttime_y_m_d($_POST["content_date"]);
		$display_date	=	coverttime_y_m_d($_POST["display_date"]);

		if($hour !='#' AND $min !='#'){
			$display_date ="$display_date $hour:$min:00";
		}else{
			$display_date ="$display_date 00:00:00";		
		}

		if ($content_status =="Y") {$content_status ="S";}else{$content_status ="H";}
		if ($content_stick =="Y") {$content_stick ="S";}else{$content_stick ="H";}
		if ($facebook_comment =="Y") {$facebook_comment ="Y";}else{$facebook_comment ="N";}


		$check_date		=	date("Y-m-d h:m:s");

		if($display_date > $check_date){
			if($content_stick =="S"){
				$content_stick="W";
			}
			if($content_status =="S"){
				$content_status="W";
			}
		}


/*
		if($display_date > $now_date){		$content_status ="W";		}
		if($display_date > $now_date){			$content_stick	="W";		}
*/
		$file				=	$_FILES['content_cover']['name'];
		$typefile			=	$_FILES['content_cover']['type'];	
		$sizefile			=	$_FILES['content_cover']['size'];


		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$content_folder/$ran$typecheck1";
					copy($_FILES['content_cover']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$content_folder/$ran$typecheck1";
					copy($_FILES['content_cover']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$content_folder/$ran$typecheck1";
					copy($_FILES['content_cover']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$w2 = 400 ;
					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = new Thumbnail($pathfull);
					$thumb->resize($w2,$h2);
					//$thumb->crop(0,0,270,125);
					$thumb->save($pathfull);

					if($t==5){	$w2=420; $h2=280;}
					else{$w2=295; $h2=180;}

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
					
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$coverfile	=	"$ran$typecheck1";
					$SQL		=	"UPDATE $DB_CONTENT SET CONTENT_COVER='$coverfile' WHERE ID='$id';";	
					$result		=	mysql_query($SQL);
		}




		for ($i=1;$i<=6;$i++ ) {

				$file				=	$_FILES["content_imagecontent$i"]['name'];
				$typefile			=	$_FILES["content_imagecontent$i"]['type'];	
				$sizefile			=	$_FILES["content_imagecontent$i"]['size'];
				$image_text			=	$_POST["content_imagetext$i"];
				$image_del			=	$_POST["content_imagecontent".$i."_temp"];

				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
							
							$ran		=random_string(8);
							$paththumb	="../../_files/images/source/$content_folder/$ran$typecheck1";
							copy($_FILES["content_imagecontent$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/images/full/$content_folder/$ran$typecheck1";
							copy($_FILES["content_imagecontent$i"]['tmp_name'],$pathfull);
							chmod("$paththumb", 0777); 
							$paththumb	="../../_files/images/thumb/$content_folder/$ran$typecheck1";
							copy($_FILES["content_imagecontent$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 

							list($w1, $h1) = getimagesize($pathfull);

							$w2 = 580 ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;

							$thumb = new Thumbnail($pathfull);
							$thumb->resize($w2,$h2);
							$thumb->save($pathfull);

							list($w1, $h1) = getimagesize($paththumb);
							if ($w1 > $h1) {
								$w2 = 120 ;
								$percent = $w2/$w1;
								$h2 = $h1 * $percent;
							}else{
								$h2 = 120 ;
								$percent = $h2/$h1;
								$w2 = $w1 * $percent;				
							}

							$thumb = new Thumbnail($paththumb);
							$thumb->resize($w2,$h2);
							$thumb->save($paththumb);
						
							$IMG_ADD ="$ran$typecheck1";
							$TXT_ADD =$image_text;

							$SQL			=	"UPDATE $DB_CONTENT SET CONTENT_IMAGE$i='$IMG_ADD' WHERE ID='$id';";	
							$result			=	mysql_query($SQL);							

				}


		}
		for ($i=1;$i<=6;$i++ ) {

							$image_text			=	$_POST["content_imagetext$i"];
							$SQL			=	"UPDATE $DB_CONTENT SET CONTENT_IMAGE_TEXT$i='$image_text' WHERE ID='$id';";	
							$result			=	mysql_query($SQL);	
		}


		$array_tag_new			=	preg_split("/,/i",$content_tag);

		for ($i=0;$i<=count($array_tag_new)-1;$i++) {
			$array_tag_new[$i]	=	trim($array_tag_new[$i]);

			if (!empty($array_tag_new[$i])) {
					$tag_new		=	$array_tag_new[$i];
					$tag_show		=	generate_seo_link($tag_new, '-', false,'');
					$SQL			=	"SELECT * FROM $DB_TAG WHERE TAG_NAME='$tag_new';";	
					$result			=	mysql_query($SQL);
					$count			=	mysql_num_rows($result);
					if ($count ==0) {

						$SQL2			=	"INSERT INTO $DB_TAG VALUES ('', '$tag_new','$tag_show');";	
						$result2			=	mysql_query($SQL2);

						$SQL2			=	"SELECT * FROM $DB_TAG WHERE TAG_NAME='$tag_new' AND TAG_SHOW='$tag_show';";	
						$result2			=	mysql_query($SQL2);

						while ($row2		=	mysql_fetch_array($result2)){	
							$ID				=	$row2["ID"];
							$tag_content_add	.=	"$ID,";
						}				

					}else{

						while ($row		=	mysql_fetch_array($result)){	
							$ID				=	$row["ID"];
							$tag_content_add	.=	"$ID,";
						}				
					
					}
					
					$tag_name_add	.=$tag_new.",";
			}

		}


		if (!empty($tag_content_add)) {
			$tag_content_add=	",$tag_content_add";
		}
		if (!empty($tag_name_add)) {
			$tag_name_add=	",$tag_name_add";
		}



	$SQL			=	"UPDATE $DB_CONTENT SET CONTENT_NAME='$content_name',CONTENT_INTRO='$content_intro',CONTENT_CONTENT='$content_content',CONTENT_HOMEPAGE='$content_stick',CONTENT_STATUS='$content_status',CONTENT_DATE='$content_date',CONTENT_DATE_SHOW='$display_date',	CONTENT_TAGS_ID='$tag_content_add',CONTENT_TAGS_NAME='$tag_name_add',SEO_URL='$seo_url',SEO_TITLE='$seo_title',SEO_DESCRIPTION='$seo_description',SEO_KEYWORD='$seo_keyword',FACEBOOK_COMMENT='$facebook_comment',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);


$TITLE_TOPIC	="เนื้อหา / <a href='index.php?t=$t'>$tname</a> / แก้ไข";


		$SQL			=	"SELECT * FROM $DB_CONTENT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$t			=	$row["TOPIC_ID"];
			}

		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_CONTENT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$content_name		=	$row["CONTENT_NAME"];
				$topic_id			=	$row["TOPIC_ID"];
				$content_intro		=	$row["CONTENT_INTRO"];				
				$content_cover		=	$row["CONTENT_COVER"];
				$content_content		=	$row["CONTENT_CONTENT"];

				$content_homepage		=	$row["CONTENT_HOMEPAGE"];
				$content_status			=	$row["CONTENT_STATUS"];
				$content_date			=	$row["CONTENT_DATE"];
				$content_date_show		=	$row["CONTENT_DATE_SHOW"];

				$content_image1		=	$row["CONTENT_IMAGE1"];
				$content_image_text1	=	$row["CONTENT_IMAGE_TEXT1"];
				$content_image2		=	$row["CONTENT_IMAGE2"];
				$content_image_text2	=	$row["CONTENT_IMAGE_TEXT2"];
				$content_image3		=	$row["CONTENT_IMAGE3"];
				$content_image_text3	=	$row["CONTENT_IMAGE_TEXT3"];
				$content_image4		=	$row["CONTENT_IMAGE4"];
				$content_image_text4	=	$row["CONTENT_IMAGE_TEXT4"];
				$content_image5		=	$row["CONTENT_IMAGE5"];
				$content_image_text5	=	$row["CONTENT_IMAGE_TEXT5"];
				$content_image6		=	$row["CONTENT_IMAGE6"];
				$content_image_text6	=	$row["CONTENT_IMAGE_TEXT6"];				

				$content_tags_id	=	$row["CONTENT_TAGS_ID"];
				$content_tags_name	=	$row["CONTENT_TAGS_NAME"];
				$content_folder		=	$row["CONTENT_FOLDER"];

				$seo_url			=	$row["SEO_URL"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];


				$facebook_comment		=	$row["FACEBOOK_COMMENT"];

			}





		$SQL			=	"SELECT * FROM $DB_CONTENT_TOPIC WHERE ID='$topic_id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if($count==0){
			exit;
		}
			while ($row		=	mysql_fetch_array($result)){	
				$tname		=	$row["TOPIC_NAME"];
			}


$TITLE_TOPIC	="เนื้อหา / <a href='index.php?t=$t'>$tname</a> / แก้ไข";


if($content_status =="S"){
	$content_status_y="CHECKED";
}

if($content_homepage =="S"){
	$content_homepage_y="CHECKED";
}


if($facebook_comment =="Y"){
	$facebook_comment_y="CHECKED";
}

				$check_date		=	date("d-m-Y");

				if($content_date_show > $check_date){
					$content_status_y="CHECKED";
					$content_homepage_y="CHECKED";
				}


				$content_date			=	coverttime_y_m_d($content_date);

				$content_date_show		=	coverttime_y_m_d_h_m_s($content_date_show);
				list($content_date_show,$content_hour)					=	preg_split("/ /i",$content_date_show);
				list($content_hour,$content_min,$content_sec)			=	preg_split("/:/i",$content_hour);



$content_hour_html ='<select name="hour" id="hour" style="width:70px;"><option value="#">เลือก</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>';
$content_hour_html =preg_replace("/value=\"$content_hour\">/i","value=\"$content_hour\" SELECTED>",$content_hour_html);

$content_min_html ='<select name="min" id="min" style="width:70px;"><option value="#">เลือก</option><option value="00">00</option><option value="05">05</option><option value="10">10</option><option value="15">15</option><option value="20">20</option><option value="25">25</option><option value="30">30</option><option value="35">35</option><option value="40">40</option><option value="45">45</option><option value="50">50</option><option value="55">55</option></select>';
$content_min_html =preg_replace("/value=\"$content_min\">/i","value=\"$content_min\" SELECTED>",$content_min_html);

$array_tag_new			=	preg_split("/,/",$content_tags_id);

for ($i=0;$i<=count($array_tag_new)-1;$i++) {
	$array_tag_new[$i]	=	trim($array_tag_new[$i]);
	if (!empty($array_tag_new[$i])) {
			$tag_new	=	$array_tag_new[$i];
			$SQL			=	"SELECT * FROM $DB_TAG WHERE ID='$tag_new';";	
			$result			=	mysql_query($SQL);
			$count			=	mysql_num_rows($result);
			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$TAG_NAME		=	$row["TAG_NAME"];
				$tags_insert .="$TAG_NAME, ";
			}
	}
}

		if($content_image1 !=""){
			$content_imagecontent1_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image1\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent1_temp\" id=\"content_imagecontent1_temp\" value=\"$content_image1\" />";
		}
		if($content_image2 !=""){
			$content_imagecontent2_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image2\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent2_temp\" id=\"content_imagecontent2_temp\" value=\"$content_image2\" />";
		}
		if($content_image3 !=""){
			$content_imagecontent3_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image3\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent3_temp\" id=\"content_imagecontent3_temp\" value=\"$content_image3\" />";
		}
		if($content_image4 !=""){
			$content_imagecontent4_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image4\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent4_temp\" id=\"content_imagecontent4_temp\" value=\"$content_image4\" />";
		}		
		if($content_image5 !=""){
			$content_imagecontent5_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image5\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent5_temp\" id=\"content_imagecontent5_temp\" value=\"$content_image5\" />";
		}	
		if($content_image6 !=""){
			$content_imagecontent6_html ="<br/><img src=\"../../_files/images/thumb/$content_folder/$content_image6\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"content_imagecontent6_temp\" id=\"content_imagecontent6_temp\" value=\"$content_image6\" />";
		}	

		if ($content_status =="S") {
		    $content_status_s ="CHECKED";
		}

		if ($content_status =="H") {
		    $content_status_h ="CHECKED";		    
		}

		if ($content_stick>100000) {
		    $content_stick_y ="CHECKED";
		}else{
		    $content_stick_n ="CHECKED";		
		}





		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>