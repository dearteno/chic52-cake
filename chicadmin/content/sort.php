<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="content";
$page_sub_nav	="list";
$TITLE_TOPIC	="<a href='index.php'>เนื้อหา</a> / แก้ไข";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}



### เรียงลำดับใหม่ ###

$act	=	$_GET["action"];
$id		=	$_GET["id"];
$t		=	$_GET["t"];
$p		=	$_GET["p"];


	$SQL		="SELECT * FROM $DB_CONTENT WHERE ID='$id'";
	$result		=mysql_query($SQL);
		while ($row		=	mysql_fetch_array($result)){	
			$CONTENT_SORT1	=	$row["CONTENT_SORT"];
			$TOPIC_ID		=	$row["TOPIC_ID"];
		}


if ($act =="down") {
	
	$SQL		="SELECT * FROM $DB_CONTENT WHERE CONTENT_SORT < '$CONTENT_SORT1' AND TOPIC_ID='$TOPIC_ID' ORDER BY CONTENT_SORT DESC LIMIT 0,1";

	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);
	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$CONTENT_SORT2		=	$row["CONTENT_SORT"];
		}

		$SQL		="UPDATE $DB_CONTENT SET CONTENT_SORT='$CONTENT_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_CONTENT SET CONTENT_SORT='$CONTENT_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}
}

if ($act =="up") {

	$SQL		="SELECT * FROM $DB_CONTENT WHERE CONTENT_SORT > '$CONTENT_SORT1' AND TOPIC_ID='$TOPIC_ID' ORDER BY CONTENT_SORT LIMIT 0,1";
	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);

	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$CONTENT_SORT2		=	$row["CONTENT_SORT"];
		}

		$SQL		="UPDATE $DB_CONTENT SET CONTENT_SORT='$CONTENT_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_CONTENT SET CONTENT_SORT='$CONTENT_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}

}


header("Location: index.php?p=$p&t=$t");
ob_end_flush();
mysql_close();
exit;
?>