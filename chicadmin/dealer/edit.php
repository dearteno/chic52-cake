<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

// Turn off all error reporting
error_reporting(0);

$page_nav		="dealer";


$TITLE_TOPIC	="<a href='index.php'>ร้านค้าทีวีดาวเทียม</a>&nbsp;/&nbsp;แก้ไข";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {



		$dealer_name			=	$_POST["dealer_name"];
		$dealer_province		=	$_POST["dealer_province"];
		$dealer_address			=	$_POST["dealer_address"];
		$dealer_tel				=	$_POST["dealer_tel"];
		$dealer_zip				=	$_POST["dealer_zip"];
		$content_status			=	$_POST["content_status"];

		if ($content_status =="Y") {$content_status ="S";}else{$content_status ="H";}



	$SQL			=	"UPDATE $DB_DEALER SET DEALER_NAME='$dealer_name', DEALER_ADDRESS='$dealer_address', DEALER_TEL='$dealer_tel', DEALER_PROVINCE='$dealer_province', DEALER_ZIP='$dealer_zip', DEALER_STATUS='$content_status',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_DEALER WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$dealer_name		=	$row["DEALER_NAME"];
				$dealer_address			=	$row["DEALER_ADDRESS"];
				$dealer_tel			=	$row["DEALER_TEL"];
				$dealer_province			=	$row["DEALER_PROVINCE"];
				$dealer_zip			=	$row["DEALER_ZIP"];
				$dealer_status			=	$row["DEALER_STATUS"];
			}


if($dealer_status =="S"){
	$content_status_y="CHECKED";
}




					$PROVINCE_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PROVINCE ORDER BY PROVINCE_ID;";	
				$result2			=	mysql_query($SQL2);
				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["PROVINCE_ID"];
					$PROVINCE_NAME			=	trim($row2["PROVINCE_NAME"]);
					if($dealer_province == $PID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$PROVINCE_HTML .=" <option value=\"$PID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$PROVINCE_NAME</option>\n";
				}





		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>