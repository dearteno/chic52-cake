<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="dealer";
$page_sub_nav	="lineup_list";


include ("../menu.php");
include ("module_info.php");


$t			= $_GET["t"];
$p			= $_GET["p"];
if ($p =="" or !is_numeric($p)) {	$p		=	1;}
if ($t =="" or !is_numeric($t) or $t >76 or $t<=0) {	$t		=	1;}

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="add.php?t=$t";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="edit.php?id=<ID>";$UP_LINK="sort.php?action=up&id=<ID>&t=$t&p=$p";$DOWN_LINK="sort.php?action=down&id=<ID>&t=$t&p=$p";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:page_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###


$PERPAGE	=	10;

### แสดงรายชื่อพนักงาน ###

if($t !=""){
	$T_SQL ="AND (DEALER_PROVINCE='$t')";
}





$TITLE_TOPIC	="ร้านค้าทีวีดาวเทียม";


$tp				=	new Template("../_tp_main.html");

$tp_page		=	new Template("_tp_list.html");


		$SQL			=	"SELECT * FROM $DB_DEALER WHERE ID!='' $T_SQL;";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);







$total_records = $count;
$scroll_page = 10; // จำนวน scroll page
$per_page = $PERPAGE; // จำนวน record ต่อหนึ่ง page
$current_page = $p; // หน้าปัจจุบัน
$pager_url = "index.php?t=$t&p="; // url page ที่ต้องการเรียก
$inactive_page_tag = ''; // 
$previous_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_previous.png">'; // หน้าก่อนหน้า
$next_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_next.png">'; // หน้าถัดไป
$first_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_first.png">'; // ไปหน้าแรก
$last_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_last.png">'; // ไปหน้าสุดท้าย

$kgPagerOBJ = new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);


$pagelink .=$kgPagerOBJ -> first_page;
$pagelink .=$kgPagerOBJ -> previous_page;
$pagelink .=$kgPagerOBJ -> page_links;
$pagelink .=$kgPagerOBJ -> next_page;
$pagelink .=$kgPagerOBJ -> last_page;

if($pagelink=="<span>1</span>") {
	$pagelink="";
}




			if ($p	== 1) {
				$start	=0;
				$limit	=$PERPAGE;
				$i		=1;
			}else{
				$start	=(($PERPAGE*$p)-$PERPAGE);
				$limit	=$PERPAGE;
				$i		=(($PERPAGE*$p)-$PERPAGE)+1;
			}



$tp_page->Block("MEMBER_LIST");
$tp_page->Sub(2);







		$SQL			=	"SELECT * FROM $DB_DEALER WHERE ID!='' $T_SQL ORDER BY CONVERT (DEALER_NAME USING tis620) LIMIT $start,$limit;";	

		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$content_question	=	$row["DEALER_NAME"];
				$content_status		=	$row["DEALER_STATUS"];


				if ($content_status=="S") {
					$STATUS ='<img src="../images/icons/accept32.png">';
				} 	 	 	 
				if ($content_status=="H") {
					$STATUS ='<img src="../images/icons/cancel32.png">';
				} 	
				if ($content_status=="W") {
					$STATUS ='<img src="../images/icons/time32.png">';
				} 	
				$C_USERNAME 		=	$row["CREATE_USERNAME"];
				$C_TIME		 		=	$row["CREATE_TIME"];
				$L_USERNAME 		=	$row["UPDATE_USERNAME"];
				$L_TIME		 		=	$row["UPDATE_TIME"];				


				if($C_TIME=="0000-00-00 00:00:00"){$C_TIME ="";}
				if($L_TIME=="0000-00-00 00:00:00"){$L_TIME ="";}

				$EDIT_LINK_HTML	=$EDIT_LINK;
				$DEL_LINK_HTML	=$DEL_LINK;
				$UP_LINK_HTML	=$UP_LINK;
				$DOWN_LINK_HTML	=$DOWN_LINK;

				$EDIT_LINK_HTML =preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);
				$DEL_LINK_HTML =preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);
				$UP_LINK_HTML =preg_replace("/<ID>/i",$ID,$UP_LINK_HTML);
				$DOWN_LINK_HTML =preg_replace("/<ID>/i",$ID,$DOWN_LINK_HTML);

				$content_read	=	$row["CONTENT_READ"];
				

				$content_url =HOSTNAMEURL."/$path_link/$seo_url";
				$content_url ="<a href='$content_url' target='_blank' class='nopad'>$content_url</a>";
	
				$tp_page->Apply();
				$i++;

			}
 	 	 


					$PROVINCE_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PROVINCE ORDER BY PROVINCE_ID;";	
				$result2			=	mysql_query($SQL2);
				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["PROVINCE_ID"];
					$PROVINCE_NAME			=	trim($row2["PROVINCE_NAME"]);
					if($t == $PID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$PROVINCE_HTML .=" <option value=\"index.php?t=$PID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$PROVINCE_NAME</option>\n";
				}



$CONTENT_HTML	=	$tp_page->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>