<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="dealer";
$page_sub_nav	="list";
$TITLE_TOPIC	="<a href='index.php'>ร้านค้าทีวีดาวเทียม</a> / แก้ไข";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}



### เรียงลำดับใหม่ ###

$act	=	$_GET["action"];
$id		=	$_GET["id"];
$t		=	$_GET["t"];
$p		=	$_GET["p"];


	$SQL		="SELECT * FROM $DB_DEALER WHERE ID='$id'";
	$result		=mysql_query($SQL);
		while ($row		=	mysql_fetch_array($result)){	
			$DEALER_SORT1	=	$row["DEALER_SORT"];

			$DEALER_PROVINCE	=	$row["DEALER_PROVINCE"];
		}



if ($act =="down") {
	
	$SQL		="SELECT * FROM $DB_DEALER WHERE DEALER_SORT < '$DEALER_SORT1' AND DEALER_PROVINCE='$DEALER_PROVINCE' ORDER BY DEALER_SORT DESC LIMIT 0,1";

	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);
	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$DEALER_SORT2		=	$row["DEALER_SORT"];
		}

		$SQL		="UPDATE $DB_DEALER SET DEALER_SORT='$DEALER_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_DEALER SET DEALER_SORT='$DEALER_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}
}

if ($act =="up") {

	$SQL		="SELECT * FROM $DB_DEALER WHERE DEALER_SORT > '$DEALER_SORT1' AND DEALER_PROVINCE='$DEALER_PROVINCE' ORDER BY DEALER_SORT LIMIT 0,1";
	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);

	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$DEALER_SORT2		=	$row["DEALER_SORT"];
		}

		$SQL		="UPDATE $DB_DEALER SET DEALER_SORT='$DEALER_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_DEALER SET DEALER_SORT='$DEALER_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}

}


header("Location: index.php?p=$p&t=$DEALER_PROVINCE");
ob_end_flush();
mysql_close();
exit;
?>