<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../_modules/config.php");
include ("../_modules/other/sub.php");
include ("../_modules/mysql/mysql.php");
include ("../_modules/cache/cache-kit.php");
include ("../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../_modules/session/session.php");


if ($U_STATUS =="") {redirect(BACKEND_URL.'/login.php');exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect(BACKEND_URL.'/logout.php');exit;}

include ("menu.php");

$tp				=	new Template("_tp_main.html");

$tp_index		=	new Template("_tp_login_complete.html");


$CONTENT_HTML	=	$tp_index->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>