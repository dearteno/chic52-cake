<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="menu2";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="Menu 2";
$MODULE_DETAIL		="Menu 2";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,";

?>