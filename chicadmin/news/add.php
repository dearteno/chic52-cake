<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="news";



$TITLE_TOPIC	="<a href='index.php'>ข่าวสาร</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_add.html");


$action		=	$_POST["action"];

$t			=	$_GET["t"];
if($t==""){	$t		=	$_POST["t"];}
if($t!=1 AND $t!=2 AND $t!=3){	exit;}

$news_date	=	date("d-m-Y");

if ($action =="add") {




		### SEO ###
		$seo_url			=	generate_seo_link($_POST["seo_url"], '-', false,'');
		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$news_date			=	coverttime_y_m_d($_POST["news_date"]);
		$file				=	$_FILES['seo_image']['name'];
		$typefile			=	$_FILES['seo_image']['type'];	
		$sizefile			=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}



		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$seofile	=	"$ran$typecheck1";

		}
		### SEO ###



		$news_name			=	$_POST["news_name"];
		$news_intro			=	$_POST["news_intro"];
		$news_content		=	$_POST["news_content"];
		$news_status		=	$_POST["news_status"];
		$news_stick			=	$_POST["news_stick"];

		if ($news_status =="Y") {$news_status ="S";}else{$news_status ="H";}


		$file				=	$_FILES['news_cover1']['name'];
		$typefile			=	$_FILES['news_cover1']['type'];	
		$sizefile			=	$_FILES['news_cover1']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['news_cover1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['news_cover1']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['news_cover1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$w2 = 330 ;
					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = new Thumbnail($pathfull);
					$thumb->resize($w2,$h2);
					$thumb->crop(0,0,330,230);
					$thumb->save($pathfull);

					list($w1, $h1) = getimagesize($paththumb);
					$h2 = 200 ;
					$percent = $h2/$h1;
					$w2 = $w1 * $percent;	

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$coverfile1	=	"$ran$typecheck1";

		}



		$file				=	$_FILES['news_cover2']['name'];
		$typefile			=	$_FILES['news_cover2']['type'];	
		$sizefile			=	$_FILES['news_cover2']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['news_cover2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['news_cover2']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['news_cover2']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$w2 = 960 ;
					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = new Thumbnail($pathfull);
					$thumb->resize($w2,$h2);
					$thumb->crop(0,0,960,340);
					$thumb->save($pathfull);

					list($w1, $h1) = getimagesize($paththumb);
					$h2 = 311 ;
					$percent = $h2/$h1;
					$w2 = $w1 * $percent;	

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->cropFromCenter(311);
					$thumb->crop(0,0,311,214);
					$thumb->save($paththumb);

					$coverfile2	=	"$ran$typecheck1";

		}



		$SQL			=	"SELECT * FROM $DB_NEWS ORDER BY NEWS_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$news_sort	=	$row["NEWS_SORT"];
			}
		$news_sort		= $news_sort+1;

		if ($news_stick =="Y") {
				$news_stick		=0;
				$SQL			=	"SELECT * FROM $DB_NEWS WHERE NEWS_STICK >='100000' ORDER BY NEWS_STICK DESC LIMIT 0,1;";	
				$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$news_stick	=	$row["NEWS_STICK"];
				}
				if ($news_stick ==0) {
					$news_stick=100000;
				}
			$news_stick		= $news_stick+1;		    
		}else{
			$news_stick		=0;		
		}




	$SQL			=	"INSERT INTO $DB_NEWS VALUES('', '$t','$news_name', '$news_intro', '$coverfile1', '$coverfile2', $SQL_IMAGE_INSERT '$news_content', '$SQL_IMAGE', '$SQL_CLIP', '$news_stick','$news_sort','$news_status','$news_date','$news_date','$seo_url','$seo_title', '$seo_description', '$seo_keyword', '$seofile','$U_USERNAME',NOW(),'$U_USERNAME',NOW());";	
	$result			=	mysql_query($SQL);



		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}


if($t ==1){
	$t_name ="ข่าวประชาสัมพันธ์";
}
if($t==2){
	$t_name	="GMM Z Family";
}
if($t==3){
	$t_name	="ประกาศจาก กสทช.";
}


		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>