<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="news";


$TITLE_TOPIC	="ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_NEWS WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$news_cover1		=	$row["NEWS_COVER1"];
				$news_cover2		=	$row["NEWS_COVER2"];
				$seo_image		=	$row["SEO_IMAGE"];
				$news_image		=	$row["NEWS_IMAGE"];
				$news_image1		=	$row["NEWS_IMAGE1"];
				$news_image2		=	$row["NEWS_IMAGE2"];
				$news_image3		=	$row["NEWS_IMAGE3"];
				$news_image4		=	$row["NEWS_IMAGE4"];
			}


					if($news_cover1 !="" AND $news_cover1 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$news_cover1");
						deletedata("../../_files/images/source/$news_cover1");
						deletedata("../../_files/images/thumb/$news_cover1");
					}
					if($news_cover2 !="" AND $news_cover2 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$news_cover2");
						deletedata("../../_files/images/source/$news_cover2");
						deletedata("../../_files/images/thumb/$news_cover2");
					}
					if($seo_image !="" AND $seo_image !="blank_user.jpg") {
						deletedata("../../_files/images/full/$seo_image");
						deletedata("../../_files/images/source/$seo_image");
						deletedata("../../_files/images/thumb/$seo_image");
					}

$SQL			=	"DELETE FROM $DB_NEWS WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>