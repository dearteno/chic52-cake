<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="news";


$TITLE_TOPIC	="<a href='index.php'>ข่าวสาร</a>&nbsp;/&nbsp;รายละเอียด";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_detail	=	new Template("_tp_detail.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}




$WRITE_LINK_HTML ="edit.php?id=$id";

		$SQL			=	"SELECT * FROM $DB_NEWS WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$news_name		=	$row["NEWS_NAME"];
				$t				=	$row["NEWS_TYPE"];
				$news_cover1		=	$row["NEWS_COVER1"];
				$news_cover2		=	$row["NEWS_COVER2"];
				$news_stick		=	$row["NEWS_STICK"];
				$news_time_start	=	$row["NEWS_TIME_START"];
				$news_time_end	=	$row["NEWS_TIME_END"];
				$news_artist		=	$row["NEWS_ARTIST"];
				$news_content		=	$row["NEWS_CONTENT"];
				$news_image		=	$row["NEWS_IMAGE"];
				$news_youtube		=	$row["NEWS_YOUTUBE"];
				$seo_url			=	$row["SEO_URL"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];
				$news_homecover	=	$row["NEWS_COVERLINEUP"];
				$news_status		=	$row["NEWS_STATUS"];
				$news_date			=	coverttime_y_m_d($row["NEWS_DATE"]);
				$news_image1		=	$row["NEWS_IMAGE1"];
				$news_image_text1	=	$row["NEWS_IMAGE_TEXT1"];
				$news_image2		=	$row["NEWS_IMAGE2"];
				$news_image_text2	=	$row["NEWS_IMAGE_TEXT2"];
				$news_image3		=	$row["NEWS_IMAGE3"];
				$news_image_text3	=	$row["NEWS_IMAGE_TEXT3"];
				$news_image4		=	$row["NEWS_IMAGE4"];
				$news_image_text4	=	$row["NEWS_IMAGE_TEXT4"];


			}

		if($news_image1 !=""){
			$news_imagecontent1_html ="<br/><img src=\"../../_files/images/thumb/$news_image1\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"news_imagecontent1_temp\" id=\"news_imagecontent1_temp\" value=\"$news_image1\" />";
		}
		if($news_image2 !=""){
			$news_imagecontent2_html ="<br/><img src=\"../../_files/images/thumb/$news_image2\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"news_imagecontent2_temp\" id=\"news_imagecontent2_temp\" value=\"$news_image2\" />";
		}
		if($news_image3 !=""){
			$news_imagecontent3_html ="<br/><img src=\"../../_files/images/thumb/$news_image3\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"news_imagecontent3_temp\" id=\"news_imagecontent3_temp\" value=\"$news_image3\" />";
		}
		if($news_image4 !=""){
			$news_imagecontent4_html ="<br/><img src=\"../../_files/images/thumb/$news_image4\" style=\"padding-top:5px; padding-bottom:5px;\" /><input type=\"hidden\" name=\"news_imagecontent4_temp\" id=\"news_imagecontent4_temp\" value=\"$news_image4\" />";
		}	


if ($news_status=="S") {
    $status ='<span class="button submit medium green-back ui-corner-all" style="width:60px;text-align:center;font-size:12px;">แสดงผล</span>';
} 	 	 	 
if ($news_status=="H") {
    $status ='<span class="button submit medium grey-back ui-corner-all" style="width:60px;text-align:center;font-size:12px;">ซ่อน</span>';
} 	 	 	 
if ($news_stick >100000) {
    $stick ='<span class="button submit medium green-back ui-corner-all" style="width:80px;text-align:center;font-size:12px;">ข่าวปักหมุด</span>';    
}else{
    $stick ='<span class="button submit medium grey-back ui-corner-all" style="width:80px;text-align:center;font-size:12px;">ไม่ปักหมุด</span>';    
}




if($t ==1){
	$t_name ="ข่าวประชาสัมพันธ์";
}
if($t==2){
	$t_name	="GMM Z Family";
}
if($t==3){
	$t_name	="ประกาศจาก กสทช.";
}


$CONTENT_HTML	=	$tp_detail->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>