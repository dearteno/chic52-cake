<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="news";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="ข่าว";
$MODULE_DETAIL		="ข่าว";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,";


$MODULE_SUB[]		="ข่าวประชาสัมพันธ์|news|index.php?t=1";
$MODULE_SUB[]		="GMM Z Family|news|index.php?t=2";
$MODULE_SUB[]		="ประกาศจาก กสทช.|news|index.php?t=3";

?>