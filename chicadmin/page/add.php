<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="page";



$TITLE_TOPIC	="<a href='index.php'>Page</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_add.html");


$action		=	$_POST["action"];



$page_date	=	date("d-m-Y");

if ($action =="add") {






		for($i=1;$i<=3;$i++){

				if($i==1){
					$FULL_SIZE_W	='1229';
					$FULL_SIZE_H	='460';
					$THUMB_SIZE_W	='200';
				}
				if($i==2){
					$FULL_SIZE_W	='81';
					$FULL_SIZE_H	='32';
					$THUMB_SIZE_W	='81';
				}
				if($i==3){
					$FULL_SIZE_W	='213';
					$FULL_SIZE_H	='100';
					$THUMB_SIZE_W	='180';
				}				

				$file				=	$_FILES["page_pic$i"]['name'];
				$typefile			=	$_FILES["page_pic$i"]['type'];	
				$sizefile			=	$_FILES["page_pic$i"]['size'];
				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
							



							$ran		=random_string(8);
							$paththumb	="../../_files/images/source/$ran$typecheck1";
							copy($_FILES["page_pic$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/images/full/$ran$typecheck1";
							copy($_FILES["page_pic$i"]['tmp_name'],$pathfull);
							chmod("$pathfull", 0777); 

							$paththumb	="../../_files/images/thumb/$ran$typecheck1";
							copy($_FILES["page_pic$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 


							$thumb = PhpThumbFactory::create($pathfull);
							$thumb->resize($FULL_SIZE_W,$FULL_SIZE_H);
							$thumb->crop(0,0,$FULL_SIZE_W,$FULL_SIZE_H);
							$thumb->save($pathfull);

							list($w1, $h1) = getimagesize($paththumb);
							$w2 = $THUMB_SIZE_W ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;


							$thumb = PhpThumbFactory::create($paththumb);
							$thumb->resize($w2,$h2);
							#$thumb->crop(0,0,$FULL_SIZE_W,$FULL_SIZE_H);
							$thumb->save($paththumb);

							$PAGE_PIC_TEMP	=	"PIC$i";
							$$PAGE_PIC_TEMP	=	"$ran$typecheck1";


				}

		}


		$page_topic			=	$_POST["page_topic"];
		$page_intro_th		=	$_POST["page_intro_th"];
		$page_content_th	=	$_POST["page_content_th"];

		$page_intro_en		=	$_POST["page_intro_en"];
		$page_content_en	=	$_POST["page_content_en"];
		$page_status		=	$_POST["page_status"];

		if($page_status =="Y"){
			$page_status ="S";
		}else{
			$page_status ="H";		
		}

		$SQL			=	"SELECT * FROM $DB_PAGE ORDER BY PAGE_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$page_sort	=	$row["PAGE_SORT"];
			}
		$page_sort		= $page_sort+1;



	$SQL			=	"INSERT INTO $DB_PAGE VALUES('', '$page_topic', '$PIC1','$PIC2','$PIC3',
	'$page_intro_th','$page_intro_en','$page_content_th','$page_content_en','$page_status','$page_sort','$U_USERNAME',NOW(),'$U_USERNAME',NOW());";	
	$result			=	mysql_query($SQL);


		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>