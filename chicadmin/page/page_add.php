<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="page";



$TITLE_TOPIC	="<a href='index.php'>หน้าอื่นๆ</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_page_add	=	new Template("_tp_page_add.html");


$action		=	$_POST["action"];


if ($action =="add") {

		$page_name			=	$_POST["page_name"];
		$page_content		=	$_POST["page_content"];
		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];


		$file			=	$_FILES['seo_image']['name'];
		$typefile		=	$_FILES['seo_image']['type'];	
		$sizefile		=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$seo_image_temp");
						deletedata("../../_files/images/source/$seo_image_temp");
						deletedata("../../_files/images/thumb/$seo_image_temp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 120 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 120 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$picfile	=	"$ran$typecheck1";


		}


	$SQL			=	"INSERT INTO $DB_PAGE VALUES('', '$seo_title', '$seo_description', '$seo_keyword', '$picfile', '$page_name', '$page_content');";	
	$result			=	mysql_query($SQL);



		$tp_page_add->Block("STAFF_SUCCESS");
		$tp_page_add->Apply();

$CONTENT_HTML	=	$tp_page_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_page_add->Block("STAFF_INFO");
		$tp_page_add->Apply();


}



		$tp_page_add->Block("STAFF_FORM");
		$tp_page_add->Apply();


$CONTENT_HTML	=	$tp_page_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>

