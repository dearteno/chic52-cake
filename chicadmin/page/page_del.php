<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="page";


$TITLE_TOPIC	="<a href='index.php'>หน้าอื่นๆ</a>&nbsp;/&nbsp;ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_PAGE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$page_name			=	$row["PAGE_NAME"];
				$page_content		=	$row["PAGE_CONTENT"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];
			}

					if($seo_image !="" AND $seo_image !="blank_user.jpg") {
						deletedata("../../_files/images/full/$seo_image");
						deletedata("../../_files/images/source/$seo_image");
						deletedata("../../_files/images/thumb/$seo_image");
					}

$SQL			=	"DELETE FROM $DB_PAGE WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_page		=	new Template("_tp_page_del.html");




$CONTENT_HTML	=	$tp_page->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>