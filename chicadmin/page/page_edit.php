<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="page";


$TITLE_TOPIC	="<a href='index.php'>หน้าอื่นๆ</a>&nbsp;/&nbsp;แก้ไข";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_page_edit	=	new Template("_tp_page_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {

		$page_name			=	$_POST["page_name"];
		$page_content		=	$_POST["page_content"];
		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$seo_image_temp		=	$_POST["seo_image_temp"];


		$file			=	$_FILES['seo_image']['name'];
		$typefile		=	$_FILES['seo_image']['type'];	
		$sizefile		=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$seo_image_temp");
						deletedata("../../_files/images/source/$seo_image_temp");
						deletedata("../../_files/images/thumb/$seo_image_temp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 120 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 120 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$picfile	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_PAGE SET SEO_IMAGE ='$picfile' WHERE ID='$id'";	
					$result			=	mysql_query($SQL);

		}



	$SQL			=	"UPDATE $DB_PAGE SET SEO_TITLE='$seo_title',SEO_DESCRIPTION='$seo_description',SEO_KEYWORD='$seo_keyword',PAGE_NAME='$page_name',PAGE_CONTENT='$page_content' WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



		$tp_page_edit->Block("STAFF_SUCCESS");
		$tp_page_edit->Apply();

$CONTENT_HTML	=	$tp_page_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_page_edit->Block("STAFF_INFO");
		$tp_page_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_PAGE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$page_name			=	$row["PAGE_NAME"];
				$page_content		=	$row["PAGE_CONTENT"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];
			}
 	 	 	 




		$tp_page_edit->Block("STAFF_FORM");
		$tp_page_edit->Apply();


$CONTENT_HTML	=	$tp_page_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>

