<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: phoinikasdigital.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="product";



include ("../menu.php");

if ($U_STATUS =="") {redirect("/ableadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("/ableadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("/ableadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_catalog_sub_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {


		$catalog_sub_name		=	$_POST["catalog_sub_name"];
		$catalog_code			=	$_POST["catalog_code"];

		$SQL			=	"UPDATE $DB_PRODUCT_CATALOG_SUB SET CATALOG_CODE='$catalog_code',CATALOG_SUB_NAME='$catalog_sub_name' WHERE ID='$id';";	
		$result			=	mysql_query($SQL);


		$SQL			=	"SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID						=	$row["ID"];
				$catalog_sub_name	=	$row["CATALOG_SUB_NAME"];
				$cid					=	$row["CATALOG_ID"];
			}


		$SQL			=	"SELECT * FROM $DB_PRODUCT_CATALOG WHERE ID='$cid';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("catalog.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$category_name			=	$row["TOPIC_NAME"];
			}


$page_nav		="product";
$page_sub_nav	="product_list";
$TITLE_TOPIC	="Product / <a href='catalog.php'>Category</a> / <a href='catalog_sub.php?cid=$cid'>$category_name</a> / แก้ไขหัวข้อ";



		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID						=	$row["ID"];
				$catalog_sub_name		=	$row["CATALOG_SUB_NAME"];
				$cid					=	$row["CATALOG_ID"];
				$catalog_code			=	$row["CATALOG_CODE"];

			}


		$SQL			=	"SELECT * FROM $DB_PRODUCT_CATALOG WHERE ID='$cid';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("catalog.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$category_name			=	$row["TOPIC_NAME"];
			}


$page_nav		="product";
$page_sub_nav	="product_list";
$TITLE_TOPIC	="Product / <a href='catalog.php'>Category</a> / <a href='catalog_sub.php?cid=$cid'>$category_name</a> / แก้ไขหัวข้อ";



		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>