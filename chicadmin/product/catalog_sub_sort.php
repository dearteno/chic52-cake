<?php
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: phoinikasdigital.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="product";
$page_sub_nav	="product_list";
$TITLE_TOPIC	="Category";


include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("/ableadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("/ableadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("/ableadmin/logout.php");exit;}



### เรียงลำดับใหม่ ###

$act	=	$_GET["action"];
$id		=	$_GET["id"];


	$SQL		="SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE ID='$id'";
	$result		=mysql_query($SQL);
		while ($row		=	mysql_fetch_array($result)){	
			$CATALOG_ID		=	$row["CATALOG_ID"];
			$CATALOG_SUB_SORT1	=	$row["CATALOG_SUB_SORT"];
		}


if ($act =="down") {
	
	$SQL		="SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE CATALOG_ID='$CATALOG_ID' AND CATALOG_SUB_SORT < '$CATALOG_SUB_SORT1' ORDER BY CATALOG_SUB_SORT DESC LIMIT 0,1";

	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);
	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$CATALOG_SUB_SORT2		=	$row["CATALOG_SUB_SORT"];
		}

		$SQL		="UPDATE $DB_PRODUCT_CATALOG_SUB SET CATALOG_SUB_SORT='$CATALOG_SUB_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_PRODUCT_CATALOG_SUB SET CATALOG_SUB_SORT='$CATALOG_SUB_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}
}

if ($act =="up") {

	$SQL		="SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE CATALOG_ID='$CATALOG_ID' AND CATALOG_SUB_SORT > '$CATALOG_SUB_SORT1' ORDER BY CATALOG_SUB_SORT LIMIT 0,1";
	$result		=mysql_query($SQL);
	$count		=mysql_num_rows($result);

	if ($count !=0) {

		while ($row		=	mysql_fetch_array($result)){	
			$id2			=	$row["ID"];
			$CATALOG_SUB_SORT2		=	$row["CATALOG_SUB_SORT"];
		}

		$SQL		="UPDATE $DB_PRODUCT_CATALOG_SUB SET CATALOG_SUB_SORT='$CATALOG_SUB_SORT1' WHERE ID='$id2'";
		$result		=mysql_query($SQL);

		$SQL		="UPDATE $DB_PRODUCT_CATALOG_SUB SET CATALOG_SUB_SORT='$CATALOG_SUB_SORT2' WHERE ID='$id'";
		$result		=mysql_query($SQL);
	}

}

/*
	$BANNER_HTML		=acmeCache::fetch('BANNER_HTML', 1);
	if(!$BANNER_HTML){

			$tp_sponsor		=	new Template(BASEDIR."/_tp_sponsor.html");
			$tp_sponsor->Block("BANNER_LIST");
			$tp_sponsor->Sub(1);

			$SQL			=	"SELECT * FROM $DB_PRODUCT_CATALOG_SUB  ORDER BY CATALOG_SUB_SORT DESC;";
			$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$name		=	$row["BANNER_NAME"];
					$pic 		=	BASEURL_UPLOAD."/images/full/".$row["BANNER_PIC"];
					$link 		=	$row["BANNER_LINK"];
					$tp_sponsor->Apply();
				}

		$BANNER_HTML	=	$tp_sponsor->Generate();

		acmeCache::save('BANNER_HTML', $BANNER_HTML);

	}
*/

header("Location: catalog_sub.php?cid=$CATALOG_ID");
ob_end_flush();
mysql_close();
exit;
?>