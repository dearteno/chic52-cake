<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: phoinikasdigital.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");


// Turn off all error reporting
error_reporting(0);

$page_nav		="product";


$TITLE_TOPIC	="<a href='index.php'>Product</a>&nbsp;/&nbsp;ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$t				=	$_GET['t'];
		$c				=	$_GET['c'];
		$id				=	$_GET['id'];
		$p				=	$_GET['p'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$PRODUCT_PATH		=	$row["PRODUCT_PATH"];
				$PRODUCT_PIC		=	$row["PRODUCT_PIC"];
				$SEO_IMAGE			=	$row["SEO_IMAGE"];
			}
 	

					if($PRODUCT_PIC !="" AND $PRODUCT_PIC !="blank_user.jpg") {
						deletedata("../../_files/product/full/$PRODUCT_PATH/$PRODUCT_PIC");
						deletedata("../../_files/product/source/$PRODUCT_PATH/$PRODUCT_PIC");
						deletedata("../../_files/product/thumb/$PRODUCT_PATH/$PRODUCT_PIC");
					}


					if($SEO_IMAGE !="" AND $SEO_IMAGE !="blank_user.jpg") {
						deletedata("../../_files/product/seo/$PRODUCT_PATH/$SEO_IMAGE");
					}

			$SQL			=	"DELETE FROM $DB_PRODUCT WHERE ID='$id'";	
			$result			=	mysql_query($SQL);

			$SQL			=	"DELETE FROM $DB_PRODUCT_STYLE_GROUP WHERE PRODUCT_ID = '$id';";	
			$result			=	mysql_query($SQL);

			$SQL			=	"DELETE FROM $DB_PRODUCT_CATALOG_GROUP WHERE PRODUCT_ID = '$id';";	
			$result			=	mysql_query($SQL);


$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>