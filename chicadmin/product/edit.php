<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="product";



$TITLE_TOPIC	="<a href='index.php'>Product</a>&nbsp;/&nbsp;แก้ไข";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];

$id			=	$_GET["id"];
if($id==""){	$id		=	$_POST["id"];}

$c			=	$_GET["c"];
if($c==""){	$c		=	$_POST["c"];}

$news_date	=	date("d-m-Y");

if ($action =="edit") {


		$styleid			=	$_POST["styleid"];
		$catid				=	$_POST["catid"];


		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);

			while ($row		=	mysql_fetch_array($result)){	
				$content_folder		=	$row["PRODUCT_PATH"];
			}


		### SEO ###
		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$seo_img_temp		=	$_POST["seo_img_temp"];
		$file				=	$_FILES['seo_image']['name'];
		$typefile			=	$_FILES['seo_image']['type'];	
		$sizefile			=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}



		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					

					if($seo_img_temp !="" AND $seo_img_temp !="blank_user.jpg") {
						deletedata("../../_files/product/seo/$content_folder/$seo_img_temp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/product/seo/$content_folder/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 


					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$seofile	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_PRODUCT SET SEO_IMAGE ='$seofile' WHERE ID='$id'";	
					$result			=	mysql_query($SQL);

		}
		### SEO ###




		$pic_temp				=	$_POST["pic_temp"];
		$product_category		=	$_POST["product_category"];
		$product_style			=	$_POST["product_style"];
		$product_name			=	$_POST["product_name"];
		$product_description	=	$_POST["product_description"];
		$chic_code				=	$_POST["chic_code"];
		$product_barcode		=	$_POST["product_barcode"];
		$product_price			=	$_POST["product_price"];
		$product_status			=	$_POST["product_status"];


		$product_material		=	$_POST["product_material"];
		$product_color			=	$_POST["product_color"];
		$product_w				=	$_POST["product_w"];
		$product_d				=	$_POST["product_d"];
		$product_h				=	$_POST["product_h"];
		$product_remark			=	$_POST["product_remark"];

		list($product_category,$product_category_sub)					=	preg_split("/\|/",$product_category);




		for($i=1;$i<=5;$i++){

				$W_PIC1="470";
				$H_PIC1="300";

				$W_PIC_THUMB1="180";
				$H_PIC_THUMB1="120";

				$W_PIC2="300";
				$H_PIC2="470";

				$W_PIC_THUMB2="120";
				$H_PIC_THUMB2="180";



				$file				=	$_FILES["product_cover$i"]['name'];
				$typefile			=	$_FILES["product_cover$i"]['type'];	
				$sizefile			=	$_FILES["product_cover$i"]['size'];
				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}



				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
						
							if($i==1 AND $pic_temp !=""){
									deletedata("../../_files/product/full/$content_folder/$pic_temp");
									deletedata("../../_files/product/source/$content_folder/$pic_temp");
									deletedata("../../_files/product/thumb/$content_folder/$pic_temp");					
							}							


							$ran		=random_string(8);
							$paththumb	="../../_files/product/source/$content_folder/$ran$typecheck1";
							copy($_FILES["product_cover$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/product/full/$content_folder/$ran$typecheck1";
							copy($_FILES["product_cover$i"]['tmp_name'],$pathfull);
							chmod("$paththumb", 0777); 
							$paththumb	="../../_files/product/thumb/$content_folder/$ran$typecheck1";
							copy($_FILES["product_cover$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 

							list($w1, $h1) = getimagesize($pathfull);

							if($w1 > $h1){
	
								$w2 = $W_PIC1 ;
								$percent = $w2/$w1;
								$h2 = $h1 * $percent;

								$thumb = PhpThumbFactory::create($pathfull);
								$thumb->resize($w2,$h2);
								#$thumb->crop(0,0,$W_PIC1,$H_PIC1);
								$thumb->save($pathfull);								

								$w2 = $W_PIC_THUMB1 ;
								$percent = $w2/$w1;
								$h2 = $h1 * $percent;

								$thumb = PhpThumbFactory::create($paththumb);
								$thumb->resize($w2,$h2);
								#$thumb->crop(0,0,$W_PIC_THUMB1,$H_PIC_THUMB1);
								$thumb->save($paththumb);

							}else{

					
								$h2 = $H_PIC2 ;
								$percent = $h2/$h1;
								$w2 = $w1 * $percent;

								$thumb = PhpThumbFactory::create($pathfull);
								$thumb->resize($w2,$h2);
								#$thumb->crop(0,0,$W_PIC2,$H_PIC2);
								$thumb->save($pathfull);								

								$h2 = $H_PIC_THUMB2 ;
								$percent = $h2/$h1;
								$w2 = $w1 * $percent;

								$thumb = PhpThumbFactory::create($paththumb);
								$thumb->resize($w2,$h2);
								#$thumb->crop(0,0,$W_PIC_THUMB2,$H_PIC_THUMB2);
								$thumb->save($paththumb);							
							
							}

								$name		=	"product_cover$i";
								$$name		=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_PRODUCT SET PRODUCT_PIC ='$product_cover1' WHERE ID='$id'";	
					$result			=	mysql_query($SQL);

				}



		}


	$SQL			=	"UPDATE $DB_PRODUCT SET PRODUCT_CATEGORY='',PRODUCT_CATEGORY_SUB='',PRODUCT_STYLE='$product_style',
	PRODUCT_NAME='$product_name',PRODUCT_DESCRIPTION='$product_description',PRODUCT_BARCODE='$product_barcode',PRODUCT_MATERIAL='$product_material',PRODUCT_COLOR='$product_color',PRODUCT_W='$product_w',PRODUCT_D='$product_d',PRODUCT_H='$product_h',PRODUCT_REMARK='$product_remark',PRODUCT_PRICE='$product_price',PRODUCT_CHIC_CODE='$chic_code',PRODUCT_STATUS='$product_status',SEO_TITLE='$seo_title',SEO_DESCRIPTION='$seo_description',SEO_KEYWORD='$seo_keyword',TIME_UPDATE=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);



							$SQL			=	"DELETE FROM $DB_PRODUCT_STYLE_GROUP WHERE PRODUCT_ID = '$id';";	
							$result			=	mysql_query($SQL);

							for($i=0;$i<=count($styleid)-1;$i++){
								$temp_style	=	trim($styleid[$i]);
								if($temp_style!=""){
									$SQL			=	"INSERT INTO $DB_PRODUCT_STYLE_GROUP VALUES('', '$id', '$temp_style');";	
									$result			=	mysql_query($SQL);
								}
							}


							$SQL			=	"DELETE FROM $DB_PRODUCT_CATALOG_GROUP WHERE PRODUCT_ID = '$id';";	
							$result			=	mysql_query($SQL);

							for($i=0;$i<=count($catid)-1;$i++){
								$temp_cat	=	trim($catid[$i]);

								list($product_category,$product_category_sub)					=	preg_split("/\|/",$temp_cat);

								if($product_category !="" AND $product_category_sub !=""){
									$SQL			=	"INSERT INTO $DB_PRODUCT_CATALOG_GROUP VALUES('', '$id', '$product_category','$product_category_sub');";	
									$result			=	mysql_query($SQL);
								}
							}


### UPDATE CAT ###

				$SQL2		=	"SELECT * FROM $DB_PRODUCT_STYLE_GROUP WHERE PRODUCT_ID='$id';";	
				$result2	=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$STYLE_ID				=	$row2["STYLE_ID"];
					$STYLE_ID_ADD	.="$STYLE_ID,";
				}
 				
				if($STYLE_ID_ADD !=''){
					$STYLE_ID_ADD =",$STYLE_ID_ADD";
					$SQL2			=	"UPDATE $DB_PRODUCT SET PRODUCT_STYLE='$STYLE_ID_ADD' WHERE ID='$id';";	
					$result2		=	mysql_query($SQL2);

				}

				$CAT_ID_ADD ='';
				$SQL2		=	"SELECT * FROM $DB_PRODUCT_CATALOG_GROUP WHERE PRODUCT_ID='$id';";	
				$result2	=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$CATALOG_ID				=	$row2["CATALOG_ID"];
					$CATALOG_SUB				=	$row2["CATALOG_SUB"];
					$CAT_ID_ADD	.="$CATALOG_ID|$CATALOG_SUB,";
				}

	
				if($CAT_ID_ADD !=''){
					$CAT_ID_ADD =",$CAT_ID_ADD";
					$SQL2			=	"UPDATE $DB_PRODUCT SET PRODUCT_CATEGORY='$CAT_ID_ADD' WHERE ID='$id';";	
					$result2		=	mysql_query($SQL2);
				}


### UPDATE CAT ###



		$c		=	$product_category;
		$t		=	$product_style;

		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}




		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE ID='$id';";	
		$result			=	mysql_query($SQL);

			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$PRODUCT_PATH		=	$row["PRODUCT_PATH"];
				$PRODUCT_PIC		=	$row["PRODUCT_PIC"];
				$PRODUCT_CATEGORY		=	$row["PRODUCT_CATEGORY"];
				$PRODUCT_CATEGORY_SUB	=	$row["PRODUCT_CATEGORY_SUB"];
				$PRODUCT_STYLE		=	$row["PRODUCT_STYLE"];
				$product_name		=	$row["PRODUCT_NAME"];
				$product_description		=	$row["PRODUCT_DESCRIPTION"];
				$product_barcode			=	$row["PRODUCT_BARCODE"];
				$product_material			=	$row["PRODUCT_MATERIAL"];
				$product_color			=	$row["PRODUCT_COLOR"];
				$product_w		=	$row["PRODUCT_W"];
				$product_d		=	$row["PRODUCT_D"];
				$product_h		=	$row["PRODUCT_H"];
				$product_remark		=	$row["PRODUCT_REMARK"];
				$product_price		=	$row["PRODUCT_PRICE"];
				$chic_code			=	$row["PRODUCT_CHIC_CODE"];
				$product_status		=	$row["PRODUCT_STATUS"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$SEO_IMAGE			=	$row["SEO_IMAGE"];

			}


			if($product_status=="S"){				$product_status_s ="SELECTED";			}
			if($product_status=="N"){				$product_status_n ="SELECTED";			}
			if($product_status=="O"){				$product_status_o ="SELECTED";			}
			if($product_status=="H"){				$product_status_h ="SELECTED";			}
			
			if($PRODUCT_PIC !=""){
				$PRODUCT_PIC_HTML ="<img src='../../_files/product/full/$PRODUCT_PATH/$PRODUCT_PIC'>";
			}
			if($SEO_IMAGE !=""){
				$SEO_IMAGE_HTML ="<img src='../../_files/product/seo/$PRODUCT_PATH/$SEO_IMAGE'>";
			}


			$SQL		=	"SELECT * FROM $DB_PRODUCT_STYLE_GROUP WHERE PRODUCT_ID='$id';";	
			$result		=	mysql_query($SQL);
			$STYLE_CHECK =",";
				while ($row		=	mysql_fetch_array($result)){	
					$STYLE_TEMP		=	$row["STYLE_ID"];
					$STYLE_CHECK		.="$STYLE_TEMP,";
				}

			$SQL		=	"SELECT * FROM $DB_PRODUCT_CATALOG_GROUP WHERE PRODUCT_ID='$id';";	
			$result		=	mysql_query($SQL);
			$CATALOG_CHECK =",";
				while ($row		=	mysql_fetch_array($result)){	
					$CATALOG_TEMP		=	$row["CATALOG_ID"];
					$CATALOG_SUB_TEMP	=	$row["CATALOG_SUB"];
					$CATALOG_CHECK		.="$CATALOG_TEMP|$CATALOG_SUB_TEMP,";
				}

					#$PRODUCT_STYLE_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PRODUCT_STYLE ORDER BY TOPIC_SORT DESC;";	
				$result2			=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["ID"];
					$TOPIC_NAME			=	trim($row2["TOPIC_NAME"]);

					if (preg_match("/,$PID,/i", $STYLE_CHECK)) {
						$SELECT="CHECKED";
					}else{
						$SELECT="";					
					}

					$PRODUCT_STYLE_HTML .="<input name=\"styleid[]\" type=\"checkbox\" id=\"style[]\" value=\"$PID\" $SELECT/> - $TOPIC_NAME<br/>\n";
				}


					#$PRODUCT_CATEGORY_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PRODUCT_CATALOG ORDER BY TOPIC_SORT DESC;";	
				$result2			=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$CID					=	$row2["ID"];
					$TOPIC_NAME			=	trim($row2["TOPIC_NAME"]);

					$PRODUCT_CATEGORY_HTML .="$TOPIC_NAME<br/>\n";

					$SQL3			=	"SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE CATALOG_ID='$CID' ORDER BY CATALOG_SUB_SORT DESC;";	
					$result3		=	mysql_query($SQL3);

					while ($row3		=	mysql_fetch_array($result3)){	
						$SID				=	$row3["ID"];
						$catalog_sub_name	=	$row3["CATALOG_SUB_NAME"];

					if (preg_match("/,$CID\|$SID,/i", $CATALOG_CHECK)) {
						$SELECT="CHECKED";
					}else{
						$SELECT="";					
					}					

					#$PRODUCT_CATEGORY_HTML .=" <option value=\"$CID|$SID\" style=\"padding:3px;padding-left:20px;\" $SELECT> - $catalog_sub_name</option>\n";
					$PRODUCT_CATEGORY_HTML .="<input name=\"catid[]\" type=\"checkbox\" id=\"cat[]\" value=\"$CID|$SID\" $SELECT /> - $catalog_sub_name<br/>\n";
					}


				}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>