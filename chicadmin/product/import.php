<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="product";



$TITLE_TOPIC	="<a href='index.php'>Product</a>&nbsp;/&nbsp;นำเข้าข้อมูล";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-I/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_import.html");


$action		=	$_POST["action"];

$t			=	$_GET["t"];
if($t==""){	$t		=	$_POST["t"];}

$c			=	$_GET["c"];
if($c==""){	$c		=	$_POST["c"];}

$news_date	=	date("d-m-Y");

if ($action =="import") {



		$content_folder	=	date("Y-m-d");

		if (!is_dir("../../_files/product/source/$content_folder")){mkdir("../../_files/product/source/$content_folder");chmod("../../_files/product/source/$content_folder", 0777); }
		if (!is_dir("../../_files/product/full/$content_folder")){mkdir("../../_files/product/full/$content_folder");chmod("../../_files/product/full/$content_folder", 0777); }
		if (!is_dir("../../_files/product/thumb/$content_folder")){mkdir("../../_files/product/thumb/$content_folder");chmod("../../_files/product/thumb/$content_folder", 0777); }
		if (!is_dir("../../_files/product/seo/$content_folder")){mkdir("../../_files/product/seo/$content_folder");chmod("../../_files/product/seo/$content_folder", 0777); }





		$file			=	$_FILES['file_import']['name'];
		$typefile		=	$_FILES['file_import']['type'];	
		$sizefile		=	$_FILES['file_import']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".text") {$typecheck1 =".txt";}

		$ran		=random_string(8);
		$pathfile	="../../_files/product/import/$ran$typecheck1";
		copy($_FILES['file_import']['tmp_name'],$pathfile);
		chmod("$pathfile", 0777); 

		$product_category	=$_POST["product_category"];
		$product_style		=$_POST["product_style"];
		$product_status		=$_POST["product_status"];
		$dup_data			=$_POST["dup_data"];

		if($product_status =="Y"){
			$product_status ="IS";
		}else{
			$product_status ="IH";		
		}

		
		list($product_category,$product_category_sub)					=	preg_split("/\|/",$product_category);


		$fp			= fopen($pathfile,"r");	
		$textdata	= fread($fp,filesize($pathfile)); 
		fclose($fp);

		$array_data	=	preg_split("/\n/",$textdata);

		for($i=0;$i<=count($array_data)-1;$i++) {

			$array_data[$i] = trim($array_data[$i]);

			if($array_data[$i] !="") {

				#echo $array_data[$i]."<br/>\n";

				list($chic_code,$chic_barcode, $chic_name,$chic_description,$chic_material,$chic_color,$chic_w,$chic_d,$chic_h,$remark,$retail)	=	preg_split("/\t/",$array_data[$i]);

				$chic_code		=	trim($chic_code);
				$chic_barcode	=	trim($chic_barcode);
				$chic_name				=	trim($chic_name);
				$chic_description		=	trim($chic_description);
				$chic_material		=	trim($chic_material);
				$chic_color			=	trim($chic_color);
				$chic_w		=	trim($chic_w);
				$chic_d		=	trim($chic_d);
				$chic_h		=	trim($chic_h);
				$remark			=	trim($remark);
				$retail			=	trim($retail);


				$chic_description			=	preg_replace("/\"/i","",$chic_description);
				$retail						=	preg_replace("/฿|\"|,/i","",$retail)*1;

				if($chic_code !="Chic Code"){


				if($product_category =="auto"){

					### หา catalog กับ sub cat
					$product_category_add	='';
					$product_category_sub_add	='';
					
					$code_check			=	substr($chic_code,0,3);
	/*
					$SQL			=	"SELECT ID,CATALOG_ID FROM $DB_PRODUCT_CATALOG_SUB WHERE CATALOG_CODE LIKE '%,$code_check,%' ORDER BY ID LIMIT 0,1;";	
					$result			=	mysql_query($SQL);
					$count			=	mysql_num_rows($result);
					if($count ==1){
						while ($row		=	mysql_fetch_array($result)){	
							$product_category_sub_add			=	$row["ID"];
							$product_category_add				=	$row["CATALOG_ID"];
						}
					}	
*/					
				}else{
				
					$product_category_add		= $product_category;
					$product_category_sub_add	=	$product_category_sub;
				
				}
					

/*
				if(is_file("../../_files/product/import/$chic_code.jpg")== true){


					$ran		=random_string(8);
					$paththumb	="../../_files/product/seo/$content_folder/$ran.jpg";
					copy("../../_files/product/import/$chic_code.jpg",$paththumb);
					chmod("$paththumb", 0777); 


					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);


					$seofile	=	"$ran.jpg";

					$W_PIC1="470";
					$H_PIC1="300";

					$W_PIC_THUMB1="180";
					$H_PIC_THUMB1="120";

					$W_PIC2="300";
					$H_PIC2="470";

					$W_PIC_THUMB2="120";
					$H_PIC_THUMB2="180";



								$ran		=random_string(8);
								$paththumb	="../../_files/product/source/$content_folder/$ran.jpg";
								copy("../../_files/product/import/$chic_code.jpg",$paththumb);
								chmod("$paththumb", 0777); 
								$pathfull	="../../_files/product/full/$content_folder/$ran.jpg";
								copy("../../_files/product/import/$chic_code.jpg",$pathfull);
								chmod("$pathfull", 0777); 
								$paththumb	="../../_files/product/thumb/$content_folder/$ran.jpg";
								copy("../../_files/product/import/$chic_code.jpg",$paththumb);
								chmod("$paththumb", 0777); 




								list($w1, $h1) = getimagesize($pathfull);

								if($w1 > $h1){
		
									$w2 = $W_PIC1 ;
									$percent = $w2/$w1;
									$h2 = $h1 * $percent;

									$thumb = new Thumbnail($pathfull);
									$thumb->resize($w2,$h2);
									#$thumb->crop(0,0,$W_PIC1,$H_PIC1);
									$thumb->save($pathfull);								

									$w2 = $W_PIC_THUMB1 ;
									$percent = $w2/$w1;
									$h2 = $h1 * $percent;

									$thumb = new Thumbnail($paththumb);
									$thumb->resize($w2,$h2);
									#$thumb->crop(0,0,$W_PIC_THUMB1,$H_PIC_THUMB1);
									$thumb->save($paththumb);

								}else{

						
									$h2 = $H_PIC2 ;
									$percent = $h2/$h1;
									$w2 = $w1 * $percent;

									$thumb = new Thumbnail($pathfull);
									$thumb->resize($w2,$h2);
									#$thumb->crop(0,0,$W_PIC2,$H_PIC2);
									$thumb->save($pathfull);								

									$h2 = $H_PIC_THUMB2 ;
									$percent = $h2/$h1;
									$w2 = $w1 * $percent;

									$thumb = new Thumbnail($paththumb);
									$thumb->resize($w2,$h2);
									#$thumb->crop(0,0,$W_PIC_THUMB2,$H_PIC_THUMB2);
									$thumb->save($paththumb);							
								
								}

									$product_cover1		=	"$ran.jpg";


				}
*/


				$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE PRODUCT_CHIC_CODE ='$chic_code';";	
				$result			=	mysql_query($SQL);
				$count			=	mysql_num_rows($result);

				if($count==0){
							$SQL			=	"INSERT INTO $DB_PRODUCT VALUES('', '$content_folder', '$product_cover1', '$product_category_add', 
							'$product_category_sub_add','$product_style', 							
							'$chic_name', '$chic_description', '$chic_barcode', '$chic_material', 
							'$chic_color', '$chic_w', '$chic_d', '$chic_h',  '$remark', '$retail', '$chic_code',  '$product_status', 
							'$chic_name','$chic_description','$chic_description','$product_cover1',NOW(), NOW());";
						$result			=	mysql_query($SQL);

							


						$product_insert=$product_insert+1;
				}else{
						if($dup_data =="update"){
					
							$SQL			=	"UPDATE $DB_PRODUCT SET PRODUCT_CATEGORY='$product_category_add',PRODUCT_CATEGORY_SUB='$product_category_sub_add',
							PRODUCT_STYLE='$product_style',
	PRODUCT_NAME='$chic_name',PRODUCT_DESCRIPTION='$chic_description',PRODUCT_BARCODE='$chic_barcode',PRODUCT_MATERIAL='$chic_material',PRODUCT_COLOR='$chic_color',PRODUCT_W='$chic_w',
	PRODUCT_D='$chic_d',PRODUCT_H='$chic_h',PRODUCT_REMARK='$remark',PRODUCT_PRICE='$retail',PRODUCT_STATUS='$product_status',SEO_TITLE='$chic_name',
	SEO_DESCRIPTION='$chic_description',SEO_KEYWORD='$chic_description',TIME_UPDATE=NOW() WHERE PRODUCT_CHIC_CODE='$chic_code';";
							$result			=	mysql_query($SQL);	


							if($seofile !=""){
								$SQL			=	"UPDATE $DB_PRODUCT SET SEO_IMAGE ='$seofile' WHERE PRODUCT_CHIC_CODE='$chic_code';";	
								$result			=	mysql_query($SQL);																
							}

							if($product_cover1 !=""){
								$SQL			=	"UPDATE $DB_PRODUCT SET PRODUCT_PIC ='$product_cover1' WHERE PRODUCT_CHIC_CODE='$chic_code';";	
								$result			=	mysql_query($SQL);								
							}

							$product_update=$product_update+1;





						}
				
				}



				
				if($product_style !=""){
				
						$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE PRODUCT_CHIC_CODE ='$chic_code';";	
						$result			=	mysql_query($SQL);
							while ($row		=	mysql_fetch_array($result)){	
								$ID					=	$row["ID"];
							}

								$SQL			=	"DELETE FROM $DB_PRODUCT_STYLE_GROUP WHERE PRODUCT_ID = '$ID';";	
								$result			=	mysql_query($SQL);


								$SQL			=	"INSERT INTO $DB_PRODUCT_STYLE_GROUP VALUES('', '$ID', '$product_style');";	
								$result			=	mysql_query($SQL);

								$SQL			=	"UPDATE $DB_PRODUCT  SET PRODUCT_STYLE=',$product_style,' WHERE ID='$ID'";	
								$result			=	mysql_query($SQL);


				}













			#	if(is_file("../../_files/product/import/$chic_code.jpg")== true){
			#		unlink("../../_files/product/import/$chic_code.jpg");
			#	}


				$seofile			='';
				$product_cover1		='';
				}

			}

		}


	unlink($pathfile);

		$c		=	$product_category;
		$t		=	$product_style;

		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}







					$PRODUCT_STYLE_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PRODUCT_STYLE ORDER BY TOPIC_SORT DESC;";	
				$result2			=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["ID"];
					$TOPIC_NAME			=	trim($row2["TOPIC_NAME"]);
					if($t == $PID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$PRODUCT_STYLE_HTML .=" <option value=\"$PID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$TOPIC_NAME</option>\n";
				}
					$PRODUCT_STYLE_HTML .=" <option value=\"\" style=\"padding:3px;padding-left:10px;\">ไม่ระบุ</option>\n";

					$PRODUCT_CATEGORY_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
					$PRODUCT_CATEGORY_HTML .=" <option value=\"auto\" style=\"padding:3px;padding-left:10px;\">เลือกอัตโนมัติจาก Chic Code</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PRODUCT_CATALOG ORDER BY TOPIC_SORT DESC;";	
				$result2			=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$CID					=	$row2["ID"];
					$TOPIC_NAME			=	trim($row2["TOPIC_NAME"]);
					if($c == $CID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$PRODUCT_CATEGORY_HTML .=" <option value=\"$CID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$TOPIC_NAME</option>\n";


					$SQL3			=	"SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE CATALOG_ID='$CID' ORDER BY CATALOG_SUB_SORT DESC;";	
					$result3		=	mysql_query($SQL3);

					while ($row3		=	mysql_fetch_array($result3)){	
						$SID				=	$row3["ID"];
						$catalog_sub_name	=	$row3["CATALOG_SUB_NAME"];
					
					if($c== $CID AND $s == $SID){
						$SELECT="SELECTED";						
						$TITLE_TOPIC	="Product / $TOPIC_NAME - $catalog_sub_name";
					}else{
						$SELECT="";					
					}
					$PRODUCT_CATEGORY_HTML .=" <option value=\"$CID|$SID\" style=\"padding:3px;padding-left:20px;\" $SELECT> - $catalog_sub_name</option>\n";

					}

				}


					$PRODUCT_CATEGORY_HTML .=" <option value=\"\" style=\"padding:3px;padding-left:10px;\">ไม่ระบุ</option>\n";


		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>