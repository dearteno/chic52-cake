<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="product";
$page_sub_nav	="lineup_list";
$TITLE_TOPIC	="รายการต่างๆ";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

$t				=	$_GET["t"];
$c				=	$_GET["c"];
$k				=	$_GET["k"];
$s				=	$_GET["s"];
$view			=	$_GET["view"];
$pp				=	$_GET["pp"];

if($t =='' and $c==''){
	#$t ="1";
}

if(($pp =="" or !is_numeric($pp)) and $U_PERPAGE!="") {
		$pp	=	$U_PERPAGE;
}

if(($pp =="" or !is_numeric($pp)) and $U_PERPAGE=="") {
		$pp	=	10;
}



$_SESSION['U_PERPAGE']	=	$pp;

if($pp =="10") {	$pp10="SELECTED";}
if($pp =="20") {	$pp20="SELECTED";}
if($pp =="50") {	$pp50="SELECTED";}
if($pp =="100") {	$pp100="SELECTED";}
if($pp =="200") {	$pp200="SELECTED";}



### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="add.php?t=$t&c=$c&s=$s";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}

if (preg_match("/$MODULE_PATH-I/i",$U_ACCESS)) {$IMPORT_LINK_HTML ="import.php";}
else{$IMPORT_LINK_HTML ="javascript:no_access_popup();";}

if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="edit.php?id=<ID>";$UP_LINK="sort.php?action=up&id=<ID>";$DOWN_LINK="sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}

if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:page_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###

$p			= $_GET["p"];
if ($p =="" or !is_numeric($p)) {	$p		=	1;}





$TITLE_TOPIC	="Product";


$tp				=	new Template("../_tp_main.html");

$tp_page		=	new Template("_tp_list.html");

if($k !=""){
	$SQL_SEARCH .="(PRODUCT_NAME LIKE '%$k%' OR PRODUCT_DESCRIPTION LIKE '%$k%' OR PRODUCT_CHIC_CODE LIKE '%$k%') AND ";	
}

if($t!="" and $t !="NOCAT"){
	$SQL_SEARCH .="PRODUCT_STYLE LIKE '%,$t,%' AND ";
}
if($t=="NOCAT"){
	$SQL_SEARCH .="PRODUCT_STYLE ='' AND ";
	$t_select	="SELECTED";
}
if($c!="" AND $s!="" and $c !="NOCAT"){
	$SQL_SEARCH .="PRODUCT_CATEGORY LIKE '%,$c|$s,%' AND ";

}
if($c!="" AND $s=="" and $c !="NOCAT"){
	$SQL_SEARCH .="PRODUCT_CATEGORY LIKE '%,$c|%' AND ";
}
if($c=="NOCAT"){
	$SQL_SEARCH .="PRODUCT_CATEGORY = '' AND ";	
	$c_select	="SELECTED";
}
if($c==""){
	$SQL_SEARCH .="ID != '' AND ";	
}


$SQL_SEARCH .="ID!='' ";


		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE $SQL_SEARCH AND PRODUCT_PIC !='';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		$product_pic	=	$count;








		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE $SQL_SEARCH;";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		$product_all	=	$count;


if($view =="pic"){

		$SQL_SEARCH .="AND PRODUCT_PIC !=''";		
		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE $SQL_SEARCH;";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);

}



$total_records = $count;
$scroll_page = 10; // จำนวน scroll page
$per_page = $pp; // จำนวน record ต่อหนึ่ง page
$current_page = $p; // หน้าปัจจุบัน
$pager_url = "index.php?t=$t&c=$c&s=$s&k=$k&view=$view&p="; // url page ที่ต้องการเรียก
$inactive_page_tag = ''; // 
$previous_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_previous.png">'; // หน้าก่อนหน้า
$next_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_next.png">'; // หน้าถัดไป
$first_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_first.png">'; // ไปหน้าแรก
$last_page_text = '<img src="'.$BASEURL.'/chicadmin/images/arrows/arrow_last.png">'; // ไปหน้าสุดท้าย

$kgPagerOBJ = new kgPager();
$kgPagerOBJ -> pager_set($pager_url, $total_records, $scroll_page, $per_page, $current_page, $inactive_page_tag, $previous_page_text, $next_page_text, $first_page_text, $last_page_text);


$pagelink .=$kgPagerOBJ -> first_page;
$pagelink .=$kgPagerOBJ -> previous_page;
$pagelink .=$kgPagerOBJ -> page_links;
$pagelink .=$kgPagerOBJ -> next_page;
$pagelink .=$kgPagerOBJ -> last_page;

if($pagelink=="<span>1</span>") {
	$pagelink="";
}


			if ($p	== 1) {
				$start	=0;
				$limit	=$pp;
				$i		=1;
			}else{
				$start	=(($pp*$p)-$pp);
				$limit	=$pp;
				$i		=(($pp*$p)-$pp)+1;
			}



$tp_page->Block("MEMBER_LIST");
$tp_page->Sub(2);


		$SQL			=	"SELECT * FROM $DB_PRODUCT WHERE $SQL_SEARCH ORDER BY ID DESC LIMIT $start,$limit;";	

		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$PRODUCT_PATH		=	$row["PRODUCT_PATH"];
				$PRODUCT_PIC		=	$row["PRODUCT_PIC"];
				$PRODUCT_NAME		=	$row["PRODUCT_NAME"];
				$PRODUCT_STATUS		=	$row["PRODUCT_STATUS"];
				$CHIC_CODE			=	$row["PRODUCT_CHIC_CODE"];
				$PRICE				=	number_format($row["PRODUCT_PRICE"]);

				$C_TIME				=	$row["TIME_ADDDATE"];
				$L_TIME				=	$row["TIME_UPDATE"];

				if(is_file("../../_files/product/thumb/$PRODUCT_PATH/$PRODUCT_PIC")== true){
					$PRODUCT_IMAGE ="<img src='../../_files/product/thumb/$PRODUCT_PATH/$PRODUCT_PIC'>";
				}else{
					$PRODUCT_IMAGE ="<img src='../../_files/images/common/noimage.jpg'>";				
				}
 	
	#PRODUCT_PATH 	PRODUCT_PIC 	PRODUCT_CATEGORY 	PRODUCT_STYLE 	PRODUCT_NAME 	SUPPLIER_DESCRIPTION 	BARCODE 	MATERIAL 	COLOR 	PRODUCT_W 	PRODUCT_D 	PRODUCT_H 	PACK_SIZE 	CBM 	NW 	GW 	REMARK1 	REMARK2 	PRICE 	SUP_CODE 	CHIC_CODE 	CHIC_DESCRIPTION 	PRODUCT_STATUS 	ADDDATE 	UPDATE


			if($PRODUCT_STATUS=="S"){$STATUS ='<span class="button submit medium green-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">ปกติ</span>';}
			if($PRODUCT_STATUS=="N"){$STATUS ='<span class="button submit medium yellow-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">สินค้าใหม่</span>';}
			if($PRODUCT_STATUS=="O"){$STATUS ='<span class="button submit medium orange-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">Out Stock</span>';}
			if($PRODUCT_STATUS=="H"){$STATUS ='<span class="button submit medium red-back ui-corner-all" style="width:70px;text-align:center;font-size:12px;">ซ่อน</span>';}


			


				if($C_TIME=="0000-00-00 00:00:00"){$C_TIME ="";}
				if($L_TIME=="0000-00-00 00:00:00"){$L_TIME ="";}

				$EDIT_LINK_HTML	=$EDIT_LINK;
				$DEL_LINK_HTML	=$DEL_LINK;
				$UP_LINK_HTML	=$UP_LINK;
				$DOWN_LINK_HTML	=$DOWN_LINK;

				$EDIT_LINK_HTML =preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);
				$DEL_LINK_HTML =preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);
				$UP_LINK_HTML =preg_replace("/<ID>/i",$ID,$UP_LINK_HTML);
				$DOWN_LINK_HTML =preg_replace("/<ID>/i",$ID,$DOWN_LINK_HTML);

				$link_html	="<a href='/program_detail.php?id=$ID' target='_blank' class='nopad'>/program_detail.php?id=$ID</a>";

				$tp_page->Apply();
				$i++;

			}
 	 	 




					$PRODUCT_STYLE_HTML .=" <option value=\"index.php\" style=\"padding:3px;padding-left:10px;\">ทั้งหมด</option>\n";
					$PRODUCT_STYLE_HTML .=" <option value=\"index.php?t=NOCAT\" style=\"padding:3px;padding-left:10px;\" $t_select>ยังไม่ได้จัดลง Style</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PRODUCT_STYLE ORDER BY TOPIC_SORT DESC;";	
				$result2			=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["ID"];
					$TOPIC_NAME			=	trim($row2["TOPIC_NAME"]);
					if($t == $PID){
						$SELECT="SELECTED";
						$TITLE_TOPIC	="Product / $TOPIC_NAME";
					}else{
						$SELECT="";					
					}
					$PRODUCT_STYLE_HTML .=" <option value=\"index.php?t=$PID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$TOPIC_NAME</option>\n";
				}


					$PRODUCT_CATEGORY_HTML .=" <option value=\"index.php\" style=\"padding:3px;padding-left:10px;\">ทั้งหมด</option>\n";
					$PRODUCT_CATEGORY_HTML .=" <option value=\"index.php?c=NOCAT\" style=\"padding:3px;padding-left:10px;\" $c_select>ยังไม่ได้จัดลง Category</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PRODUCT_CATALOG ORDER BY TOPIC_SORT DESC;";	
				$result2			=	mysql_query($SQL2);

				while ($row2		=	mysql_fetch_array($result2)){	
					$CID					=	$row2["ID"];
					$TOPIC_NAME			=	trim($row2["TOPIC_NAME"]);
					if($c == $CID AND $s ==''){
						$SELECT="SELECTED";
						$TITLE_TOPIC	="Product / $TOPIC_NAME";
					}else{
						$SELECT="";					
					}
					$PRODUCT_CATEGORY_HTML .=" <option value=\"index.php?c=$CID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$TOPIC_NAME</option>\n";


					$SQL3			=	"SELECT * FROM $DB_PRODUCT_CATALOG_SUB WHERE CATALOG_ID='$CID' ORDER BY CATALOG_SUB_SORT DESC;";	
					$result3		=	mysql_query($SQL3);
					while ($row3		=	mysql_fetch_array($result3)){	
						$SID				=	$row3["ID"];
						$catalog_sub_name	=	$row3["CATALOG_SUB_NAME"];
					
					if($c== $CID AND $s == $SID){
						$SELECT="SELECTED";						
						$TITLE_TOPIC	="Product / $TOPIC_NAME - $catalog_sub_name";
					}else{
						$SELECT="";					
					}
					$PRODUCT_CATEGORY_HTML .=" <option value=\"index.php?c=$CID&s=$SID\" style=\"padding:3px;padding-left:20px;\" $SELECT> - $catalog_sub_name</option>\n";

					}


				}



$CONTENT_HTML	=	$tp_page->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>