<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="product";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="Product";
$MODULE_DETAIL		="";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,X,I,";



$MODULE_SUB[]		="Category|program|catalog.php";
$MODULE_SUB[]		="Style|program|style.php";

$MODULE_SUB[]		="Product|program|index.php";

?>