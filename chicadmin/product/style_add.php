<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="product";
$page_sub_nav	="lineup_list";
$TITLE_TOPIC	="Product  / <a href='style.php'>Style</a> / เพิ่มหัวข้อ";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_style_add.html");


$action		=	$_POST["action"];


if ($action =="add") {


	$PIC_W	=1020;
	$PIC_H	=440;  
	$style_name			=	$_POST["style_name"];
	$style_detail			=	$_POST["style_detail"];


		$file			=	$_FILES['pic']['name'];
		$typefile		=	$_FILES['pic']['type'];	
		$sizefile		=	$_FILES['pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}




					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 180 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 180 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}


					$picfile	=	"$ran$typecheck1";


		}


		$PIC_W			=213;
		$PIC_H			=100;
		$file			=	$_FILES['pic_text']['name'];
		$typefile		=	$_FILES['pic_text']['type'];	
		$sizefile		=	$_FILES['pic_text']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic_text']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic_text']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}




					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic_text']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 140 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 140 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}


					$pictext	=	"$ran$typecheck1";


		}



		$seo_link		=	generate_seo_link($style_name, '-', false,'');


		$SQL			=	"SELECT * FROM $DB_PRODUCT_STYLE ORDER BY TOPIC_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$TOPIC_SORT	=	$row["TOPIC_SORT"];
			}
		$TOPIC_SORT		= $TOPIC_SORT+1;


	$SQL			=	"INSERT INTO $DB_PRODUCT_STYLE VALUES('', '$seo_link','$style_name','$picfile','$pictext','$style_detail','$TOPIC_SORT');";	
	$result			=	mysql_query($SQL);


		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>