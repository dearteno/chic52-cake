<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="product";



$TITLE_TOPIC	="Product  / <a href='style.php'>Style</a> / ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}


		$SQL			=	"SELECT * FROM $DB_PRODUCT_STYLE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$TOPIC_PIC 				=	$row["TOPIC_PIC"];
				$TOPIC_PIC_TEXT 		=	$row["TOPIC_PIC_TEXT"];
			}

					if($TOPIC_PIC !="") {
						deletedata("../../_files/images/full/$TOPIC_PIC");
						deletedata("../../_files/images/source/$TOPIC_PIC");
						deletedata("../../_files/images/thumb/$TOPIC_PIC");
					}

					if($TOPIC_PIC_TEXT !="") {
						deletedata("../../_files/images/full/$TOPIC_PIC_TEXT");
						deletedata("../../_files/images/source/$TOPIC_PIC_TEXT");
						deletedata("../../_files/images/thumb/$TOPIC_PIC_TEXT");
					}


$SQL			=	"DELETE FROM $DB_PRODUCT_STYLE WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_style_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>