<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="product";



$TITLE_TOPIC	="Product  / <a href='style.php'>Style</a> / แก้ไขหัวข้อ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_style_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {



	$PIC_W	=1020;
	$PIC_H	=440;  
	$style_name			=	$_POST["style_name"];
	$style_detail			=	$_POST["style_detail"];
	$pictemp				=	$_POST["pictemp"];
	$pictemp2				=	$_POST["pictemp2"];


		$file			=	$_FILES['pic']['name'];
		$typefile		=	$_FILES['pic']['type'];	
		$sizefile		=	$_FILES['pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$pictemp");
						deletedata("../../_files/images/source/$pictemp");
						deletedata("../../_files/images/thumb/$pictemp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}



					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 180 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 180 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					
					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}

					$picfile	=	"$ran$typecheck1";

					$SQL		=	"UPDATE $DB_PRODUCT_STYLE SET TOPIC_PIC='$picfile' WHERE ID='$id'";	
					$result		=	mysql_query($SQL);    


		}




		$PIC_W			=213;
		$PIC_H			=100;
		$file			=	$_FILES['pic_text']['name'];
		$typefile		=	$_FILES['pic_text']['type'];	
		$sizefile		=	$_FILES['pic_text']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					

					if($pictemp2 !="" AND $pictemp2 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$pictemp2");
						deletedata("../../_files/images/source/$pictemp2");
						deletedata("../../_files/images/thumb/$pictemp2");
					}


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['pic_text']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['pic_text']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($PIC_W,$PIC_H);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}




					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['pic_text']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 140 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 140 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}


					if($PIC_W !=0 AND $PIC_H !=0){
						$thumb = PhpThumbFactory::create($paththumb);
						$thumb->resize($w2,$h2);
						#$thumb->crop(0,0,202,119);
						$thumb->save($paththumb);
					}


					$pictext	=	"$ran$typecheck1";


					$SQL		=	"UPDATE $DB_PRODUCT_STYLE SET TOPIC_PIC_TEXT='$pictext' WHERE ID='$id'";	
					$result		=	mysql_query($SQL);    

		}



		$SQL			=	"UPDATE $DB_PRODUCT_STYLE SET TOPIC_NAME='$style_name',TOPIC_DETAIL='$style_detail' WHERE ID='$id';";	
		$result			=	mysql_query($SQL);



		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_PRODUCT_STYLE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$ID					=	$row["ID"];
				$pictemp			=	$row["TOPIC_PIC"];
				$pictemp2			=	$row["TOPIC_PIC_TEXT"];
				$style_name			=	$row["TOPIC_NAME"];
				$style_detail			=	$row["TOPIC_DETAIL"];

			}




		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>