<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../_modules/config.php");
include ("../_modules/other/sub.php");
include ("../_modules/mysql/mysql.php");
include ("../_modules/cache/cache-kit.php");
include ("../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../_modules/session/session.php");
include ("../_modules/image/thumbnail.inc.php");


$page_nav		="profile";
$page_sub_nav	="profile_edit";

include ("menu.php");

if ($U_STATUS =="") {redirect("login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF" AND $U_STATUS !="USER") {redirect("$BASEURL/chicadmin/logout.php");exit;}

$dialog_open		="false";	

$action				=	$_POST["action"];


$tp					=	new Template("_tp_main.html");
$tp_profile_edit	=	new Template("_tp_profile_edit.html");

$TITLE_TOPIC		=	"แก้ไขข้อมูลส่วนตัว";

if($action=="edit") {
		
		$name			=	$_POST["name"];
		$surname		=	$_POST["surname"];
		$nickname		=	$_POST["nickname"];
		$pictemp		=	$_POST["pictemp"];
		$email			=	$_POST["email"];
		$mobile			=	$_POST["mobile"];
		$sex			=	$_POST["sex"];
		$bday			=	$_POST["bday"];
		$bmonth			=	$_POST["bmonth"];
		$byear			=	$_POST["byear"];
		$idcard			=	$_POST["idcard"];
		$address		=	$_POST["address"];
		$province		=	$_POST["province"];
		$zipcode		=	$_POST["zipcode"];
		$alert_mobile	=	$_POST["alert_mobile"];
		$alert_amount1	=	$_POST["alert_amount1"];
		$alert_amount2	=	$_POST["alert_amount2"];
		$alert_amount3	=	$_POST["alert_amount3"];


		$file			=	$_FILES['file']['name'];
		$typefile		=	$_FILES['file']['type'];	
		$sizefile		=	$_FILES['file']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../_files/member/$pictemp");
					}

					$ran		=random_string(8);
					$paththumb	="../_files/member/$ran$typecheck1";
					copy($_FILES['file']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 110 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 110 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					//$thumb->cropFromCenter(65);
					$thumb->save($paththumb);

					$picfile	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_USER SET PIC='$picfile' WHERE USERNAME ='$U_USERNAME'";	
					$result			=	mysql_query($SQL);
					$_SESSION['U_PIC']	=	"$ran$typecheck1";

					$U_PIC ="$ran$typecheck1";



		}


		$byear = $byear-543;
		if (strlen($bday)==1) {		$bday ="0$bday";}
		if (strlen($bmonth)==1) {	$bmonth ="0$bmonth";}


		$SQL			=	"UPDATE $DB_USER SET NAME='$name',SURNAME='$surname',NICKNAME='$nickname',SEX='$sex',EMAIL='$email',MOBILE='$mobile' WHERE USERNAME ='$U_USERNAME'";	
		$result			=	mysql_query($SQL);


		$tp_profile_edit->Block("PROFILE_SUCCESS");
		$tp_profile_edit->Apply();


				$_SESSION['U_NAME']				=	$name;
				$_SESSION['U_SURNAME']			=	$surname;
				$_SESSION['U_NICKNAME']			=	$nickname;
				$_SESSION['U_EMAIL']			=	$email;


}else{

		$SQL			=	"SELECT * FROM $DB_USER WHERE USERNAME ='$U_USERNAME' AND (STATUS='ADMIN' OR STATUS='STAFF' OR STATUS='USER')";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if($count !=1) {
			redirect("index.php");
			exit;
		}

			while ($row		=	mysql_fetch_array($result)){	

				$username				=	$row["USERNAME"];
				$pictemp				=	$row["PIC"];
				$name					=	$row["NAME"];
				$surname				=	$row["SURNAME"];
				$nickname				=	$row["NICKNAME"];
				$email					=	$row["EMAIL"];
				$sex					=	$row["SEX"];
				$mobile					=	$row["MOBILE"];
				$birthday				=	$row["BIRTHDAY"];
				$idcard					=	$row["ID_CARD"];
				$address				=	$row["ADDRESS"];
				$province				=	$row["PROVINCE"];
				$zipcode				=	$row["ZIPCODE"];

			}


			if($pictemp =="") {
				$pictemp ="blank_user.jpg";
			}


			if ($sex=="M") {
				$select_m	="checked";
			}else{
				$select_f	="checked";
			}

			$tp_profile_edit->Block("PROFILE_INFO");
			$tp_profile_edit->Apply();

			$tp_profile_edit->Block("PROFILE_FORM");
			$tp_profile_edit->Apply();

}





			$CONTENT_HTML	= $tp_profile_edit->Generate();





$tp->Display();

ob_end_flush();
mysql_close();
?>