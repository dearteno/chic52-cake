<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="program";



$TITLE_TOPIC	="<a href='index.php'>รายการต่างๆ</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_add.html");


$action		=	$_POST["action"];

$t			=	$_GET["t"];
if($t==""){	$t		=	$_POST["t"];}


$news_date	=	date("d-m-Y");

if ($action =="add") {




		### SEO ###
		$seo_url			=	generate_seo_link($_POST["seo_url"], '-', false,'');
		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$news_date			=	coverttime_y_m_d($_POST["news_date"]);
		$file				=	$_FILES['seo_image']['name'];
		$typefile			=	$_FILES['seo_image']['type'];	
		$sizefile			=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}



		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$seofile	=	"$ran$typecheck1";

		}
		### SEO ###



		$program_type			=	$_POST["program_type"];
		$program_channel		=	$_POST["program_channel"];
		$program_package1		=	$_POST["program_package1"];
		$program_package2		=	$_POST["program_package2"];
		$program_package3		=	$_POST["program_package3"];
		$highlight_home			=	$_POST["highlight_home"];
		$highlight_program		=	$_POST["highlight_program"];
		$program_status			=	$_POST["program_status"];

		if ($highlight_home =="Y") {$highlight_home ="Y";}else{$highlight_home ="N";}
		if ($highlight_program =="Y") {$highlight_program ="Y";}else{$highlight_program ="N";}
		if ($program_status =="Y") {$program_status ="S";}else{$program_status ="H";}

		$program_name			=	$_POST["program_name"];
		$program_intro			=	$_POST["program_intro"];
		$program_content		=	$_POST["program_content"];
		$program_youtube		=	$_POST["program_youtube"];

		if($program_package1 !=""){			$program_package .="$program_package1,";		}
		if($program_package2 !=""){			$program_package .="$program_package2,";		}
		if($program_package3 !=""){			$program_package .="$program_package3,";		}
		if($program_package !=""){			$program_package =",$program_package";}

		for($i=1;$i<=5;$i++){


				if($i==1){$W2="978";$H2="340";}
				if($i==2){$W2="291";$H2="349";}
				if($i==3){$W2="250";$H2="215";}
				if($i==4){$W2="250";$H2="215";}
				if($i==5){$W2="137";$H2="118";}
			
				$file				=	$_FILES["program_cover$i"]['name'];
				$typefile			=	$_FILES["program_cover$i"]['type'];	
				$sizefile			=	$_FILES["program_cover$i"]['size'];
				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
							
							$ran		=random_string(8);
							$paththumb	="../../_files/images/source/$ran$typecheck1";
							copy($_FILES["program_cover$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/images/full/$ran$typecheck1";
							copy($_FILES["program_cover$i"]['tmp_name'],$pathfull);
							chmod("$paththumb", 0777); 
							$paththumb	="../../_files/images/thumb/$ran$typecheck1";
							copy($_FILES["program_cover$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 

							list($w1, $h1) = getimagesize($pathfull);

							$w2 = $W2 ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;

							$thumb = new Thumbnail($pathfull);
							$thumb->resize($w2,$h2);
							$thumb->crop(0,0,$W2,$H2);
							$thumb->save($pathfull);

							list($w1, $h1) = getimagesize($paththumb);
							$h2 = 118 ;
							$percent = $h2/$h1;
							$w2 = $w1 * $percent;	

							$thumb = new Thumbnail($paththumb);
							$thumb->resize($w2,$h2);
							$thumb->save($paththumb);

							$name		=	"program_cover$i";
							$$name		=	"$ran$typecheck1";

				}



		}


	$SQL			=	"INSERT INTO $DB_PROGRAM VALUES('', '$program_cover1', '$program_cover2', '$program_cover3', '$program_cover4', '$program_cover5', '$program_type', '$program_channel', '$program_package', '$program_name', '$program_intro','$program_content', '$program_youtube', '', '$highlight_home', '$highlight_program', '$program_status', '$seo_url','$seo_title', '$seo_description', '$seo_keyword', '$seofile', '$U_USERNAME', NOW(), '$U_USERNAME', NOW());";	
	$result			=	mysql_query($SQL);


	$SQL			=	"SELECT ID FROM $DB_PROGRAM WHERE 	COVER_PIC1='$program_cover1' ORDER BY ID DESC LIMIT 0,1;";	
	$result			=	mysql_query($SQL);
	while ($row		=	mysql_fetch_array($result)){	
		$ID				=	$row["ID"];
	}


	for($i=1;$i<=10;$i++){
		$TEMP_DATE	=	$_POST["program_date$i"];
		$TEMP_HOUR	=	$_POST["program_hour$i"];
		$TEMP_MIN	=	$_POST["program_min$i"];

		if($TEMP_DATE !="" AND $TEMP_HOUR !="" AND $TEMP_MIN !=""){
			$TEMP_DATE		=	coverttime_y_m_d($TEMP_DATE);
			$SQL			=	"INSERT INTO $DB_PROGRAM_SCHEDULE VALUES('', '$ID', '$program_type','$TEMP_DATE $TEMP_HOUR:$TEMP_MIN:00');";	
			$result			=	mysql_query($SQL);			
		}
	}










	$t	=$program_type;


		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



					$PROGRAM_TYPE_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PROGRAM_TYPE ORDER BY ID;";	
				$result2			=	mysql_query($SQL2);
				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["ID"];
					$PROGRAM_TYPE			=	trim($row2["PROGRAM_TYPE"]);
					if($t == $PID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$PROGRAM_TYPE_HTML .="<option value=\"$PID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$PROGRAM_TYPE</option>\n";
				}


				$CHANNEL_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL			=	"SELECT * FROM $DB_CHANNEL ORDER BY CHANNEL_ID;";	
				$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$ID					=	$row["ID"];
					$CHANNEL_NAME		=	$row["CHANNEL_NAME"];
					$CHANNEL_ID			=	$row["CHANNEL_ID"];
					$CHANNEL_PIC		=	$row["CHANNEL_PIC"];

					$CHANNEL_HTML .="<option value=\"$CHANNEL_ID\" style=\"padding:3px;padding-left:10px;\">$CHANNEL_NAME ($CHANNEL_ID)</option>\n";
				}
 	 	 

				$SQL			=	"SELECT * FROM $DB_PACKAGE ORDER BY ID;";	
				$result			=	mysql_query($SQL);
					while ($row		=	mysql_fetch_array($result)){	
						$ID				=	$row["ID"];

						$PACKAGE_PIC	=	$row["PACKAGE_PIC"];			
						$PACKAGE_HTML .="<input name=\"program_package$ID\" type=\"checkbox\" id=\"program_package$ID\" value=\"$ID\" /><img src=\"../../_files/images/full/$PACKAGE_PIC\"  /> ";

					}
 	 	 



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>