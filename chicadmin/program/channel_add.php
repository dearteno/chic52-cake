<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="program";



$TITLE_TOPIC	="<a href='channel.php'>Channel</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_channel_add.html");


$action		=	$_POST["action"];

$t			=	$_GET["t"];
if($t==""){	$t		=	$_POST["t"];}
if($t!=1 AND $t!=2){	exit;}

$news_date	=	date("d-m-Y");

if ($action =="add") {



		$channel_name		=	$_POST["channel_name"];
		$channel_id			=	$_POST["channel_id"];

		$file				=	$_FILES['channel_cover1']['name'];
		$typefile			=	$_FILES['channel_cover1']['type'];	
		$sizefile			=	$_FILES['channel_cover1']['size'];

		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['channel_cover1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['channel_cover1']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['channel_cover1']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 


					$coverfile1	=	"$ran$typecheck1";

		}


		$SQL			=	"INSERT INTO $DB_CHANNEL VALUES('', '$channel_id','$channel_name', '$coverfile1','$U_USERNAME',NOW(),'$U_USERNAME',NOW());";	
		$result			=	mysql_query($SQL);


		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}


if($t ==1){
	$t_name ="Channel";
}
if($t==2){
	$t_name	="GMMZ Family";
}


		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>