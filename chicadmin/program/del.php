<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="program";


$TITLE_TOPIC	="ลบ";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

		$id				=	$_GET['id'];
		if ($id =="" or !is_numeric($id)) {	exit;}

		$SQL			=	"SELECT * FROM $DB_PROGRAM WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$cover_pic1		=	$row["COVER_PIC1"];
				$cover_pic2		=	$row["COVER_PIC2"];
				$COVER_HOMEPAGE_HIGHLIGHT1		=	$row["COVER_HOMEPAGE_HIGHLIGHT1"];
				$COVER_HOMEPAGE_HIGHLIGHT2		=	$row["COVER_HOMEPAGE_HIGHLIGHT2"];
				$COVER_PROGRAM_HIGHLIGHT		=	$row["COVER_PROGRAM_HIGHLIGHT"];
				$PROGRAM_TYPE			=	$_row["PROGRAM_TYPE"];
			}
 	 	 	 	

					if($cover_pic1 !="" AND $cover_pic1 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$cover_pic1");
						deletedata("../../_files/images/source/$cover_pic1");
						deletedata("../../_files/images/thumb/$cover_pic1");
					}
					if($cover_pic2 !="" AND $cover_pic2 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$cover_pic2");
						deletedata("../../_files/images/source/$cover_pic2");
						deletedata("../../_files/images/thumb/$cover_pic2");
					}
					if($COVER_HOMEPAGE_HIGHLIGHT1 !="" AND $COVER_HOMEPAGE_HIGHLIGHT1 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$COVER_HOMEPAGE_HIGHLIGHT1");
						deletedata("../../_files/images/source/$COVER_HOMEPAGE_HIGHLIGHT1");
						deletedata("../../_files/images/thumb/$COVER_HOMEPAGE_HIGHLIGHT1");
					}
					if($COVER_HOMEPAGE_HIGHLIGHT2 !="" AND $COVER_HOMEPAGE_HIGHLIGHT2 !="blank_user.jpg") {
						deletedata("../../_files/images/full/$COVER_HOMEPAGE_HIGHLIGHT2");
						deletedata("../../_files/images/source/$COVER_HOMEPAGE_HIGHLIGHT2");
						deletedata("../../_files/images/thumb/$COVER_HOMEPAGE_HIGHLIGHT2");
					}
					if($COVER_PROGRAM_HIGHLIGHT !="" AND $COVER_PROGRAM_HIGHLIGHT !="blank_user.jpg") {
						deletedata("../../_files/images/full/$COVER_PROGRAM_HIGHLIGHT");
						deletedata("../../_files/images/source/$COVER_PROGRAM_HIGHLIGHT");
						deletedata("../../_files/images/thumb/$COVER_PROGRAM_HIGHLIGHT");
					}



$SQL			=	"DELETE FROM $DB_PROGRAM WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$SQL			=	"DELETE FROM $DB_PROGRAM_SCHEDULE WHERE PROGRAM_ID='$id'";	
$result			=	mysql_query($SQL);


$tp				=	new Template("../_tp_main.html");
$tp_del		=	new Template("_tp_del.html");




$CONTENT_HTML	=	$tp_del->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>