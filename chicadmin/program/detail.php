<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");

$page_nav		="program";


$TITLE_TOPIC	="<a href='index.php'>รายการต่างๆ</a>&nbsp;/&nbsp;รายละเอียด";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_detail	=	new Template("_tp_detail.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}




$WRITE_LINK_HTML ="edit.php?id=$id";

		$SQL			=	"SELECT * FROM $DB_PROGRAM WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	

				$seo_url			=	$row["SEO_URL"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];

				$program_cover1			=	$row["COVER_PIC1"];
				$program_cover2			=	$row["COVER_PIC2"];
				$program_cover3			=	$row["COVER_HOMEPAGE_HIGHLIGHT1"];
				$program_cover4			=	$row["COVER_HOMEPAGE_HIGHLIGHT2"];
				$program_cover5			=	$row["COVER_PROGRAM_HIGHLIGHT"];
				$program_type			=	$row["PROGRAM_TYPE"];
				$program_channel_id			=	$row["PROGRAM_CHANNEL_ID"];
				$program_package			=	$row["PROGRAM_PACKAGE"];
				$program_name				=	$row["PROGRAM_NAME"];
				$program_intro				=	$row["PROGRAM_INTRO"];
				$program_content			=	$row["PROGRAM_DETAIL"];
				$program_youtube			=	$row["PROGRAM_CLIP"];
				$program_schedule			=	$row["PROGRAM_SCHEDULE"];
				$program_homepage_highlight			=	$row["PROGRAM_HOMEPAGE_HIGHLIGHT"];
				$program_highlight			=	$row["PROGRAM_HIGHLIGHT"];
				$program_status			=	$row["PROGRAM_STATUS"];
			}




if ($program_status=="S") {
    $program_status_y ="CHECKED  disabled='disabled' ";
} 	 	 	 


if ($program_homepage_highlight=="Y") {
    $highlight_home_y ="CHECKED  disabled='disabled' ";
} 	

if ($program_highlight=="Y") {
    $highlight_program_y ="CHECKED  disabled='disabled' ";
} 


if($program_youtube !=""){
	$program_youtube ='<iframe width="560" height="315" src="//www.youtube.com/embed/'.$program_youtube.'" frameborder="0" allowfullscreen></iframe>';
}

				$SQL2			=	"SELECT * FROM $DB_PROGRAM_TYPE WHERE ID='$program_type' ORDER BY ID;";	
				$result2			=	mysql_query($SQL2);
				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["ID"];
					$PROGRAM_TYPE			=	trim($row2["PROGRAM_TYPE"]);

					$PROGRAM_TYPE_HTML .="$PROGRAM_TYPE\n";
				}


				$SQL			=	"SELECT * FROM $DB_CHANNEL WHERE CHANNEL_ID='$program_channel_id' ORDER BY CHANNEL_ID;";	
				$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$ID					=	$row["ID"];
					$CHANNEL_NAME		=	$row["CHANNEL_NAME"];
					$CHANNEL_ID			=	$row["CHANNEL_ID"];
					$CHANNEL_PIC		=	$row["CHANNEL_PIC"];

					$CHANNEL_HTML .="$CHANNEL_NAME ($CHANNEL_ID)\n";
				}
 	 	 

				$SQL			=	"SELECT * FROM $DB_PACKAGE ORDER BY ID;";	
				$result			=	mysql_query($SQL);
					while ($row		=	mysql_fetch_array($result)){	
						$ID				=	$row["ID"];

						if (preg_match("/,$ID,/i", $program_package)) {
							$SELECT="CHECKED  disabled='disabled' ";
						}else{
							$SELECT="";					
						}
						
						$PACKAGE_PIC	=	$row["PACKAGE_PIC"];			
						$PACKAGE_HTML .="<input name=\"program_package$ID\" type=\"checkbox\" id=\"program_package$ID\" value=\"$ID\" $SELECT /><img src=\"../../_files/images/full/$PACKAGE_PIC\"  /> ";

					}
 	 	

				$i=1;
				$SQL			=	"SELECT * FROM $DB_PROGRAM_SCHEDULE WHERE PROGRAM_ID='$id' ORDER BY PROGRAM_DATETIME;";	
				$result			=	mysql_query($SQL);
					while ($row		=	mysql_fetch_array($result)){	
						$PROGRAM_DATETIME	=	$row["PROGRAM_DATETIME"];
						list($DATE,$TIME)				=	preg_split('/ /',$PROGRAM_DATETIME);
						$PACKAGE_SCHEDULE_HTML .="<tr><td width=\"10\"><span class='bold'>$i.</span></td><td >$DATE</td><td >$TIME</td></tr>";
						$i++;
					}		 

                      



$CONTENT_HTML	=	$tp_detail->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>