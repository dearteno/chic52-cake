<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="program";


$TITLE_TOPIC	="<a href='index.php'>รายการต่างๆ</a>&nbsp;/&nbsp;แก้ไข";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");
$tp_edit	=	new Template("_tp_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


if ($action =="edit") {




		### SEO ###
		$seo_url			=	generate_seo_link($_POST["seo_url"], '-', false,'');

		$seo_title			=	$_POST["seo_title"];
		$seo_description	=	$_POST["seo_description"];
		$seo_keyword		=	$_POST["seo_keyword"];
		$news_date			=	coverttime_y_m_d($_POST["news_date"]);


		$file				=	$_FILES['seo_image']['name'];
		$typefile			=	$_FILES['seo_image']['type'];	
		$sizefile			=	$_FILES['seo_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['seo_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}

					$thumb = new Thumbnail($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$seofile	=	"$ran$typecheck1";
					$SQL		=	"UPDATE $DB_PROGRAM SET SEO_IMAGE='$seofile' WHERE ID='$id';";	
					$result		=	mysql_query($SQL);
		}
		### SEO ###


		$program_type			=	$_POST["program_type"];
		$program_channel		=	$_POST["program_channel"];
		$program_package1		=	$_POST["program_package1"];
		$program_package2		=	$_POST["program_package2"];
		$program_package3		=	$_POST["program_package3"];
		$highlight_home			=	$_POST["highlight_home"];
		$highlight_program		=	$_POST["highlight_program"];
		$program_status			=	$_POST["program_status"];

		if ($highlight_home =="Y") {$highlight_home ="Y";}else{$highlight_home ="N";}
		if ($highlight_program =="Y") {$highlight_program ="Y";}else{$highlight_program ="N";}
		if ($program_status =="Y") {$program_status ="S";}else{$program_status ="H";}

		$program_name			=	$_POST["program_name"];
		$program_intro			=	$_POST["program_intro"];
		$program_content		=	$_POST["program_content"];
		$program_youtube		=	$_POST["program_youtube"];

		if($program_package1 !=""){			$program_package .="$program_package1,";		}
		if($program_package2 !=""){			$program_package .="$program_package2,";		}
		if($program_package3 !=""){			$program_package .="$program_package3,";		}
		if($program_package !=""){			$program_package =",$program_package";}


		for($i=1;$i<=5;$i++){

 	 	 	 	 
				if($i==1){$W2="978";$H2="340"; $SQL_IMAGE ='COVER_PIC1';}
				if($i==2){$W2="291";$H2="349"; $SQL_IMAGE ='COVER_PIC2';}
				if($i==3){$W2="250";$H2="215"; $SQL_IMAGE ='COVER_HOMEPAGE_HIGHLIGHT1';}
				if($i==4){$W2="250";$H2="215"; $SQL_IMAGE ='COVER_HOMEPAGE_HIGHLIGHT2';}
				if($i==5){$W2="137";$H2="118"; $SQL_IMAGE ='COVER_PROGRAM_HIGHLIGHT';}
			
				$file				=	$_FILES["program_cover$i"]['name'];
				$typefile			=	$_FILES["program_cover$i"]['type'];	
				$sizefile			=	$_FILES["program_cover$i"]['size'];
				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
							
							$tempfile	=	$_POST["pic_temp_cover$i"];
							if($tempfile !=""){
								deletedata("../../_files/images/full/$tempfile");
								deletedata("../../_files/images/source/$tempfile");
								deletedata("../../_files/images/thumb/$tempfile");								
							}

							$ran		=random_string(8);
							$paththumb	="../../_files/images/source/$ran$typecheck1";
							copy($_FILES["program_cover$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/images/full/$ran$typecheck1";
							copy($_FILES["program_cover$i"]['tmp_name'],$pathfull);
							chmod("$paththumb", 0777); 
							$paththumb	="../../_files/images/thumb/$ran$typecheck1";
							copy($_FILES["program_cover$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 

							list($w1, $h1) = getimagesize($pathfull);

							$w2 = $W2 ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;

							$thumb = new Thumbnail($pathfull);
							$thumb->resize($w2,$h2);
							$thumb->crop(0,0,$W2,$H2);
							$thumb->save($pathfull);

							list($w1, $h1) = getimagesize($paththumb);
							$h2 = 118;
							$percent = $h2/$h1;
							$w2 = $w1 * $percent;	

							$thumb = new Thumbnail($paththumb);
							$thumb->resize($w2,$h2);
							$thumb->save($paththumb);


					$SQL		=	"UPDATE $DB_PROGRAM SET $SQL_IMAGE='$ran$typecheck1' WHERE ID='$id';";	
					$result		=	mysql_query($SQL);


				}



		}



	$SQL			=	"UPDATE $DB_PROGRAM SET 
	PROGRAM_TYPE='$program_type', PROGRAM_CHANNEL_ID='$program_channel', PROGRAM_PACKAGE='$program_package', PROGRAM_NAME='$program_name', PROGRAM_INTRO='$program_intro',PROGRAM_DETAIL='$program_content', PROGRAM_CLIP='$program_youtube', PROGRAM_HOMEPAGE_HIGHLIGHT='$highlight_home', PROGRAM_HIGHLIGHT='$highlight_program', PROGRAM_STATUS='$program_status',SEO_TITLE='$seo_title',SEO_DESCRIPTION='$seo_description',SEO_KEYWORD='$seo_keyword',SEO_URL='$seo_url',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);

	for($i=1;$i<=10;$i++){
		$TEMP_DATE	=	$_POST["program_date$i"];
		$TEMP_HOUR	=	$_POST["program_hour$i"];
		$TEMP_MIN	=	$_POST["program_min$i"];

		if($TEMP_DATE !="" AND $TEMP_HOUR !="" AND $TEMP_MIN !=""){
			$TEMP_DATE		=	coverttime_y_m_d($TEMP_DATE);
			$SQL			=	"INSERT INTO $DB_PROGRAM_SCHEDULE VALUES('', '$id', '$program_type','$TEMP_DATE $TEMP_HOUR:$TEMP_MIN:00');";	
			$result			=	mysql_query($SQL);			
		}
	}




		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_PROGRAM WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	

				$seo_url			=	$row["SEO_URL"];
				$seo_title			=	$row["SEO_TITLE"];
				$seo_description	=	$row["SEO_DESCRIPTION"];
				$seo_keyword		=	$row["SEO_KEYWORD"];
				$seo_image			=	$row["SEO_IMAGE"];

				$program_cover1			=	$row["COVER_PIC1"];
				$program_cover2			=	$row["COVER_PIC2"];
				$program_cover3			=	$row["COVER_HOMEPAGE_HIGHLIGHT1"];
				$program_cover4			=	$row["COVER_HOMEPAGE_HIGHLIGHT2"];
				$program_cover5			=	$row["COVER_PROGRAM_HIGHLIGHT"];
				$program_type			=	$row["PROGRAM_TYPE"];
				$program_channel_id			=	$row["PROGRAM_CHANNEL_ID"];
				$program_package			=	$row["PROGRAM_PACKAGE"];
				$program_name				=	$row["PROGRAM_NAME"];
				$program_intro				=	$row["PROGRAM_INTRO"];
				$program_content			=	$row["PROGRAM_DETAIL"];
				$program_youtube			=	$row["PROGRAM_CLIP"];
				$program_schedule			=	$row["PROGRAM_SCHEDULE"];
				$program_homepage_highlight			=	$row["PROGRAM_HOMEPAGE_HIGHLIGHT"];
				$program_highlight			=	$row["PROGRAM_HIGHLIGHT"];
				$program_status			=	$row["PROGRAM_STATUS"];
			}




		if($program_homepage_highlight =="Y"){
			$highlight_home_y ="CHECKED";
		}
		if($program_highlight =="Y"){
			$highlight_program_y ="CHECKED";
		}

		if ($program_status =="S") {
		    $program_status_y ="CHECKED";
		}

		if ($news_status =="H") {
		    $news_status_h ="CHECKED";		    
		}




					$PROGRAM_TYPE_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL2			=	"SELECT * FROM $DB_PROGRAM_TYPE ORDER BY ID;";	
				$result2			=	mysql_query($SQL2);
				while ($row2		=	mysql_fetch_array($result2)){	
					$PID					=	$row2["ID"];
					$PROGRAM_TYPE			=	trim($row2["PROGRAM_TYPE"]);
					if($program_type == $PID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$PROGRAM_TYPE_HTML .="<option value=\"$PID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$PROGRAM_TYPE</option>\n";
				}


				$CHANNEL_HTML .=" <option value=\"#\" style=\"padding:3px;padding-left:10px;\">กรุณาระบุ</option>\n";
				$SQL			=	"SELECT * FROM $DB_CHANNEL ORDER BY CHANNEL_ID;";	
				$result			=	mysql_query($SQL);
				while ($row		=	mysql_fetch_array($result)){	
					$ID					=	$row["ID"];
					$CHANNEL_NAME		=	$row["CHANNEL_NAME"];
					$CHANNEL_ID			=	$row["CHANNEL_ID"];
					$CHANNEL_PIC		=	$row["CHANNEL_PIC"];
					if($program_channel_id == $CHANNEL_ID){
						$SELECT="SELECTED";
					}else{
						$SELECT="";					
					}
					$CHANNEL_HTML .="<option value=\"$CHANNEL_ID\" style=\"padding:3px;padding-left:10px;\" $SELECT>$CHANNEL_NAME ($CHANNEL_ID)</option>\n";
				}
 	 	 

				$SQL			=	"SELECT * FROM $DB_PACKAGE ORDER BY ID;";	
				$result			=	mysql_query($SQL);
					while ($row		=	mysql_fetch_array($result)){	
						$ID				=	$row["ID"];

						if (preg_match("/,$ID,/i", $program_package)) {
							$SELECT="CHECKED";
						}else{
							$SELECT="";					
						}
						
						$PACKAGE_PIC	=	$row["PACKAGE_PIC"];			
						$PACKAGE_HTML .="<input name=\"program_package$ID\" type=\"checkbox\" id=\"program_package$ID\" value=\"$ID\" $SELECT /><img src=\"../../_files/images/full/$PACKAGE_PIC\"  /> ";

					}
 	 	 
				$i=1;
				$SQL			=	"SELECT * FROM $DB_PROGRAM_SCHEDULE WHERE PROGRAM_ID='$id' ORDER BY PROGRAM_DATETIME;";	
				$result			=	mysql_query($SQL);
					while ($row		=	mysql_fetch_array($result)){	
						$ID				=	$row["ID"];
						$PROGRAM_DATETIME	=	$row["PROGRAM_DATETIME"];
						list($DATE,$TIME)				=	preg_split('/ /',$PROGRAM_DATETIME);
						$PACKAGE_SCHEDULE_HTML .="<tr><td width=\"10\"><span class='bold'>$i.</span></td><td >$DATE</td><td >$TIME</td><td width=\"10\"><a href='javascript:schedule_confirm($ID);' class='nopad'><img src='../images/icons/delete.png'></a></td></tr>";
						$i++;
					}		 

$program_intro_show = preg_replace("/\(SD\)/i","</span><span class=\"sp80\">(SD)</span>",$program_intro);
$program_intro_show = preg_replace("/\(HD\)/i","</span><span class=\"sp80\">(HD)</span>",$program_intro_show);
$program_intro_show = preg_replace("/หมายเลขช่อง/i","<span class=\"sp150\">หมายเลขช่อง",$program_intro_show);
$program_intro_show = htmlspecialchars($program_intro_show);

			$program_intro = htmlspecialchars($program_intro);

		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>