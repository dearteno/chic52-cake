<?php
 
###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

$MODULE_PATH		="program";
$MODULE_ACCESS		="ADMIN,STAFF";
$MODULE_NAME		="รายการ";
$MODULE_DETAIL		="รายการ";
$MODULE_VERSION		="1.0";



$MODULE_PERMISSION	=",R,W,D,E,";



$MODULE_SUB[]		="รายการต่างๆ|program|index.php";
$MODULE_SUB[]		="Channel|program|channel.php";
$MODULE_SUB[]		="Package|program|package.php";

?>