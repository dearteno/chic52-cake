<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/image/thumbnail.inc.php");



$page_nav		="program";


$TITLE_TOPIC	="<a href='index.php'>รายการต่างๆ</a>&nbsp;/&nbsp;แก้ไข";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_member_edit	=	new Template("_tp_edit.html");


$t			=	$_GET["t"];
$id			=	$_GET["id"];
$p			=	$_GET["p"];




		$SQL			=	"SELECT * FROM $DB_PROGRAM WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("$BASEURL/chicadmin/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$PROGRAM_HOMEPAGE_HIGHLIGHT  	=	$row["PROGRAM_HOMEPAGE_HIGHLIGHT"];
				$PROGRAM_HIGHLIGHT  			=	$row["PROGRAM_HIGHLIGHT"];
				$PROGRAM_STATUS  				=	$row["PROGRAM_STATUS"];
				$PROGRAM_TYPE					=	$row["PROGRAM_TYPE"];
			}
 	 	

if($t=="show"){


			if($PROGRAM_STATUS  =="S"){
				$PROGRAM_STATUS_NEW="H";
			}else{
				$PROGRAM_STATUS_NEW="S";
			}

			$SQL		=	"UPDATE $DB_PROGRAM SET PROGRAM_STATUS='$PROGRAM_STATUS_NEW',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id'";	
			$result		=	mysql_query($SQL);   
	
}

if($t=="program"){


			if($PROGRAM_HIGHLIGHT  =="Y"){
				$PROGRAM_HIGHLIGHT_NEW="N";
			}else{
				$PROGRAM_HIGHLIGHT_NEW="Y";
			}

			$SQL		=	"UPDATE $DB_PROGRAM SET PROGRAM_HIGHLIGHT='$PROGRAM_HIGHLIGHT_NEW',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id'";	
			$result		=	mysql_query($SQL);   
	
}

if($t=="homepage"){


			if($PROGRAM_HOMEPAGE_HIGHLIGHT  =="Y"){
				$PROGRAM_HOMEPAGE_HIGHLIGHT_NEW="N";
			}else{
				$PROGRAM_HOMEPAGE_HIGHLIGHT_NEW="Y";
			}

			$SQL		=	"UPDATE $DB_PROGRAM SET PROGRAM_HOMEPAGE_HIGHLIGHT='$PROGRAM_HOMEPAGE_HIGHLIGHT_NEW',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id'";	
			$result		=	mysql_query($SQL);   
	
}



 

redirect("index.php?t=$PROGRAM_TYPE&p=$p");

ob_end_flush();
mysql_close();
?>