<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="promotion";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");



$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="image_add.php?id=$id";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="image_edit.php?id=<ID>";$UP_LINK="image_sort.php?action=up&id=<ID>";$DOWN_LINK="image_sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:image_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###


$tp_edit	=	new Template("_tp_edit.html");


$TITLE_TOPIC	="<a href='index.php'>Promotion</a>&nbsp;/&nbsp;แก้ไข";



if ($action =="edit") {


		$page_topic			=	$_POST["page_topic"];
		
		$page_intro_th			=	$_POST["page_intro_th"];
		$page_intro_en			=	$_POST["page_intro_en"];
		$page_content_th		=	$_POST["page_content_th"];
		$page_content_en		=	$_POST["page_content_en"];

		$page_status		=	$_POST["page_status"];

		if($page_status =="Y"){
			$page_status ="S";
		}else{
			$page_status ="H";		
		}





		for($i=1;$i<=3;$i++){

				if($i==1){
					$FULL_SIZE_W	='1229';
					$FULL_SIZE_H	='460';
					$THUMB_SIZE_W	='200';
				}
				if($i==2){
					$FULL_SIZE_W	='81';
					$FULL_SIZE_H	='32';
					$THUMB_SIZE_W	='81';
				}
				if($i==3){
					$FULL_SIZE_W	='213';
					$FULL_SIZE_H	='100';
					$THUMB_SIZE_W	='180';
				}					

				$file				=	$_FILES["page_pic$i"]['name'];
				$typefile			=	$_FILES["page_pic$i"]['type'];	
				$sizefile			=	$_FILES["page_pic$i"]['size'];
				$typecheck1		=	strtolower($file);
				$typecheck1		=	strrchr($typecheck1,'.');
				if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

				if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
							
							$page_pic_temp			=	$_POST["page_pic_temp$i"];

							if($page_pic_temp !=""){
								deletedata("../../_files/images/full/$page_pic_temp");
								deletedata("../../_files/images/source/$page_pic_temp");
								deletedata("../../_files/images/thumb/$page_pic_temp");						
							}


							$ran		=random_string(8);
							$paththumb	="../../_files/images/source/$ran$typecheck1";
							copy($_FILES["page_pic$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 
							$pathfull	="../../_files/images/full/$ran$typecheck1";
							copy($_FILES["page_pic$i"]['tmp_name'],$pathfull);
							chmod("$pathfull", 0777); 

							$paththumb	="../../_files/images/thumb/$ran$typecheck1";
							copy($_FILES["page_pic$i"]['tmp_name'],$paththumb);
							chmod("$paththumb", 0777); 


							$thumb = PhpThumbFactory::create($pathfull);
							$thumb->resize($FULL_SIZE_W,$FULL_SIZE_H);
							$thumb->crop(0,0,$FULL_SIZE_W,$FULL_SIZE_H);
							$thumb->save($pathfull);

							list($w1, $h1) = getimagesize($paththumb);
							$w2 = $THUMB_SIZE_W ;
							$percent = $w2/$w1;
							$h2 = $h1 * $percent;


							$thumb = PhpThumbFactory::create($paththumb);
							$thumb->resize($w2,$h2);
							#$thumb->crop(0,0,$FULL_SIZE_W,$FULL_SIZE_H);
							$thumb->save($paththumb);

							$PROMOTION_PIC_TEMP	=	"PROMOTION_PIC$i";



	$SQL			=	"UPDATE $DB_PROMOTION SET $PROMOTION_PIC_TEMP='$ran$typecheck1',
	UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);


				}

		}






	$SQL			=	"UPDATE $DB_PROMOTION SET PROMOTION_TOPIC='$page_topic',
	PROMOTION_INTRO_TH='$page_intro_th',PROMOTION_INTRO_EN='$page_intro_en',
	PROMOTION_CONTENT_TH='$page_content_th',PROMOTION_CONTENT_EN='$page_content_en',PROMOTION_STATUS='$page_status',
	UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);







		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_PROMOTION WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$page_topic				=	$row["PROMOTION_TOPIC"];
				$page_pic_temp1			=	$row["PROMOTION_PIC1"];
				$page_pic_temp2			=	$row["PROMOTION_PIC2"];
				$page_pic_temp3			=	$row["PROMOTION_PIC3"];
				$page_intro_th			=	$row["PROMOTION_INTRO_TH"];
				$page_intro_en			=	$row["PROMOTION_INTRO_EN"];
				$page_content_th		=	$row["PROMOTION_CONTENT_TH"];
				$page_content_en		=	$row["PROMOTION_CONTENT_EN"];
				$page_status			=	$row["PROMOTION_STATUS"];
			}


if($page_status =="S"){
	$content_status_y="CHECKED";
}else{
	$content_status_y="";
}


		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>