<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");



$page_nav		="service";



$TITLE_TOPIC	="<a href='index.php'>Service</a>&nbsp;/&nbsp;เพิ่ม";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### เพิ่มรายชื่อพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_add	=	new Template("_tp_add.html");


$action		=	$_POST["action"];



$service_date	=	date("d-m-Y");

if ($action =="add") {







		$service_name			=	$_POST["service_name"];
		$service_content		=	$_POST["service_content"];



		$file				=	$_FILES['service_pic']['name'];
		$typefile			=	$_FILES['service_pic']['type'];	
		$sizefile			=	$_FILES['service_pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['service_pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['service_pic']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['service_pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$w2=300;

					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = PhpThumbFactory::create($pathfull);
					$thumb->resize($w2,$h2);
					$thumb->save($pathfull);

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$coverfile	=	"$ran$typecheck1";

		}



		$SQL			=	"SELECT * FROM $DB_SERVICE ORDER BY SERVICE_SORT DESC LIMIT 0,1;";	
		$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$service_sort	=	$row["SERVICE_SORT"];
			}
		$service_sort		= $service_sort+1;



	$SQL			=	"INSERT INTO $DB_SERVICE VALUES('', '$service_name', '$coverfile', '$service_content', '$service_content', '', '', 'S', '$service_sort', '$U_USERNAME',NOW(),'$U_USERNAME',NOW());";
	$result			=	mysql_query($SQL);


		$tp_add->Block("STAFF_SUCCESS");
		$tp_add->Apply();

$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_add->Block("STAFF_INFO");
		$tp_add->Apply();


}



		$tp_add->Block("STAFF_FORM");
		$tp_add->Apply();


$CONTENT_HTML	=	$tp_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>