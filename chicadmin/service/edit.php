<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");






$page_nav		="service";




include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### แก้ไขรายชื่อพนักงาน ###
$YOUTUBE_DEL_LINK_HTML ="javascript:youtubedel('<ID>');";
$IMAGE_DEL_LINK_HTML ="javascript:imagedel('<ID>');";

$tp			=	new Template("../_tp_main.html");



$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("/logout.php");exit;}


### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="image_add.php?id=$id";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="image_edit.php?id=<ID>";$UP_LINK="image_sort.php?action=up&id=<ID>";$DOWN_LINK="image_sort.php?action=down&id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";$UP_LINK="javascript:no_access_popup();";$DOWN_LINK="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:image_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###



	$service_name	='Chic Concept';
	$tp_edit	=	new Template("_tp_edit.html");



$TITLE_TOPIC	="<a href='index.php'>About Us</a>&nbsp;/&nbsp;$service_name&nbsp;/&nbsp;แก้ไข";



if ($action =="edit") {


		$service_topic			=	$_POST["service_topic"];
		$service_pic_temp			=	$_POST["service_pic_temp"];
		$service_content			=	$_POST["service_content"];

		$file				=	$_FILES['service_pic']['name'];
		$typefile			=	$_FILES['service_pic']['type'];	
		$sizefile			=	$_FILES['service_pic']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}

		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($service_pic_temp !=""){
						deletedata("../../_files/images/full/$service_pic_temp");
						deletedata("../../_files/images/source/$service_pic_temp");
						deletedata("../../_files/images/thumb/$service_pic_temp");						
					}


					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['service_pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$pathfull	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['service_pic']['tmp_name'],$pathfull);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['service_pic']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($pathfull);

					$w2=300;

					$percent = $w2/$w1;
					$h2 = $h1 * $percent;

					$thumb = PhpThumbFactory::create($pathfull);
					$thumb->resize($w2,$h2);
					$thumb->save($pathfull);

					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize($w2,$h2);
					$thumb->save($paththumb);

					$coverfile	=	"$ran$typecheck1";


					$SQL			=	"UPDATE $DB_SERVICE SET SERVICE_PIC='$coverfile',UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
					$result			=	mysql_query($SQL);

		}




	$SQL			=	"UPDATE $DB_SERVICE SET SERVICE_TOPIC='$service_topic',
	SERVICE_INTRO_TH='$service_content',SERVICE_INTRO_EN='$service_content',
	UPDATE_USERNAME='$U_USERNAME',UPDATE_TIME=NOW() WHERE ID='$id';";	
	$result			=	mysql_query($SQL);


		$tp_edit->Block("STAFF_SUCCESS");
		$tp_edit->Apply();

$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_edit->Block("STAFF_INFO");
		$tp_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_SERVICE WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$service_topic		=	$row["SERVICE_TOPIC"];
				$service_pic_temp			=	$row["SERVICE_PIC"];
				$service_content			=	$row["SERVICE_INTRO_TH"];
			}



		$tp_edit->Block("STAFF_FORM");
		$tp_edit->Apply();


$CONTENT_HTML	=	$tp_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>