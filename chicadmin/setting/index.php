<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="setting";
$page_sub_nav	="settingf_list";
$TITLE_TOPIC	="Setting";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### SET MENU ###
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK_HTML ="setting_edit.php";}
else{$EDIT_LINK_HTML ="javascript:no_access_popup();";}
### SET MENU ###

### เพิ่มรายชื่อพนักงาน ###

$tp				=	new Template("../_tp_main.html");
$tp_setting_list	=	new Template("_tp_setting_list.html");


$action		=$_POST["action"];




	$SQL	="SELECT * FROM $DB_SETTING WHERE ID='1';";
	$result	=mysql_query($SQL);

	while ($row		=	mysql_fetch_array($result)){	
		$WEBSITE_TITLE			=	$row["WEBSITE_TITLE"];
		$WEBSITE_DESCRIPTION	=	$row["WEBSITE_DESCRIPTION"];
		$WEBSITE_KEYWORD		=	$row["WEBSITE_KEYWORD"];
		$WEBSITE_IMAGE			=	$row["WEBSITE_IMAGE"];
		$WEBSITE_TEL			=	$row["WEBSITE_TEL"];
		$WEBSITE_MOBILE			=	$row["WEBSITE_MOBILE"];
		$WEBSITE_EMAIL			=	$row["WEBSITE_EMAIL"];
		$WEBSITE_ADDRESS		=	nl2br($row["WEBSITE_ADDRESS"]);
		$WEBSITE_FACEBOOK			=	$row["WEBSITE_FACEBOOK"];
		$WEBSITE_YOUTUBE			=	$row["WEBSITE_YOUTUBE"];
		$WEBSITE_INSTAGRAM			=	$row["WEBSITE_INSTAGRAM"];
		$WEBSITE_TWITTER			=	$row["WEBSITE_TWITTER"];
		$WEBSITE_PINTEREST			=	$row["WEBSITE_PINTEREST"];
		$WEBSITE_NAME				=	$row["WEBSITE_NAME"];
	}	

		if($WEBSITE_FACEBOOK !=""){$WEBSITE_FACEBOOK ="<a href='$WEBSITE_FACEBOOK' target='_blank'>$WEBSITE_FACEBOOK</a>";}
		if($WEBSITE_YOUTUBE !=""){$WEBSITE_YOUTUBE ="<a href='$WEBSITE_YOUTUBE' target='_blank'>$WEBSITE_YOUTUBE</a>";}
		if($WEBSITE_INSTAGRAM !=""){$WEBSITE_INSTAGRAM ="<a href='$WEBSITE_INSTAGRAM' target='_blank'>$WEBSITE_INSTAGRAM</a>";}
		if($WEBSITE_TWITTER !=""){$WEBSITE_TWITTER ="<a href='$WEBSITE_TWITTER' target='_blank'>$WEBSITE_TWITTER</a>";}
		if($WEBSITE_PINTEREST !=""){$WEBSITE_PINTEREST ="<a href='$WEBSITE_PINTEREST' target='_blank'>$WEBSITE_PINTEREST</a>";}



		$tp_setting_list->Block("PASSWORD_FORM");
		$tp_setting_list->Apply();




$CONTENT_HTML	=	$tp_setting_list->Generate();

$tp->Display();

ob_end_flush();
mysql_close();
?>