<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
include ("../../_modules/phpthumb/ThumbLib.inc.php");

$page_nav		="setting";
$page_sub_nav	="settingf_list";
$TITLE_TOPIC	="<a href='index.php'>Setting</a> / Edit";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### SET MENU ###
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK_HTML ="setting_edit.php";}
else{$EDIT_LINK_HTML ="javascript:no_access_popup();";}
### SET MENU ###

### เพิ่มรายชื่อพนักงาน ###

$tp				=	new Template("../_tp_main.html");
$tp_setting_edit	=	new Template("_tp_setting_edit.html");


$action		=$_POST["action"];


if ($action =="edit") {


		$website_title				=	trim($_POST["website_title"]);
		$website_description		=	trim($_POST["website_description"]);
		$website_keyword			=	trim($_POST["website_keyword"]);
		$pictemp					=	trim($_POST["pictemp"]);
		$website_tel				=	trim($_POST["website_tel"]);
		$website_mobile				=	trim($_POST["website_mobile"]);
		$website_email				=	trim($_POST["website_email"]);
		$website_address			=	trim($_POST["website_address"]);
		$website_facebook			=	trim($_POST["website_facebook"]);
		$website_youtube			=	trim($_POST["website_youtube"]);
		$website_instagram			=	trim($_POST["website_instagram"]);

		$website_name				=	trim($_POST["website_name"]);
		$website_twitter			=	trim($_POST["website_twitter"]);
		$website_pinterest			=	trim($_POST["website_pinterest"]);



		$file			=	$_FILES['website_image']['name'];
		$typefile		=	$_FILES['website_image']['type'];	
		$sizefile		=	$_FILES['website_image']['size'];
		$typecheck1		=	strtolower($file);
		$typecheck1		=	strrchr($typecheck1,'.');
		if ($typecheck1 ==".jpeg") {$typecheck1 =".jpg";}


		if ((preg_match("/image/i",$typefile)) AND ($typecheck1 == ".jpg" OR $typecheck1 == ".gif" OR $typecheck1 == ".png" )) {
					
					if($pictemp !="" AND $pictemp !="blank_user.jpg") {
						deletedata("../../_files/images/full/$pictemp");
						deletedata("../../_files/images/source/$pictemp");
						deletedata("../../_files/images/thumb/$pictemp");
					}

					$ran		=random_string(8);
					$paththumb	="../../_files/images/source/$ran$typecheck1";
					copy($_FILES['website_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/full/$ran$typecheck1";
					copy($_FILES['website_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 
					$paththumb	="../../_files/images/thumb/$ran$typecheck1";
					copy($_FILES['website_image']['tmp_name'],$paththumb);
					chmod("$paththumb", 0777); 

					list($w1, $h1) = getimagesize($paththumb);
					if ($w1 > $h1) {
						$w2 = 400 ;
						$percent = $w2/$w1;
						$h2 = $h1 * $percent;
					}else{
						$h2 = 400 ;
						$percent = $h2/$h1;
						$w2 = $w1 * $percent;				
					}


					$thumb = PhpThumbFactory::create($paththumb);
					$thumb->resize($w2,$h2);
					#$thumb->crop(0,0,202,119);
					$thumb->save($paththumb);

					$picfile	=	"$ran$typecheck1";

					$SQL			=	"UPDATE $DB_SETTING SET WEBSITE_IMAGE='$picfile' WHERE ID='1'";	
					$result			=	mysql_query($SQL);

		}




	$SQL		= "UPDATE $DB_SETTING SET WEBSITE_TITLE='$website_title',WEBSITE_DESCRIPTION='$website_description',WEBSITE_KEYWORD='$website_keyword',WEBSITE_MOBILE='$website_mobile',WEBSITE_TEL='$website_tel',WEBSITE_EMAIL='$website_email',WEBSITE_ADDRESS='$website_address',WEBSITE_FACEBOOK='$website_facebook',WEBSITE_YOUTUBE='$website_youtube',WEBSITE_INSTAGRAM='$website_instagram',WEBSITE_NAME='$website_name', WEBSITE_TWITTER='$website_twitter',WEBSITE_PINTEREST='$website_pinterest' WHERE ID='1';";	
	$result		= mysql_query($SQL);


	$tp_setting_edit->Block("PASSWORD_SUCCESS");
	$tp_setting_edit->Apply();


}else{




	$SQL	="SELECT * FROM $DB_SETTING WHERE ID='1';";
	$result	=mysql_query($SQL);

	while ($row		=	mysql_fetch_array($result)){	
		$website_title			=	$row["WEBSITE_TITLE"];
		$website_description	=	$row["WEBSITE_DESCRIPTION"];
		$website_keyword		=	$row["WEBSITE_KEYWORD"];
		$website_image			=	$row["WEBSITE_IMAGE"];
		$website_tel			=	$row["WEBSITE_TEL"];
		$website_mobile			=	$row["WEBSITE_MOBILE"];
		$website_email			=	$row["WEBSITE_EMAIL"];
		$website_address		=	$row["WEBSITE_ADDRESS"];
		$website_map			=	$row["WEBSITE_MAP"];
		$website_facebook			=	$row["WEBSITE_FACEBOOK"];
		$website_youtube			=	$row["WEBSITE_YOUTUBE"];
		$website_instagram			=	$row["WEBSITE_INSTAGRAM"];
		$website_twitter			=	$row["WEBSITE_TWITTER"];
		$website_pinterest			=	$row["WEBSITE_PINTEREST"];
		$website_name				=	$row["WEBSITE_NAME"];
	}	

		$tp_setting_edit->Block("PASSWORD_INFO");
		$tp_setting_edit->Apply();

		$tp_setting_edit->Block("PASSWORD_FORM");
		$tp_setting_edit->Apply();

}


$CONTENT_HTML	=	$tp_setting_edit->Generate();

$tp->Display();

ob_end_flush();
mysql_close();
?>