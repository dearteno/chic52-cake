<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="staff";
$page_sub_nav	="staff_list";
$TITLE_TOPIC	="Staff";

include ("../menu.php");
include ("module_info.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-R/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### SET MENU ###
if (preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {$WRITE_LINK_HTML ="staff_add.php";}
else{$WRITE_LINK_HTML ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {$EDIT_LINK ="staff_edit.php?id=<ID>";}
else{$EDIT_LINK ="javascript:no_access_popup();";}
if (preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {$DEL_LINK ="javascript:staff_delete(<ID>);";}
else{$DEL_LINK ="javascript:no_access_popup();";}
### SET MENU ###


### แสดงรายnameพนักงาน ###

$tp				=	new Template("../_tp_main.html");

$tp_staff		=	new Template("_tp_staff_list.html");


		$SQL			=	"SELECT * FROM $DB_USER WHERE (STATUS='ADMIN' OR STATUS='STAFF' OR STATUS='MANAGER') ORDER BY STATUS;";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);

$tp_staff->Block("MEMBER_LIST");
$tp_staff->Sub(2);

			$i	=1;
			while ($row		=	mysql_fetch_array($result)){	
				$ID				=	$row["ID"];
				$USERNAME		=	$row["USERNAME"];
				$PIC			=	$row["PIC"];
				$NAME			=	$row["NAME"];
				$SURNAME		=	$row["SURNAME"];
				$NICKNAME		=	$row["NICKNAME"];
				$EMAIL			=	$row["EMAIL"];
				$SEX			=	$row["SEX"];
				$MOBILE			=	$row["MOBILE"];
				$STATUS			=	$row["STATUS"];
				$ACCESS			=	$row["ACCESS"];
				$LASTLOGIN		=	$row["LASTLOGIN"];
				if ($PIC =="") {
					$PIC="blank_user.jpg";
				}

				if ($STATUS =="ADMIN") {
				    $STATUS ="Admin";
				}

				if ($STATUS =="STAFF") {
				    $STATUS ="Staff";
				}

				if($LASTLOGIN =="0000-00-00 00:00:00") {
					$LASTLOGIN="";
				}

				$EDIT_LINK_HTML	=$EDIT_LINK;
				$DEL_LINK_HTML	=$DEL_LINK;

				$EDIT_LINK_HTML =preg_replace("/<ID>/i",$ID,$EDIT_LINK_HTML);
				$DEL_LINK_HTML =preg_replace("/<ID>/i",$ID,$DEL_LINK_HTML);

				$tp_staff->Apply();
				$i++;
			}



$CONTENT_HTML	=	$tp_staff->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>