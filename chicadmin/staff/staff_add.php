<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
#include ("../../forum/smf_2_api.php");

$page_nav		="staff";
$page_sub_nav	="staff_add";


$TITLE_TOPIC	="<a href='index.php'>Staff</a>&nbsp;/&nbsp;Add Staff";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-W/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}


### Addรายnameพนักงาน ###

$tp				=	new Template("../_tp_main.html");


$tp_staff_add	=	new Template("_tp_staff_add.html");




$action		=	$_POST["action"];

if ($action =="add") {

$username	=	$_POST["username"];
$p1			=	$_POST["p1"];
$nickname	=	$_POST["nickname"];
$name		=	$_POST["name"];
$surname	=	$_POST["surname"];
$email		=	$_POST["email"];
$mobile		=	$_POST["mobile"];
$sex		=	$_POST["sex"];
$status		=	$_POST["status_level"];

for ($a=0;$a<=count($MODULE_CONTROL)-1;$a++) {

	include ("../".$MODULE_CONTROL[$a]."/module_info.php");

			$DATA_ACCESS=$_POST["access_".$MODULE_PATH];
			$TEMP_ACCESS="";

			for ($b=0;$b<=count($DATA_ACCESS)-1;$b++) {
				$TEMP_ACCESS	.=	"$MODULE_PATH-".$DATA_ACCESS[$b].",";
			}
			$access_add .=$TEMP_ACCESS;
}
			if($access_add !="") {
				$access_add =",$access_add";
			}



	$SQL			=	"SELECT * FROM $DB_USER WHERE USERNAME='$username';";	
	$result			=	mysql_query($SQL);
	$count			=	mysql_num_rows($result);

	if ($count !=0) {
		$ERROR ="ERROR";
	}



if ($ERROR !="ERROR") {

	$p11	=	md5(md5(md5($p1)));


	$SQL		=	"INSERT INTO $DB_USER VALUES('', '$username', '$p11', 'blank_user.jpg', '$name', '$surname', '$nickname', '$sex', '$email', '$mobile', '$status', '$access_add', NOW(),'0000-00-00 00:00:00','');";
	$result		=	mysql_query($SQL);        

/*
					$reg= array(
						'member_name' => $username,
						'email' => $email,
						'real_name' => $nickname,
						'require' => 'nothing',
						'password' => $p1,
						'password_check' => $p1
					);
					smfapi_registerMember($reg);

			$SQL			=	"UPDATE smf_members SET avatar='/_files/member/blank_user.jpg' WHERE member_name='$username';";	
			$result			=	mysql_query($SQL);  
*/


$tp_staff_add->Block("STAFF_SUCCESS");
$tp_staff_add->Apply();

$CONTENT_HTML	=	$tp_staff_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{

			$dialog_open	="true";
			$ERROR_MSG		="<p>&nbsp;</p><p>Username duplicate. Please enter again.</p><p>&nbsp;</p>";

			$tp_staff_add->Block("STAFF_ERROR");
			$tp_staff_add->Apply();

}


}else{

$tp_staff_add->Block("STAFF_INFO");
$tp_staff_add->Apply();

}











$tp_staff_add->Block("MEMBER_LIST");
$tp_staff_add->Sub(2);





for ($a=0;$a<=count($MODULE_CONTROL)-1;$a++) {

	include ("../".$MODULE_CONTROL[$a]."/module_info.php");



#echo "--- ".$MODULE_CONTROL[$a]."$MODULE_PERMISSION<Br/>";
		

		if (preg_match("/,R,/i",$MODULE_PERMISSION)) {
			$READ_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"R\" class='access_level' />";
		}
		if (preg_match("/,W,/i",$MODULE_PERMISSION)) {
			$WRITE_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"W\" class='access_level' />";
		}
		if (preg_match("/,E,/i",$MODULE_PERMISSION)) {
			$EDIT_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"E\" class='access_level' />";
		}
		if (preg_match("/,D,/i",$MODULE_PERMISSION)) {
			$DELETE_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"D\" class='access_level' />";
		}
		if (preg_match("/,I,/i",$MODULE_PERMISSION)) {
			$EXPORT_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"I\" class='access_level' />";
		}
		if (preg_match("/,X,/i",$MODULE_PERMISSION)) {
			$EXPORT_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"X\" class='access_level' />";
		}


	$tp_staff_add->Apply();

	unset($READ_CHECKBOX);
	unset($WRITE_CHECKBOX);
	unset($EDIT_CHECKBOX);
	unset($DELETE_CHECKBOX);
	unset($EXPORT_CHECKBOX);
	unset($MODULE_PERMISSION);

	unset($MODULE_LIST);
	unset($MODULE_ACCESS);
	unset($MODULE_NAME);
	unset($MODULE_DETAIL);


}




$tp_staff_add->Block("STAFF_FORM");
$tp_staff_add->Apply();


$CONTENT_HTML	=	$tp_staff_add->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>

