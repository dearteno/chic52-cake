<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="staff";
$page_sub_nav	="staff_list";

$TITLE_TOPIC	="<a href='index.php'>Staff</a>&nbsp;/&nbsp;Delete";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-D/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### Deleteรายnameพนักงาน ###

			$id				=	$_GET['id'];
			if ($id =="" or !is_numeric($id)) {	exit;}

			$SQL			=	"SELECT * FROM $DB_USER WHERE ID='$id'";	
			$result			=	mysql_query($SQL);
			while ($row		=	mysql_fetch_array($result)){	
				$NAME		=	$row["NAME"];
				$SURNAME	=	$row["SURNAME"];
				$PIC		=	$row["PIC"];
				$username	=	$row["USERNAME"];
			}
			if ($PIC !="" AND $PIC!="blank_user.jpg") {
				deletedata("../../_files/member/$PIC");
			}

$SQL			=	"DELETE FROM $DB_USER WHERE ID='$id'";	
$result			=	mysql_query($SQL);

$SQL			=	"DELETE FROM smf_members WHERE member_name='$username'";	
$result			=	mysql_query($SQL);

$tp				=	new Template("../_tp_main.html");
$tp_staff		=	new Template("_tp_staff_del.html");




$CONTENT_HTML	=	$tp_staff->Generate();


$tp->Display();

ob_end_flush();
mysql_close();
?>