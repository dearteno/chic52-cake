<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");
#include ("../../forum/smf_2_api.php");
$page_nav		="staff";
$page_sub_nav	="staff_list";

$TITLE_TOPIC	="<a href='index.php'>Staff</a>&nbsp;/&nbsp;Edit";

include ("../menu.php");

if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-E/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}

### Editรายnameพนักงาน ###


$tp			=	new Template("../_tp_main.html");
$tp_staff_edit	=	new Template("_tp_staff_edit.html");


$action		=	$_POST["action"];
$id			=	$_GET["id"];
if ($id =="") {$id			=	$_POST["id"];}
if ($id =="") {redirect("$BASEURL/chicadmin/logout.php");exit;}


if ($action =="edit") {

$username	=	$_POST["username"];
$p1			=	$_POST["p1"];
$nickname	=	$_POST["nickname"];
$name		=	$_POST["name"];
$surname	=	$_POST["surname"];
$status		=	$_POST["status"];
$email		=	$_POST["email"];
$mobile		=	$_POST["mobile"];
$sex		=	$_POST["sex"];
$status		=	$_POST["status_level"];

for ($a=0;$a<=count($MODULE_CONTROL)-1;$a++) {

	include ("../".$MODULE_CONTROL[$a]."/module_info.php");

			$DATA_ACCESS=$_POST["access_".$MODULE_PATH];
			$TEMP_ACCESS="";

			for ($b=0;$b<=count($DATA_ACCESS)-1;$b++) {
				$TEMP_ACCESS	.=	"$MODULE_PATH-".$DATA_ACCESS[$b].",";
			}
			$access_add .=$TEMP_ACCESS;
}
			if($access_add !="") {
				$access_add =",$access_add";
			}

	if ($p1 !="") {
			$p11				=	md5(md5(md5($p1)));
			$SQL			=	"UPDATE $DB_USER SET PASSWORD='$p11' WHERE ID='$id';";	
			$result			=	mysql_query($SQL);

			$SQL			=	"SELECT * FROM smf_members WHERE member_name ='$username'";	
			$result			=	mysql_query($SQL);

			while ($row		=	mysql_fetch_array($result)){	
				$id_member	=	$row["id_member"];
			}

			#smfapi_updateMemberData($id_member, array('last_login' => time(), 'passwd' => sha1(strtolower($username) . $p1),'password_salt' => substr(md5(mt_rand()), 0, 4)));

	}



			$SQL			=	"UPDATE $DB_USER SET USERNAME='$username',NAME='$name',SURNAME='$surname',NICKNAME='$nickname',SEX='$sex',EMAIL='$email',MOBILE='$mobile',STATUS='$status',ACCESS='$access_add' WHERE ID='$id';";	
			$result			=	mysql_query($SQL);


			#$SQL			=	"UPDATE smf_members SET real_name='$nickname',email_address='$email' WHERE member_name='$username';";	
			#$result			=	mysql_query($SQL); 

		$tp_staff_edit->Block("STAFF_SUCCESS");
		$tp_staff_edit->Apply();

$CONTENT_HTML	=	$tp_staff_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
exit;

}else{


		$tp_staff_edit->Block("STAFF_INFO");
		$tp_staff_edit->Apply();


}



		$SQL			=	"SELECT * FROM $DB_USER WHERE ID='$id';";	
		$result			=	mysql_query($SQL);
		$count			=	mysql_num_rows($result);
		if ($count ==0) {redirect("$BASEURL/chicadmin/logout.php");exit;}

			while ($row		=	mysql_fetch_array($result)){	
				$username		=	$row["USERNAME"];
				$pic			=	$row["PIC"];
				$name			=	$row["NAME"];
				$surname		=	$row["SURNAME"];
				$nickname		=	$row["NICKNAME"];
				$email			=	$row["EMAIL"];
				$sex			=	$row["SEX"];
				$mobile			=	$row["MOBILE"];
				$status			=	$row["STATUS"];
				$access			=	$row["ACCESS"];
			}


if ($status =="ADMIN") {
	$status1	=	"selected";
}else{
	$status2	=	"selected";
}




if ($sex =="M") {
	$select_m	=	"CHECKED";
}else{
	$select_f	=	"CHECKED";
}



$tp_staff_edit->Block("MEMBER_LIST");
$tp_staff_edit->Sub(2);



for ($a=0;$a<=count($MODULE_CONTROL)-1;$a++) {

	include ("../".$MODULE_CONTROL[$a]."/module_info.php");



#echo "--- ".$MODULE_CONTROL[$a]."$MODULE_PERMISSION<Br/>";
		
		
		if (preg_match("/,R,/i",$MODULE_PERMISSION)) {
			if (preg_match("/$MODULE_PATH-R/i",$access)) {
			    $check_permission="CHECKED";
			}else{
			    $check_permission="";			
			}
			$READ_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"R\" class='access_level' $check_permission />";
		}
		if (preg_match("/,W,/i",$MODULE_PERMISSION)) {
			if (preg_match("/$MODULE_PATH-W/i",$access)) {
			    $check_permission="CHECKED";
			}else{
			    $check_permission="";			
			}
			$WRITE_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"W\" class='access_level' $check_permission />";
		}
		if (preg_match("/,E,/i",$MODULE_PERMISSION)) {
			if (preg_match("/$MODULE_PATH-E/i",$access)) {
			    $check_permission="CHECKED";
			}else{
			    $check_permission="";			
			}
			$EDIT_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"E\" class='access_level' $check_permission />";
		}
		if (preg_match("/,D,/i",$MODULE_PERMISSION)) {
			if (preg_match("/$MODULE_PATH-D/i",$access)) {
			    $check_permission="CHECKED";
			}else{
			    $check_permission="";			
			}
			$DELETE_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"D\" class='access_level' $check_permission />";
		}
		if (preg_match("/,I,/i",$MODULE_PERMISSION)) {
			if (preg_match("/$MODULE_PATH-I/i",$access)) {
			    $check_permission="CHECKED";
			}else{
			    $check_permission="";			
			}
			$IMPORT_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"I\" class='access_level' $check_permission />";
		}
		if (preg_match("/,X,/i",$MODULE_PERMISSION)) {
			if (preg_match("/$MODULE_PATH-X/i",$access)) {
			    $check_permission="CHECKED";
			}else{
			    $check_permission="";			
			}
			$EXPORT_CHECKBOX ="<input name=\"access_".$MODULE_PATH."[]\" type=\"checkbox\" id=\"access_".$MODULE_PATH."[]\" value=\"X\" class='access_level' $check_permission />";
		}


	$tp_staff_edit->Apply();

	unset($READ_CHECKBOX);
	unset($WRITE_CHECKBOX);
	unset($EDIT_CHECKBOX);
	unset($DELETE_CHECKBOX);
	unset($IMPORT_CHECKBOX);
	unset($EXPORT_CHECKBOX);
	unset($MODULE_PERMISSION);

	unset($MODULE_LIST);
	unset($MODULE_ACCESS);
	unset($MODULE_NAME);
	unset($MODULE_DETAIL);


}



		$tp_staff_edit->Block("STAFF_FORM");
		$tp_staff_edit->Apply();


$CONTENT_HTML	=	$tp_staff_edit->Generate();
$tp->Display();

ob_end_flush();
mysql_close();
?>