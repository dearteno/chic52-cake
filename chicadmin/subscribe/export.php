<?
session_name("SESSION_WEBSITE");
session_start();
ob_start();

###### CMS Version 1.0 ######
#
# @author		: Nirun Chaiyadet
# @contact		: nirun@phoinikasdigital.com
# @mobile		: 0814200962
# @copyright	: ChicRepublic.com
#
###### CMS Version 1.0 ######

include ("../../_modules/config.php");
include ("../../_modules/other/sub.php");
include ("../../_modules/mysql/mysql.php");
include ("../../_modules/cache/cache-kit.php");
include ("../../_modules/kgpager/kgPager.class.php");
include ("../../_modules/sixhead_template/SiXhEaD.Template.php");
include ("../../_modules/session/session.php");


$page_nav		="subscribe";
$page_sub_nav	="lineup_list";


include ("../menu.php");
include ("module_info.php");





if ($U_STATUS =="") {redirect("$BASEURL/chicadmin/login.php");exit;}
if ($U_STATUS !="ADMIN" AND $U_STATUS !="STAFF") {redirect("$BASEURL/chicadmin/logout.php");exit;}
if (!preg_match("/$MODULE_PATH-X/i",$U_ACCESS)) {redirect("$BASEURL/chicadmin/logout.php");exit;}



      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=export-email.xls" );
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
      header("Pragma: public");

echo "
<html xmlns:o=\"urn:schemas-microsoft-com:office:office\"
xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
xmlns=\"http://www.w3.org/TR/REC-html40\">
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-type\" content=\"text/html;charset=tis-620\" />
<title>$filename</title>
</head>
<body>
<div id=\"WELOVE_Excel\" align=center x:publishsource=\"Excel\">
<table x:str border=0 cellpadding=0 cellspacing=0 width=100% style=\"border-collapse: collapse\">
<tr> 	 	 	
    <td  class=xl2216681 nowrap>Email</td>
</tr>
";


		$SQL			=	"SELECT * FROM $DB_SUBSCRIBE WHERE ID!='';";	
		$result			=	mysql_query($SQL);

			while ($row		=	mysql_fetch_array($result)){	
				$ID		=	$row["ID"];
				$EMAIL	=	$row["EMAIL"];

echo "
<tr> 	 	 	
    <td class=xl2216681 nowrap>$EMAIL</td>

  </tr>
";

			}
 	 	 
echo "
</table>
</div>
</body>
</html>
";


ob_end_flush();
mysql_close();
?>